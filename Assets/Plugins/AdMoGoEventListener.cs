using UnityEngine;
using System.Collections;

public class AdMoGoEventListener : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnEnable()
	{
		// Listen to  AdMoGoEvent
		AdMoGoManager.adMoGoDidStartAdEvent += adMoGoDidStartAdEvent;
		AdMoGoManager.adMoGoDidReceiveAdEvent += adMoGoDidReceiveAdEvent;
		AdMoGoManager.adMoGoDidFailToReceiveAdEvent += adMoGoDidFailToReceiveAdEvent;
		AdMoGoManager.adMoGoClickAdEvent += adMoGoClickAdEvent;
		AdMoGoManager.adMoGoDeleteAdEvent += adMoGoDeleteAdEvent;
		
		// Listen to AdMoGoWebBrowserControllerUserEvent
		AdMoGoManager.webBrowserWillAppearEvent += webBrowserWillAppearEvent;
		AdMoGoManager.webBrowserDidAppearEvent += webBrowserDidAppearEvent;
		AdMoGoManager.webBrowserWillClosedEvent += webBrowserWillClosedEvent;
		AdMoGoManager.webBrowserDidClosedEvent += webBrowserDidClosedEvent;
		AdMoGoManager.webBrowserShareEvent += webBrowserShareEvent;
		
		// Listen to AdMoGoInterstitialEvent
		AdMoGoManager.adsMoGoInterstitialAdDidStartEvent += adsMoGoInterstitialAdDidStartEvent;
		AdMoGoManager.adsMoGoInterstitialAdIsReadyEvent += adsMoGoInterstitialAdIsReadyEvent;
		AdMoGoManager.adsMoGoInterstitialAdReceivedRequestEvent += adsMoGoInterstitialAdReceivedRequestEvent;
		AdMoGoManager.adsMoGoInterstitialAdWillPresentEvent += adsMoGoInterstitialAdWillPresentEvent;
		AdMoGoManager.adsMoGoInterstitialAdFailedWithErrorEvent += adsMoGoInterstitialAdFailedWithErrorEvent;
		AdMoGoManager.adsMoGoInterstitialAdDidDismissEvent += adsMoGoInterstitialAdDidDismissEvent;
		AdMoGoManager.adsMoGoWillPresentInterstitialAdModalEvent += adsMoGoWillPresentInterstitialAdModalEvent;
		AdMoGoManager.adsMoGoDidDismissInterstitialAdModalEvent += adsMoGoDidDismissInterstitialAdModalEvent;
		AdMoGoManager.adsMoGoInterstitialAdClosedEvent += adsMoGoInterstitialAdClosedEvent;
		AdMoGoManager.didLoadVideoAdEvent += didLoadVideoAdEvent;
		AdMoGoManager.failLoadVideoAdEvent += failLoadVideoAdEvent;
		AdMoGoManager.didPlayVideoAdEvent += didPlayVideoAdEvent;
		AdMoGoManager.finishVideoAdEvent += finishVideoAdEvent;
		AdMoGoManager.adsMogoInterstitialAdAllAdsFailEvent += adsMogoInterstitialAdAllAdsFailEvent;
		AdMoGoManager.adMoGoInterstitialInitFinishEvent += adMoGoInterstitialInitFinishEvent;
		AdMoGoManager.adMoGoInitFinishEvent += adMoGoInitFinishEvent;
		AdMoGoManager.adMoGoInterstitialInMaualfreshAllAdsFailEvent += adMoGoInterstitialInMaualfreshAllAdsFailEvent;
	}


	void OnDisable()
	{
		// Remove all event handlers
		AdMoGoManager.adMoGoDidStartAdEvent -= adMoGoDidStartAdEvent;
		AdMoGoManager.adMoGoDidReceiveAdEvent -= adMoGoDidReceiveAdEvent;
		AdMoGoManager.adMoGoDidFailToReceiveAdEvent -= adMoGoDidFailToReceiveAdEvent;
		AdMoGoManager.adMoGoClickAdEvent -= adMoGoClickAdEvent;
		AdMoGoManager.adMoGoDeleteAdEvent -= adMoGoDeleteAdEvent;
		
		AdMoGoManager.webBrowserWillAppearEvent -= webBrowserWillAppearEvent;
		AdMoGoManager.webBrowserDidAppearEvent -= webBrowserDidAppearEvent;
		AdMoGoManager.webBrowserWillClosedEvent -= webBrowserWillClosedEvent;
		AdMoGoManager.webBrowserDidClosedEvent -= webBrowserDidClosedEvent;
		AdMoGoManager.webBrowserShareEvent -= webBrowserShareEvent;
		
		AdMoGoManager.adsMoGoInterstitialAdDidStartEvent -= adsMoGoInterstitialAdDidStartEvent;
		AdMoGoManager.adsMoGoInterstitialAdIsReadyEvent -= adsMoGoInterstitialAdIsReadyEvent;
		AdMoGoManager.adsMoGoInterstitialAdReceivedRequestEvent -= adsMoGoInterstitialAdReceivedRequestEvent;
		AdMoGoManager.adsMoGoInterstitialAdWillPresentEvent -= adsMoGoInterstitialAdWillPresentEvent;
		AdMoGoManager.adsMoGoInterstitialAdFailedWithErrorEvent -= adsMoGoInterstitialAdFailedWithErrorEvent;
		AdMoGoManager.adsMoGoInterstitialAdDidDismissEvent -= adsMoGoInterstitialAdDidDismissEvent;
		AdMoGoManager.adsMoGoWillPresentInterstitialAdModalEvent -= adsMoGoWillPresentInterstitialAdModalEvent;
		AdMoGoManager.adsMoGoDidDismissInterstitialAdModalEvent -= adsMoGoDidDismissInterstitialAdModalEvent;
		AdMoGoManager.adsMoGoInterstitialAdClosedEvent -= adsMoGoInterstitialAdClosedEvent;
		AdMoGoManager.didLoadVideoAdEvent -= didLoadVideoAdEvent;
		AdMoGoManager.failLoadVideoAdEvent -= failLoadVideoAdEvent;
		AdMoGoManager.didPlayVideoAdEvent -= didPlayVideoAdEvent;
		AdMoGoManager.finishVideoAdEvent -= finishVideoAdEvent;
		AdMoGoManager.adsMogoInterstitialAdAllAdsFailEvent -= adsMogoInterstitialAdAllAdsFailEvent;
		AdMoGoManager.adMoGoInterstitialInitFinishEvent -= adMoGoInterstitialInitFinishEvent;
		AdMoGoManager.adMoGoInitFinishEvent -= adMoGoInitFinishEvent;
		AdMoGoManager.adMoGoInterstitialInMaualfreshAllAdsFailEvent -= adMoGoInterstitialInMaualfreshAllAdsFailEvent;
	}
	
	void adMoGoDidStartAdEvent()
	{
		Debug.Log( "adsMoGo,didStartAd");
	}
	
	void adMoGoDidReceiveAdEvent()
	{
		Debug.Log("adsMoGo,didReceiveAd");
	}
	
	void adMoGoDidFailToReceiveAdEvent(string error)
	{
		Debug.Log("adsMoGo,didFailToReceiveAd :"+error);
	}
	
	void adMoGoClickAdEvent()
	{
		Debug.Log("adsMoGo,clickAd");
	}
	
	void adMoGoDeleteAdEvent()
	{
		Debug.Log("adsMoGo,deleteAd");
	}
	
	void webBrowserWillAppearEvent()
	{
		Debug.Log("adsMoGo,webBrowserWillAppear");
	}
	
	void webBrowserDidAppearEvent()
	{
		Debug.Log("adsMoGo,webBrowserDidAppear");
	}
	
	void webBrowserWillClosedEvent()
	{
		Debug.Log("adsMoGo,webBrowserWillClosedEvent");
	}
	
	void webBrowserDidClosedEvent()
	{
		Debug.Log("adsMoGo,webBrowserDidClosed");
	}
	
	void webBrowserShareEvent(string url)
	{
		Debug.Log("adsMoGo,webBrowserShare url"+ url);
	}
	
	void adsMoGoInterstitialAdDidStartEvent()
	{
		Debug.Log("adsMoGo,interstitialAdDidStart");
	}
	
	void adsMoGoInterstitialAdIsReadyEvent()
	{
		Debug.Log("adsMoGo,interstitialAdIsReady");
	}
	
	void adsMoGoInterstitialAdReceivedRequestEvent()
	{
		Debug.Log("adsMoGo,interstitialAdReceivedRequest");
	}
	
	void adsMoGoInterstitialAdWillPresentEvent()
	{
		Debug.Log("adsMoGo,interstitialAdWillPresentEvent");
	}
	
	void adsMoGoInterstitialAdFailedWithErrorEvent(string error)
	{
		Debug.Log("adsMoGo,interstitialAdFailedWithError :" + error);
	}
	
	void adsMoGoInterstitialAdDidDismissEvent()
	{
		Debug.Log("adsMoGo,interstitialAdDidDismiss");
	}
	
	void adsMoGoWillPresentInterstitialAdModalEvent()
	{
		Debug.Log("adsMoGo,willPresentInterstitialAdModal");
	}
	
	void adsMoGoDidDismissInterstitialAdModalEvent()
	{
		Debug.Log("adsMoGo,didDismissInterstitialAdModal");
	}
	
	void adsMoGoInterstitialAdClosedEvent()
	{
		Debug.Log("adsMoGo,interstitialAdClosed");
	}
	
	void didLoadVideoAdEvent(string url)
	{
		Debug.Log ("adsMoGo,didLoadVideoAd url"+url);
	}
	
	void failLoadVideoAdEvent(string url)
	{
		Debug.Log("adsMoGo,failLoadVideoAd url"+url);
	}
	
	void didPlayVideoAdEvent(string url)
	{
		Debug.Log("adsMoGo,didPlayVideoAd url"+url);
	}
	
	void finishVideoAdEvent(string url)
	{
		Debug.Log("adsMoGo,finishVideoAd url"+url);
	}
	
	void adsMogoInterstitialAdAllAdsFailEvent(string  error){
		Debug.Log("adsMoGo,interstitialAdAllAdsFail error "+error);
	}

	void adMoGoInitFinishEvent(){
		Debug.Log ("adsMoGo,adMoGoInitFinish");
	}

	void adMoGoInterstitialInitFinishEvent(){
		Debug.Log ("adsMoGo,adMoGoInterstitialInitFinish");
	}

	void adMoGoInterstitialInMaualfreshAllAdsFailEvent(){
		Debug.Log ("adsMoGo,adMoGoInterstitialInMaualfreshAllAdsFailEvent");
	}
}
