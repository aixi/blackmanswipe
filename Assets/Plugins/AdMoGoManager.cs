using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class AdMoGoManager : MonoBehaviour {

	public const string ADS_MOGO_ID = "d4e5dbfcc1914b8a8f97f917373ac437";

	// AdMoGoEvent
	public static event Action adMoGoDidStartAdEvent;
	
	public static event Action adMoGoDidReceiveAdEvent;
	
	public static event Action<string> adMoGoDidFailToReceiveAdEvent;
	
	public static event Action adMoGoClickAdEvent;
	
	public static event Action adMoGoDeleteAdEvent;
	
	// AdMoGoWebBrowserControllerUserEvent
	public static event Action webBrowserWillAppearEvent;
		
	public static event Action webBrowserDidAppearEvent;
			
	public static event Action webBrowserWillClosedEvent;
			
	public static event Action webBrowserDidClosedEvent;
			
	public static event Action<string> webBrowserShareEvent;
			
	// AdMoGoInterstitialEvent
	public static event Action adsMoGoInterstitialAdDidStartEvent;
	
	public static event Action adsMoGoInterstitialAdIsReadyEvent;
	
	public static event Action adsMoGoInterstitialAdReceivedRequestEvent;
	
	public static event Action adsMoGoInterstitialAdWillPresentEvent;
	
	public static event Action<string> adsMoGoInterstitialAdFailedWithErrorEvent;
	
	public static event Action adsMoGoInterstitialAdDidDismissEvent;
	
	public static event Action adsMoGoWillPresentInterstitialAdModalEvent;
	
	public static event Action adsMoGoDidDismissInterstitialAdModalEvent;
	
	public static event Action adsMoGoInterstitialAdClosedEvent;
	
	public static event Action<string> didLoadVideoAdEvent;
	
	public static event Action<string> failLoadVideoAdEvent;
	
	public static event Action<string> didPlayVideoAdEvent;
	
	public static event Action<string> finishVideoAdEvent;
	
	public static event Action<string> adsMogoInterstitialAdAllAdsFailEvent;

	public static event Action adMoGoInterstitialInitFinishEvent;

	public static event Action adMoGoInterstitialInMaualfreshAllAdsFailEvent;

	public static event Action adMoGoInitFinishEvent;

	void Awake()
	{
		// Set the GameObject name to the class name for easy access from Obj-C
		gameObject.name = this.GetType().ToString();
		DontDestroyOnLoad( this );
	}

	// Use this for initialization
	void Start () {
		InvokeRepeating( "ShowAdsMogo", 0f, 200f );
	}


	public void adMoGoDidStartAd()
	{
		if( adMoGoDidStartAdEvent != null )
			adMoGoDidStartAdEvent();
	}
	
	public void adMoGoDidReceiveAd()
	{
		if(adMoGoDidReceiveAdEvent != null){
			adMoGoDidReceiveAdEvent();
		}
	}
	
	public void adMoGoDidFailToReceiveAd(string error)
	{
		if(adMoGoDidFailToReceiveAdEvent != null){
			adMoGoDidFailToReceiveAdEvent(error);
			
		}
	}
	
	public void adMoGoClickAd()
	{
		if(adMoGoClickAdEvent != null){
			adMoGoClickAdEvent();
		}
	}
	
	public void adMoGoDeleteAd()
	{
		if(adMoGoDeleteAdEvent != null){
			adMoGoDeleteAdEvent();
		}
		
	}
	
	
	public void webBrowserWillAppear()
	{
		if(webBrowserWillAppearEvent != null){
			webBrowserDidAppearEvent();
		}
	}
	
	public void webBrowserDidAppear()
	{
		if(webBrowserDidAppearEvent != null){
			webBrowserDidAppearEvent();
		}
		
	}
	
	public void webBrowserWillClosed(){
		if(webBrowserWillClosedEvent != null){
			webBrowserWillClosedEvent();
		}
	}
	
	public void webBrowserDidClosed() {
		if(webBrowserDidClosedEvent != null){
			webBrowserDidClosedEvent();
		}
		
	}
	
	public void webBrowserShare(string url){
		if(webBrowserShareEvent != null){
			webBrowserShareEvent(url);
		}
	}
	
	public void adsMoGoInterstitialAdDidStart()
	{
		if(adsMoGoInterstitialAdDidStartEvent != null)
		{
			adsMoGoInterstitialAdDidStartEvent();
		}
	}
	
	public void adsMoGoInterstitialAdIsReady()
	{
		if(adsMoGoInterstitialAdIsReadyEvent != null)
		{
			adsMoGoInterstitialAdIsReadyEvent();
		}
	}
	
	public void adsMoGoInterstitialAdReceivedRequest()
	{
		if(adsMoGoInterstitialAdReceivedRequestEvent != null)
		{
			adsMoGoInterstitialAdReceivedRequestEvent();
		}	
	}
	
	public void adsMoGoInterstitialAdWillPresent()
	{
		if(adsMoGoInterstitialAdWillPresentEvent != null)
		{
			adsMoGoInterstitialAdWillPresentEvent();
		}
		
	}
	
	public void adsMoGoInterstitialAdFailedWithError(string error)
	{
		if(adsMoGoInterstitialAdFailedWithErrorEvent != null)
		{
			adsMoGoInterstitialAdFailedWithErrorEvent(error);
		}
		
	}
	
	public void adsMoGoInterstitialAdDidDismiss()
	{
		if(adsMoGoInterstitialAdDidDismissEvent != null)
		{
			adsMoGoInterstitialAdDidDismissEvent();
		}
	}
	
	public void adsMoGoWillPresentInterstitialAdModal()
	{
		if(adsMoGoDidDismissInterstitialAdModalEvent != null)
		{
			adsMoGoDidDismissInterstitialAdModalEvent();
		}
		
	}
	
	public void adsMoGoInterstitialAdClosed()
	{
		if(adsMoGoInterstitialAdClosedEvent != null)
		{
			adsMoGoInterstitialAdClosedEvent();
		}
	}
	
	public void didLoadVideoAd(string url)
	{
		if(didLoadVideoAdEvent != null)
		{
			didLoadVideoAdEvent(url);	
		}
	}
	
	public void failLoadVideoAd(string error)
	{
		if(failLoadVideoAdEvent != null){
			failLoadVideoAdEvent(error);
		}
	}
	
	public void didPlayVideoAd(string url)
	{
		if(didPlayVideoAdEvent != null)
		{
			didPlayVideoAdEvent(url);
		}
	}
	
	public void finishVideoAd(string url)
	{
		if(finishVideoAdEvent != null)
		{
			finishVideoAdEvent(url);
		}
	}
	
	public void adsMogoInterstitialAdAllAdsFail(string url)
	{
		if(adsMogoInterstitialAdAllAdsFailEvent != null)
		{
			adsMogoInterstitialAdAllAdsFailEvent(url);
		}
	}

	
	public void adMoGoInterstitialInitFinish()
	{
		if(adMoGoInterstitialInitFinishEvent != null){

			adMoGoInterstitialInitFinishEvent();
		}
	}

	public void adMoGoInterstitialInMaualfreshAllAdsFail()
	{
		if(adMoGoInterstitialInMaualfreshAllAdsFailEvent != null){
			adMoGoInterstitialInMaualfreshAllAdsFailEvent();
		}

	}

	public void adMoGoInitFinish()
	{
		if(adMoGoInitFinishEvent!=null){
			adMoGoInitFinishEvent();
		}

	}

	public void ShowAdsMogo(){
		AdsMoGoUnity.releaseAdsMoGoView();
		AdsMoGoUnity.initAdsMoGoView(AdsMoGoUnity.AdViewType.AdViewTypeNormalBanner,ADS_MOGO_ID);
		if (Screen.height == 1136f){
			AdsMoGoUnity.setPosition(0,88);
		}
		else{
			AdsMoGoUnity.setViewPointType(AdsMoGoUnity.AdMoGoViewPointType.AdMoGoViewPointTypeTop_middle);
		}
		AdsMoGoUnity.setAdsMoGoViewHidden(false);

	}


	
}
