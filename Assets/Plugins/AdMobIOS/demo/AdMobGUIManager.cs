using UnityEngine;
using System.Collections.Generic;
using Prime31;


public class AdMobGUIManager : MonoBehaviourGUI
{
	// private const string MediationID = "4e3b0cd2aff44e97"; // ピヨすわ用
	private const string MediationID = "ca-app-pub-5435893185719828/3396368596"; //ca-app-pub-7880228280022063/2714311233 ca-app-pub-5435893185719828/3396368596
	
	#if UNITY_IPHONE
	void Start(){
		AdMobBinding.init( MediationID, false );
		InvokeRepeating( "ShowAd", 0f, 200f );
	}
	
	void ShowAd(){
		AdMobAdPosition pos;
		if (Screen.height == 1136f)
			pos =  AdMobAdPosition.IPhone5;
		else
			pos = AdMobAdPosition.TopCenter;
		AdMobBinding.destroyBanner();
		AdMobBinding.createBanner( AdMobBannerType.iPhone_320x50, pos );
	}
	#endif
}

