using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class AdsMoGoUnity 
{

	// Use this for initialization
	public enum AdViewType{
		AdViewTypeUnknown          = 0,  //error
		AdViewTypeNormalBanner     = 1,  //e.g. 320 * 50 ; 320 * 48...           iphone banner
    	AdViewTypeLargeBanner      = 2,  //e.g. 728 * 90 ; 768 * 110             ipad only
    	AdViewTypeMediumBanner     = 3,  //e.g. 468 * 60 ; 508 * 80              ipad only
   	 	AdViewTypeRectangle        = 4,  //e.g. 300 * 250; 320 * 270             ipad only
    	AdViewTypeSky              = 5,  //Don't support
    	AdViewTypeFullScreen       = 6,  //iphone full screen ad
    	AdViewTypeVideo            = 11, //iPad and iPhone use video ad
    	AdViewTypeiPadNormalBanner = 8,  //ipad use iphone banner
    	AdViewTypeiPadFullScreen   = 9,  //ipad full screen ad e.g. 1024*768     ipad only
    	AdViewTypeCustomSize       = 10, //iPad and iPhone use custom size
    	AdViewTypeSplash           = 12,
		AdViewTypeiPhoneRectangel  = 14, //iPhone rectangel
	}

	public enum AdMoGoViewPointType{
		AdMoGoViewPointTypeTop_left       = 0,
		AdMoGoViewPointTypeTop_middle     = 1,
		AdMoGoViewPointTypeTop_right      = 2,
		
		AdMoGoViewPointTypeMiddle_left    = 3,
		AdMoGoViewPointTypeMiddle_middle  = 4,
		AdMoGoViewPointTypeMiddle_right   = 5,
		
		AdMoGoViewPointTypeDown_left      = 6,
		AdMoGoViewPointTypeDown_middle    = 7,
		AdMoGoViewPointTypeDown_right     = 8,
		AdMoGoViewPointTypeCustom         = 9,

	}
	
	[DllImport ("__Internal")]
	private static extern void _initAdsMoGoView(string mogokey,AdViewType adtype);

	[DllImport ("__Internal")]
	private static extern void _initAdsMoGoViewManualFresh(string mogokey,AdViewType adtype);

	[DllImport ("__Internal")]
	private static  extern void _Setposition(float x,float y);
	
	[DllImport ("__Internal")]
	private static  extern float _Getposition_X();
	
	[DllImport ("__Internal")]
	private static  extern float _Getposition_Y();
	
	[DllImport ("__Internal")]
	private static  extern void _SetAdsMoGoViewHidden(bool ishidden);
	
	[DllImport ("__Internal")]
	private static  extern bool _GetAdsMoGoViewhidden();
	
	[DllImport ("__Internal")]
	private static  extern void _releaseAdsMoGoView();


	[DllImport ("__Internal")]
	private static  extern void _refreshAd();





	[DllImport ("__Internal")]
	private static  extern void _setViewPointType(AdMoGoViewPointType viewPointType);

	[DllImport ("__Internal")]
	private static extern int _getViewPointType();
	
	
	[DllImport ("__Internal")]
	private static extern void _initAdsMoGoInterstitial(AdViewType adtype,string mogokey);

	[DllImport ("__Internal")]
	private static extern void _initAdsMoGoInterstitialManual(AdViewType adtype,string mogokey);
	
	[DllImport ("__Internal")]
	private static extern void _presentAdsMoGoInterstitial(AdViewType adtype,string mogokey,bool isWait);
		
	[DllImport ("__Internal")]
	private static extern void _cancelAdsMoGoInterstitial(AdViewType adtype,string mogokey);
	
	[DllImport ("__Internal")]	
	private static extern void _releaseAdsMoGoInterstitial(AdViewType adtype,string mogokey);

	[DllImport ("__Internal")]
	private static extern void _refreshAdsMoGoInterstitialAd(AdViewType adtype,string mogokey);
	
	public static  void initAdsMoGoView(AdViewType adtype,string mogokey)
	{
		// Call plugin only when running on real device
		if (Application.platform != RuntimePlatform.OSXEditor){
			switch(adtype){
			case AdViewType.AdViewTypeNormalBanner:
				_initAdsMoGoView(mogokey,AdViewType.AdViewTypeNormalBanner);
				break;
			case AdViewType.AdViewTypeLargeBanner:
				_initAdsMoGoView(mogokey,AdViewType.AdViewTypeLargeBanner);
				break;
			case AdViewType.AdViewTypeMediumBanner:
				_initAdsMoGoView(mogokey,AdViewType.AdViewTypeMediumBanner);
				break;
			case AdViewType.AdViewTypeRectangle:
				_initAdsMoGoView(mogokey,AdViewType.AdViewTypeRectangle);
				break;
			case AdViewType.AdViewTypeiPadNormalBanner:
				_initAdsMoGoView(mogokey,AdViewType.AdViewTypeiPadNormalBanner);
				break;
			case AdViewType.AdViewTypeiPhoneRectangel:
				_initAdsMoGoView(mogokey,AdViewType.AdViewTypeiPhoneRectangel);
				break;
			default:
				break;
			}
		}
	}

	public static void initAdsMoGoViewInManualFresh(AdViewType adtype,string mogokey)
	{
		// Call plugin only when running on real device
		if (Application.platform != RuntimePlatform.OSXEditor){
			switch(adtype){
			case AdViewType.AdViewTypeNormalBanner:
				_initAdsMoGoViewManualFresh(mogokey,AdViewType.AdViewTypeNormalBanner);
				break;
			case AdViewType.AdViewTypeLargeBanner:
				_initAdsMoGoViewManualFresh(mogokey,AdViewType.AdViewTypeLargeBanner);
				break;
			case AdViewType.AdViewTypeMediumBanner:
				_initAdsMoGoViewManualFresh(mogokey,AdViewType.AdViewTypeMediumBanner);
				break;
			case AdViewType.AdViewTypeRectangle:
				_initAdsMoGoViewManualFresh(mogokey,AdViewType.AdViewTypeRectangle);
				break;
			case AdViewType.AdViewTypeiPadNormalBanner:
				_initAdsMoGoViewManualFresh(mogokey,AdViewType.AdViewTypeiPadNormalBanner);
				break;
			case AdViewType.AdViewTypeiPhoneRectangel:
				_initAdsMoGoViewManualFresh(mogokey,AdViewType.AdViewTypeiPhoneRectangel);
				break;
			default:
				break;
			}
		}
	}
	
	public static  void setPosition(float x,float y){
		if (Application.platform != RuntimePlatform.OSXEditor)
			_Setposition(x,y);
	}
	
	public static  float[] getPosition(){
		float[] positions= new float[2];
		positions[0] = _Getposition_X();
		positions[1] = _Getposition_Y();
		return positions;
	}
	
	public static  void setAdsMoGoViewHidden(bool ishidden){
		if (Application.platform != RuntimePlatform.OSXEditor)
			_SetAdsMoGoViewHidden(ishidden);
	}
	
	public static   bool getAdsMoGoViewHidden(){
		return _GetAdsMoGoViewhidden();
	}
	
	public static  void releaseAdsMoGoView(){
		if (Application.platform != RuntimePlatform.OSXEditor)
			_releaseAdsMoGoView();
	}

	public static void  refreshAd (){
		 _refreshAd();
	}



	public static void setViewPointType(AdMoGoViewPointType viewPointType){
		if (Application.platform != RuntimePlatform.OSXEditor)
			_setViewPointType(viewPointType);
	}
	
	public static AdMoGoViewPointType getViewPointType(){
		if (Application.platform != RuntimePlatform.OSXEditor) {
						int i = _getViewPointType ();
						switch (i) {
						case 0:
								return AdMoGoViewPointType.AdMoGoViewPointTypeTop_left;
						case 1:
								return AdMoGoViewPointType.AdMoGoViewPointTypeTop_middle;
						case 2:
								return AdMoGoViewPointType.AdMoGoViewPointTypeTop_right;
						case 3:
								return AdMoGoViewPointType.AdMoGoViewPointTypeMiddle_left;
						case 4:
								return AdMoGoViewPointType.AdMoGoViewPointTypeMiddle_middle;
						case 5:
								return AdMoGoViewPointType.AdMoGoViewPointTypeMiddle_right;
						case 6:
								return AdMoGoViewPointType.AdMoGoViewPointTypeDown_left;
						case 7:
								return AdMoGoViewPointType.AdMoGoViewPointTypeDown_middle;
						case 8:
								return AdMoGoViewPointType.AdMoGoViewPointTypeDown_right;
						case 9:
								return AdMoGoViewPointType.AdMoGoViewPointTypeCustom;
						default:
								return  AdMoGoViewPointType.AdMoGoViewPointTypeCustom;
						}	
				} else {
			           return AdMoGoViewPointType.AdMoGoViewPointTypeCustom;
				}
	}
	
	public static  void initAdsMoGoInterstitial(AdViewType adtype,string mogokey){
		if (Application.platform != RuntimePlatform.OSXEditor)
			switch(adtype){
			case AdViewType.AdViewTypeFullScreen:
			_initAdsMoGoInterstitial(AdViewType.AdViewTypeFullScreen,mogokey);
				break;
			case AdViewType.AdViewTypeiPadFullScreen:
			_initAdsMoGoInterstitial(AdViewType.AdViewTypeiPadFullScreen,mogokey);
				break;
			case AdViewType.AdViewTypeVideo:
			_initAdsMoGoInterstitial(AdViewType.AdViewTypeVideo,mogokey);
				break;
			default:
				break;
		}
	}

	public static void initAdsMoGoInterstitialInManualFresh(AdViewType adtype,string mogokey){
		if (Application.platform != RuntimePlatform.OSXEditor)
		switch(adtype){
			case AdViewType.AdViewTypeFullScreen:
			_initAdsMoGoInterstitialManual(AdViewType.AdViewTypeFullScreen,mogokey);
			break;
			case AdViewType.AdViewTypeiPadFullScreen:
			_initAdsMoGoInterstitialManual(AdViewType.AdViewTypeiPadFullScreen,mogokey);
			break;
			case AdViewType.AdViewTypeVideo:
			_initAdsMoGoInterstitialManual(AdViewType.AdViewTypeVideo,mogokey);
			break;
			default:
			break;
		}
	}

	public static void freshAdsMoGoInterstitial(AdViewType adtype,string mogokey){
		_refreshAdsMoGoInterstitialAd(adtype,mogokey);
	}

	
	public static void presentAdsMoGoInterstitial(AdViewType adtype,string mogokey,bool isWait)
	{
		if (Application.platform != RuntimePlatform.OSXEditor)
			_presentAdsMoGoInterstitial(adtype,mogokey,isWait);
	}


	public static void cancelAdsMoGoIntersitial(AdViewType adtype,string mogokey){
		if (Application.platform != RuntimePlatform.OSXEditor)
			_cancelAdsMoGoInterstitial(adtype,mogokey);
	}
	
	public static void releaseInterstitial(AdViewType adtype,string mogokey)
	{
		if (Application.platform != RuntimePlatform.OSXEditor)
			_releaseAdsMoGoInterstitial(adtype,mogokey);
	}
	
	
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
}



