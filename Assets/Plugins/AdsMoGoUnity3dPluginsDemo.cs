using UnityEngine;
using System.Collections;

	
public class AdsMoGoUnity3dPluginsDemo : MonoBehaviour {
	
	public Texture Button0;

	public Texture Button1;
	
	
	
	void OnGUI() {

        //AdsMoGoUnity adsmogo;
        if(GUI.Button(new Rect(0,44,120,60),"init banner"))
		{
			
			AdsMoGoUnity.initAdsMoGoView(AdsMoGoUnity.AdViewType.AdViewTypeNormalBanner,"4b298ad419e24ae191501d73a004bb0e");
//			AdsMoGoUnity.setPosition(0.0f,200.0f);
			AdsMoGoUnity.setViewPointType(AdsMoGoUnity.AdMoGoViewPointType.AdMoGoViewPointTypeDown_middle);
   		}  
		
		if(GUI.Button(new Rect(150,44,120,60),"show banner"))
		{
			AdsMoGoUnity.setAdsMoGoViewHidden(false);
   		}
		
		if(GUI.Button(new Rect(0,110,120,60),"hidden banner"))
		{
			AdsMoGoUnity.setAdsMoGoViewHidden(true);
   		}
		
		if(GUI.Button(new Rect(150,110,120,60),"release banner"))
		{
			AdsMoGoUnity.releaseAdsMoGoView();
   		}
		
		// AdsMoGoUnity interstitial
		if(GUI.Button(new Rect(300,44,120,60),"init interstitial"))
		{
			AdsMoGoUnity.initAdsMoGoInterstitial(AdsMoGoUnity.AdViewType.AdViewTypeFullScreen,"4b298ad419e24ae191501d73a004bb0e");
   		}
		
		if(GUI.Button(new Rect(450,44,120,60),"show interstitial"))
		{
			AdsMoGoUnity.presentAdsMoGoInterstitial(AdsMoGoUnity.AdViewType.AdViewTypeFullScreen,"4b298ad419e24ae191501d73a004bb0e",true);
   		}

		if(GUI.Button(new Rect(300,110,120,60),"cancel interstitial")){
			AdsMoGoUnity.cancelAdsMoGoIntersitial(AdsMoGoUnity.AdViewType.AdViewTypeFullScreen,"4b298ad419e24ae191501d73a004bb0e");
		}
		
		if(GUI.Button(new Rect(450,110,120,60),"release interstitial"))
		{
			AdsMoGoUnity.releaseInterstitial(AdsMoGoUnity.AdViewType.AdViewTypeFullScreen,"4b298ad419e24ae191501d73a004bb0e");
   		}

		if(GUI.Button(new Rect(400,44,120,60),"hand refresh"))
		{
			AdsMoGoUnity.refreshAd();
		}
	}
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
