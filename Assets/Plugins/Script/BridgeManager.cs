﻿using UnityEngine;
using System.Collections;

public class BridgeManager : MonoBehaviour {

	void Start () {
		IOSBridge.Init ();
	}

	void OnGUI(){

		int btnWidth = Screen.width / 4;
		int btnHieght = Screen.height / 10;

		if (GUI.Button (new Rect (
				((Screen.width / 2) - btnWidth/2), 
				((Screen.height / 2) - btnHieght/2), 
				btnWidth, 
				btnHieght), 
		        "Click")) 
		{
#if UNITY_IOS
			IOSBridge.SharedText_Moment("黑人碰到大麻烦啦！谁快来救救他\nhttps://itunes.apple.com/cn/app/jue-wang!-yu-zhi-he-mei-qiu/id892050338");
		
#endif
		}
	}

	public void UserFeedBack(string id){
		Debug.Log ("Unity ID : " + id);
	}
}
