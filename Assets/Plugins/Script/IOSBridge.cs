﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class IOSBridge 
{
	[DllImport ("__Internal")]	
	private static extern void _AddNotification(
		string title,
		string body,
		string cancelLabel,
		string fisrtLabel,
		string secondLabel);
	public static void AddNotification(
		string title,
		string body,
		string cancelLabel,
		string fisrtLabel,
		string secondLabel)
	{
		_AddNotification(
			  title,
			  body,
			  cancelLabel,
			  fisrtLabel,
			  secondLabel);
	}

	[DllImport ("__Internal")]
	private static extern void _Init();
	public static void Init(){
		_Init ();
	}

	[DllImport ("__Internal")]
	private static extern void _SharedTextMoment(string txtMessage);
	public static void SharedText_Moment(string _txtMessage){
		_SharedTextMoment (_txtMessage);
	}

	
}
