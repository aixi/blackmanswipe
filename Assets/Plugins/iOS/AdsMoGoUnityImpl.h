//
//  AdsMoGoUnityImpl.h
//  AdsMoGoUnity
//
//  Created by MOGO on 12-11-6.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdMoGoDelegateProtocol.h"
#import "AdMoGoWebBrowserControllerUserDelegate.h"
@interface AdsMoGoUnityImpl : NSObject<AdMoGoDelegate,AdMoGoWebBrowserControllerUserDelegate>

@end
