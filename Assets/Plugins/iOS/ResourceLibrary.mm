#import <Foundation/Foundation.h>
#import "Guid.h"

#define MakeStringCopy( _x_ ) ( _x_ != NULL && [_x_ isKindOfClass:[NSString class]] ) ? strdup( [_x_ UTF8String] ) : NULL

extern "C"{
    const char* _GetAppName(){
        return MakeStringCopy( [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleDisplayName"] );
    }
    const char* _GetAppVersion(){
        return MakeStringCopy( [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleVersion"] );
    }
    const char* _GetIdentifierForVendor(){
        return MakeStringCopy( [[UIDevice currentDevice].identifierForVendor UUIDString] );
    }
    
    const char* _GetLocaleIdentifier(){
        return MakeStringCopy( [[NSLocale currentLocale] objectForKey:NSLocaleIdentifier] );
    }
    
    const char* _GetGUID(){
        return MakeStringCopy( [Guid randomGuid].description );
    }
}
