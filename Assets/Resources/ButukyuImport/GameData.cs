﻿using UnityEngine;
using System.Collections;


//ゲーム進行に関わるパラメータを管理するクラス
public class GameData {
		
	public static int pitcher = 5;//選択されたピッチャーのID
	public static int batter = 0;//選択されたバッターのID
	public static int try_times = 10;//残り球数
	public static float distance = 0;//ボールの飛距離
	public static int num_HR = 0;//1ゲーム中にホームランを打った回数
	public static int num_Hit= 0;
	
	public static int volume_Sound = 1;
	
	public static void Reset(){
		try_times = 10;
		distance = 10;
		num_HR = 0;
		num_Hit = 0;
	}
	
}
