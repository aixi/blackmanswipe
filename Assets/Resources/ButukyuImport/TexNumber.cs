using UnityEngine;
using System.Collections;

public class TexNumber {
	private Texture tex;
	
	private Rect[] rect_num_Big_InTex;
	private Rect[] rect_num_Red_InTex;
	private Rect[] rect_num_Blue_InTex;
	
	private Texture tex_pop;
	private Rect[] rect_num_Pop_InTex;
	
	private Rect rect_Slash;
	private Rect rect_Meter;
	private Rect rect_Combo;
	private Rect rect_Plus;
	private Rect rect_Total;
	private Rect rect_Ten;
	private Rect rect_Kakeru;
	
	
	public TexNumber(){
		tex = (Texture)Resources.Load("PiyoPiyo/Texture308/no_txt");
		tex_pop = (Texture)Resources.Load("PiyoPiyo/Texture310/score_font");
		rect_num_Big_InTex = new Rect[10];
		rect_num_Red_InTex = new Rect[10];
		rect_num_Blue_InTex = new Rect[10];
		rect_num_Pop_InTex = new Rect[10];
		SetUp();
	}
	
	public void Show_Big(int num, Rect rect){
		GUI.DrawTextureWithTexCoords(rect, tex, rect_num_Big_InTex[num]);
	}
	
	public void Show_Red(int num, Rect rect){
		GUI.DrawTextureWithTexCoords(rect, tex, rect_num_Red_InTex[num]);
	}
	
	public void Show_Blue(int num, Rect rect){
		GUI.DrawTextureWithTexCoords(rect, tex, rect_num_Blue_InTex[num]); 
	}
	
	public void Show_Pop(int num, Rect rect){
		GUI.DrawTextureWithTexCoords(rect, tex_pop, rect_num_Pop_InTex[num]); 
	}
	
	public void Show_Slash(Rect rect){
		GUI.DrawTextureWithTexCoords(rect, tex, rect_Slash); 
	}
	
	public void Show_Meter(Rect rect){
		GUI.DrawTextureWithTexCoords(rect, tex, rect_Meter); 
	}
	
	public void Show_Combo(Rect rect){
		GUI.DrawTextureWithTexCoords(rect, tex, rect_Combo); 
	}
	
	public void Show_Plus(Rect rect){
		GUI.DrawTextureWithTexCoords(rect, tex, rect_Plus);	
	}
	
	public void Show_Total(Rect rect){
		GUI.DrawTextureWithTexCoords(rect, tex, rect_Total);	
	}
	
	public void Show_Ten(Rect rect){
		GUI.DrawTextureWithTexCoords(rect, tex, rect_Ten);
	}
	
	public void Show_Kakeru(Rect rect){
		GUI.DrawTextureWithTexCoords(rect, tex, rect_Kakeru);
	}
	
	public void SetUp(){
		for(int i=0;i<rect_num_Big_InTex.Length;i++){
			rect_num_Big_InTex[i] = new Rect((i % 4) * 0.23f, (-i / 4) * 0.25f + 0.525f, 0.23f, 0.25f);
			
			rect_num_Red_InTex[i] = new Rect(0.005f + 0.0886f * i, 0.774f, 0.09f, 0.113f);
			
			rect_num_Blue_InTex[i] = new Rect(0.005f + 0.0887f * i, 0.89f, 0.09f, 0.113f);
		}
		
		for(int i=0;i < rect_num_Pop_InTex.Length;i++){
			rect_num_Pop_InTex[i] = new Rect(0.02f+(i%5)*0.194f,0.7f + (-i/5)*0.3f, 0.19f, 0.3f);
		}
		
		rect_Meter = new Rect(0.9f, 0.774f, 0.1f, 0.113f);
		rect_Slash = new Rect(0.893f, 0.88f, 0.09f, 0.113f);
		rect_Combo = new Rect(0.48f, 0.125f, 0.42f, 0.125f);
		rect_Total = new Rect(0.625f, 0.02f, 0.29f, 0.113f);
		rect_Plus = new Rect(0.495f, 0.02f, 0.09f, 0.113f);
		rect_Ten = new Rect(0.915f,0.02f,0.05f,0.113f);
		rect_Kakeru = new Rect(0.9f, 0.125f, 0.09f, 0.125f);
	}
	
	public void ShowRankBig( int score, int digit, Rect rect){
		while(true){
			if(digit == 0)
				break;
			
			Show_Big(score/(int)Mathf.Pow(10, digit-1), new Rect(rect.x - rect.width * (digit - 1) * 0.8f, rect.y, rect.width, rect.height));
			score -= (score/(int)Mathf.Pow(10, digit-1)) * (int)Mathf.Pow(10, digit-1);
			digit--;
			
			
			
		}
	}
	public void ShowRankBigZero( int score, int digit, Rect rect, int needDigit){
		if(needDigit <= 0)
			return;
		
		if(needDigit < digit){
			while(true){
				if(needDigit == 0)
					break;
				Show_Big(9, new Rect(rect.x - rect.width * (needDigit - 1) * 0.9f, rect.y, rect.width, rect.height));
				needDigit--;
			}
		}else{
		
			while(true){
				
				if(needDigit == 0)
					break;
				if(needDigit > digit){
					Show_Big(0, new Rect(rect.x - rect.width * (needDigit - 1) * 0.9f, rect.y, rect.width, rect.height));
				}else{
					Show_Big(score/(int)Mathf.Pow(10, digit-1), new Rect(rect.x - rect.width * (digit - 1) * 0.9f, rect.y, rect.width, rect.height));
					score -= (score/(int)Mathf.Pow(10, digit-1)) * (int)Mathf.Pow(10, digit-1);
					digit--;
				}
					
				needDigit--;
				
				
				
			}
		}
	}
	public void ShowRankBlueZero( int score, int digit, Rect rect, int needDigit, bool isCenter){
		Rect r = new Rect(rect.x - rect.width * (needDigit - 1) * 0.9f, rect.y, rect.width, rect.height);	
		
		if(needDigit <= 0)
			return;
		
		
		if(needDigit < digit){
			while(true){
				if(needDigit == 0)
					break;
				Show_Blue(9, new Rect(rect.x - rect.width * (needDigit - 1) * 0.9f, rect.y, rect.width, rect.height));
				needDigit--;
			}
		}else{
		
			while(true){
				
				if(needDigit == 0)
					break;
				if(needDigit > digit){
					if(isCenter)
						r.x += r.width * 0.9f / 2;
					//Show_Blue(0, new Rect(rect.x - rect.width * (needDigit - 1) * 0.9f, rect.y, rect.width, rect.height));
				}else{
					if(isCenter){
						Show_Blue(score/(int)Mathf.Pow(10, digit-1), r);
						r.x += r.width * 0.9f;
					}else
						Show_Blue(score/(int)Mathf.Pow(10, digit-1), new Rect(rect.x - rect.width * (needDigit - 1) * 0.9f, rect.y, rect.width, rect.height));
					score -= (score/(int)Mathf.Pow(10, digit-1)) * (int)Mathf.Pow(10, digit-1);
					digit--;
				}
					
				needDigit--;
				
				
				
			}
		}
	}
	
	
	public void ShowRankRed( int score, int digit, Rect rect){
		while(true){
			if(digit == 0)
				break;
			
			
			Show_Red(score/(int)Mathf.Pow(10, digit-1), new Rect(rect.x - rect.width * (digit - 1), rect.y, rect.width, rect.height));
			
			score -= (score/(int)Mathf.Pow(10, digit-1)) * (int)Mathf.Pow(10, digit-1);
			digit--;
			
			
			
		}
	}
	
	public void ShowRankRedZero( int score, int digit, Rect rect, int needDigit){
		if(needDigit <= 0)
			return;
		
		if(needDigit < digit){
			while(true){
				if(needDigit == 0)
					break;
				Show_Red(9, new Rect(rect.x - rect.width * (needDigit - 1) * 0.9f, rect.y, rect.width, rect.height));
				needDigit--;
			}
		}else{
		
			while(true){
				
				if(needDigit == 0)
					break;
				if(needDigit > digit){
					//Show_Red(0, new Rect(rect.x - rect.width * (needDigit - 1) * 0.9f, rect.y, rect.width, rect.height));
				}else{
					Show_Red(score/(int)Mathf.Pow(10, digit-1), new Rect(rect.x - rect.width * (digit - 1) * 0.9f, rect.y, rect.width, rect.height));
					score -= (score/(int)Mathf.Pow(10, digit-1)) * (int)Mathf.Pow(10, digit-1);
					digit--;
				}
					
				needDigit--;
				
				
				
			}
		}
	}
	
	public void ShowRankBlue( int score, int digit, Rect rect){
		while(true){
			if(digit == 0)
				break;
			
			Show_Blue(score/(int)Mathf.Pow(10, digit-1), new Rect(rect.x - rect.width * (digit - 1), rect.y, rect.width, rect.height));
			
			score -= (score/(int)Mathf.Pow(10, digit-1)) * (int)Mathf.Pow(10, digit-1);
			digit--;
			
			
			
		}
	}
	
	public void ShowRankPop( int score, int digit, Rect rect){
		while(true){
			if(digit == 0)
				break;
			
			Show_Pop(score/(int)Mathf.Pow(10, digit-1), new Rect(rect.x - rect.width * (digit - 1), rect.y, rect.width, rect.height));
			
			score -= (score/(int)Mathf.Pow(10, digit-1)) * (int)Mathf.Pow(10, digit-1);
			digit--;
			
			
			
		}
	}
	
	public int ReturnDigit(int score){
		int s=0;
		while(true){
			score /= 10;
			s++;
			if(score == 0){
				break;
			}
		}
		return s;
	}
}
