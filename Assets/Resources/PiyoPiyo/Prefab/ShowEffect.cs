﻿using UnityEngine;
using System.Collections;


//各球の結果を表示させるためのエフェクトを管理するクラス
public class ShowEffect : MonoBehaviour {
	
	ParticleSystem ps;
	// Use this for initialization
	void Start () {
		ps = GetComponent<ParticleSystem>();
	}
	
	void Update(){
		if(!ps.IsAlive())
			Destroy(gameObject);
	}
}
