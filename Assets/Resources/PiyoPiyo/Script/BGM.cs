﻿using UnityEngine;
using System.Collections;

public class BGM : MonoBehaviour {

	// Use this for initialization
	void Start () {
		//DontDestroyOnLoad(gameObject);
	}
	
	
	public void Stop(){
		//Destroy(gameObject);
		audio.Stop();
	}
	
	public void Pause(){
		audio.pitch = 0;
	}
	
	public void PlayBGM(){
		if (!audio.isPlaying) audio.Play();
		audio.pitch = 1;
	}
}
