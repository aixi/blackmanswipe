using UnityEngine;
using System.Collections;

public class CameraWork : MonoBehaviour {
	Vector3 angle_Default;
	float mv = 1.5f;
	public bool fl_move;
	
	float limit = 1;
	
	float timer;
	
	// Use this for initialization
	void Start () {
		angle_Default = transform.eulerAngles;
	}
	
	// Update is called once per frame
	void Update () {
		if(fl_move){
			
			if(transform.eulerAngles.z >= angle_Default.z + limit || transform.eulerAngles.z <= angle_Default.z - limit){
				mv *= -1;
			}
			transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z+mv);
			
			if(timer < 0)
				Fl_Off_Moving();
			else
				timer -= 0.015f;
		}
	}
	
	public void Fl_On_Moving(){
		fl_move = true;
		GameObject.FindWithTag("GameController").SendMessage("ShakeMissCount");
		timer = 0.2f;
		//Invoke("Fl_Off_Moving", 1.0f);
	}
	
	public void Fl_Off_Moving(){
		fl_move = false;
		if(PiyoPiyoController.state != "stop") { // ポーズ中は動き出さないように
			Time.timeScale = 1;
		}
		transform.eulerAngles = angle_Default;
		GameObject.FindWithTag("GameController").SendMessage("MissCountDown");
	}
}
