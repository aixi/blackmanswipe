﻿using UnityEngine;
using System.Collections;

public class DestroyTrigge : MonoBehaviour {
	public PiyoPiyoController controller;
	
	void Start(){
		controller = GameObject.FindWithTag("GameController").GetComponent<PiyoPiyoController>();
	}

	void OnTriggerEnter(Collider info){
		if(info.gameObject.tag == "Target"){
	//		Debug.Log(info.gameObject.tag);
			info.gameObject.tag = "UnTarget";
			//controller.Judge(false, info.gameObject.GetComponent<PiyoPiyo>().piyoData.speedLevel,  info.gameObject.transform.position);
			controller.Judge(false, info.gameObject.GetComponent<blackman>().piyoData.speedLevel,  info.gameObject.transform.position);
			Destroy(info.gameObject);
		}
		
		if(info.gameObject.tag == "UnTarget"){
	//		Debug.Log(info.gameObject.tag);
			Destroy(info.gameObject);
		}
		
		if(info.gameObject.tag == "Board"){
	//		Debug.Log(info.gameObject.tag);
			Destroy(info.gameObject);
		}
	}
}
