﻿using UnityEngine;
using System.Collections;

public class Field : MonoBehaviour {
	
	public bool fl_LeftField;
	public Vector3 pos_FieldCenter;
	public PiyoPiyoController piyoController;
	
	void Start(){
		piyoController = GameObject.FindWithTag("GameController").GetComponent<PiyoPiyoController>();
	}

	void OnCollisionEnter(Collision info){
		if(info.gameObject.tag == "Target"){
			info.gameObject.SendMessage("OnField", pos_FieldCenter);
			
			piyoController.Judge(fl_LeftField, info.gameObject.GetComponent<PiyoPiyo>().piyoData.speedLevel, pos_FieldCenter);
			info.gameObject.tag = "UnTarget";
		}
	}
}
