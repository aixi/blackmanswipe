﻿// #define DEBUG_GUI
#define WITH_FACEBOOK // Facebook共有ありなら定義

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[ExecuteInEditMode]
public class GUIController : MonoBehaviour
{
#if DEBUG_GUI
	public bool fl_debug_disp = true;
#endif// DEBUG_GUI

		private PiyoPiyoController piyopiyoController;
	
		public Texture[] tex_num;
	
		//ゲーム開始時のカウントダウン用
		public Texture tex_Mask_Before_Start;
		public Texture tex_Condition_Base;
		public Texture tex_txt_Condition_2;
		public Texture tex_txt_Condition_2_forLastStage;
		private Texture tex_CountDown_Show;
		public Texture[] tex_CountDown_Start;
	
		//ゲーム画面以外用背景
		public Texture tex_Stop_Back;
	
		//クリアー・ゲームオーバー画面用
		private float alpha = 0;
		public Texture[] texs_piyo_cutin;
		public Texture[] texs_piyo_txt_All;
		public Rect[] rects_piyo_txt;
		public Rect rect_piyo_txt;
		public int rnd_piyo_cutin;
		public Texture tex_yoropiyo;
		public Texture tex_piyo_cutin;
		public Texture tex_piyo_txt;
		public Texture tex_yoropiyo_txt;
		private bool fl_Show_Result;
		private bool fl_Show_Help;
		private bool fl_FaceBook;
		private bool fl_Wechat;
		public Texture[] texs_Comment;
		public Texture tex_Clear_Logo;
		public Texture tex_Clear_Logo_forLastStage;
		public Texture tex_Score_Logo;
		public Texture tex_ResultScore_Logo;
		public Texture tex_Best_Logo;
		public Texture tex_Rank_Logo;
		public Texture tex_NewRecord_Logo;
		public Texture tex_GameOver_Logo;
		public Texture tex_GameOver_Mask;
		public Texture tex_GameOver_Piyo;
		public Texture tex_GameOver_HelpPiyo;
		public GUIStyle style_Ok_Bt;
		public float _a;
		public float _x;
		public Texture tex_OK;
		public Texture tex_txtMask;
		public Texture tex_Release_txt;
		public Texture tex_Release_txt_forLastStage;
	
	
		private TexNumber texNumber;
		private TexTxt texTxt;
		public Texture tex_Stock;
		public Texture[] texs_Stage;
	
	
		private bool possibleTouch;
	
	
		public GUIStyle style_Pause;
	
		public bool fl_PointShow;
		public int point;
		public Vector3 pos_point;
		public float ratio;
		public float ratio_point;
		public float ratio_inc;
	
		public int combo_Bonus;

		private bool gameover_cutin_appeared;
	
		public bool fl_StageClear;
		private float pos_x_clear_logo;
		public Texture tex_clear_tassei;
		private float size_tassei = 0;
		private bool size_up = true;
	
	
		public GUIStyle style_GameOver_Select;
		public GUIStyle style_GameOver_Retry;
		public GUIStyle style_GameOver_Next;
	
		public Texture tex_Pause_txt;
		public GUIStyle gui_Stop_Resume;
		public GUIStyle tex_Pause_Retry_txt;
		public GUIStyle gui_Stop_Retry;
		public GUIStyle tex_Pause_Sound_txt;
		public GUIStyle gui_Sound;
		public Texture tex_Pause_Sound_on;
		public Texture tex_Pause_Sound_off;
	
		public GUIStyle tex_Pause_Return_txt;
		public GUIStyle gui_Stop_Top;

		private bool[] fl_countDown_se_played = {false, false, false, false};
	
		public float y_miss_Stock = 0;
		public float x_miss_Stock = 0;
		public float mx = 0.003f;
		private int miss_count_for_Show;
	
		private int next_Stage;
	
		//public Texture tex_Facebook;
		public Texture tex_Wechat;
	
		//public Texture tex_bt_Result;
		public GUIStyle style_bt_Result;
		public GUIStyle style_bt_Result_Assist;

		private string icon_path;
		private string sns_message;
		//public Vector2 pos_Facebook;
		//public Vector2 pos_Twitter;
		private Vector2 pos_Facebook = new Vector2 (128, 578);
		private Vector2 pos_Wechat = new Vector2 (32, 578);
	
//リザルト画面へ進みますか確認画面素材
		public Texture tex_resultComfirm_window;
		public Texture tex_resultComfirm_text;
		public Texture tex_resultComfirm_yesnotext;
		public GUIStyle style_bt_resultComfirm_yes;
		public GUIStyle style_bt_resultComfirm_no;
	
//リザルト画面へ進みますか確認画面表示フラグ
		public bool fl_resultComfirm = true;
//リザルト画面へ進みますかボタンの点滅間隔
		private float timer_resultComfirm_switch;
		private float time_resultComfirm_switch = 0.5f;
		private bool fl_resultComfirm_show = true;
//リザルト画面へ進みますかボタンの点滅時間
		private float time_resultComfirm_long = 5f;
		private float timer_resultComfirm_long;
	
		void Start ()
		{
				int sw = FixedScreen.width;
				int sh = FixedScreen.height;
		
				fl_Show_Result = false;
				fl_Show_Help = false;
				alpha = 0;
				gameover_cutin_appeared = false;
				pos_x_clear_logo = FixedScreen.width * 1.1f;
				texNumber = new TexNumber ();
				texTxt = ResidentResources.GetTexTxt ();
				ratio = 0;
				piyopiyoController = GetComponent<PiyoPiyoController> ();	
				possibleTouch = true;
		
				combo_Bonus = 0;
		
				rnd_piyo_cutin = Random.Range (0, texs_piyo_cutin.Length);
				tex_piyo_cutin = texs_piyo_cutin [rnd_piyo_cutin];
		
				if (PiyoPiyoController.selectedStage <= 2) {
						tex_piyo_txt = texs_piyo_txt_All [Random.Range (5, 10)];
						tex_yoropiyo_txt = texs_piyo_txt_All [Random.Range (0, 2)];
				} else if (PiyoPiyoController.selectedStage <= 3) {
						tex_yoropiyo_txt = texs_piyo_txt_All [Random.Range (0, 2)];
						tex_piyo_txt = texs_piyo_txt_All [Random.Range (5, 12)];
				} else if (PiyoPiyoController.selectedStage <= 4) {
						tex_yoropiyo_txt = texs_piyo_txt_All [Random.Range (0, 4)];
						tex_piyo_txt = texs_piyo_txt_All [Random.Range (5, 12)];
				} else {
						tex_yoropiyo_txt = texs_piyo_txt_All [Random.Range (0, 5)];
						tex_piyo_txt = texs_piyo_txt_All [Random.Range (5, texs_piyo_txt_All.Length)];
				}
		
				rects_piyo_txt = new Rect[]{OffsetRect.GetOffsetRect (sw * 1.1f - tex_piyo_txt.width, -sh * 0.1f, tex_piyo_txt.width, tex_piyo_txt.height),
			OffsetRect.GetOffsetRect (-sw * 0.1f, -sh * 0.1f, tex_piyo_txt.width, tex_piyo_txt.height),
			OffsetRect.GetOffsetRect (sw * 1.1f - tex_piyo_txt.width, -sh * 0.1f, tex_piyo_txt.width, tex_piyo_txt.height),
			OffsetRect.GetOffsetRect (-sw * 0.15f, sh * 0.42f, tex_piyo_txt.width, tex_piyo_txt.height)
		};
		
				rect_piyo_txt = rects_piyo_txt [rnd_piyo_cutin];
		
				miss_count_for_Show = piyopiyoController.scoreManager.missCount;
		
				icon_path = Application.dataPath + "/../Icon@2x.png";
		
				PlayerPrefs.SetInt ("Stage" + PiyoPiyoController.selectedStage.ToString (), 1 + PlayerPrefs.GetInt ("Stage" + PiyoPiyoController.selectedStage.ToString ()));
		}
	
		void Update ()
		{
				if (fl_StageClear) {
						pos_x_clear_logo -= Time.deltaTime * 150;
						if (size_tassei < 1f && size_up)
								size_tassei += 0.09f;
						else if (size_tassei < 1.3f && size_up) {
								size_tassei += 0.06f;
						} else {
								size_tassei -= 0.05f;
								size_up = false;
						}
				
						//GUI.DrawTexture( OffsetRect.GetOffsetRect(pos_x_clear_logo, sh * 0.9f ,sw, sh * 0.1f), tex_Clear_Logo);	
				}	
		}

		void OnGUI ()
		{
				int sw = FixedScreen.width;
				int sh = FixedScreen.height;
				//texNumber.Show_Red(9,OffsetRect.GetOffsetRect(0,0,sw,sh));
				//texNumber.Show_Pop(7,OffsetRect.GetOffsetRect(0,0,sw,sh));
				if (PiyoPiyoController.state == "play") {
						if (!piyopiyoController.fl_GameStart)
								GUI.DrawTexture (OffsetRect.GetOffsetRect (0, 0, sw, sh), tex_Mask_Before_Start);
			
						if (GUI.Button (OffsetRect.GetOffsetRect (sw * 0.85f, sh - sw * 0.15f, sw * 0.15f, sw * 0.15f), "", style_Pause)) {
								PiyoPiyoController.state = "stop";
								piyopiyoController.obj_Sound.SendMessage ("Play", 9);
								piyopiyoController.obj_BGM.SendMessage ("Pause");
								Time.timeScale = 0f;
						}
		
						if (!piyopiyoController.fl_GameStart) {
								//if(PlayerPrefs.GetInt("Stage"+(PiyoPiyoController.selectedStage+1).ToString()) == 0 && PiyoPiyoController.selectedStage != 6){
								if (PlayerPrefs.GetInt ("Stage" + (PiyoPiyoController.selectedStage + 1).ToString ()) == 0) {
										GUI.DrawTexture (OffsetRect.GetOffsetRect (sw * 0.01f, sh * 0.7f, tex_Condition_Base.width, tex_Condition_Base.height), tex_Condition_Base);
										texTxt.ShowTxtConditionZero (piyopiyoController.scoreManager.stageClearCount, texNumber.ReturnDigit (piyopiyoController.scoreManager.stageClearCount), OffsetRect.GetOffsetRect (sw * 0.445f, sh * 0.736f, sw * 0.03f, tex_txt_Condition_2.height), 8);
										texTxt.Show_Txt_Condition_1 (OffsetRect.GetOffsetRect (sw * 0.465f, sh * 0.735f, sw * 0.23f, tex_txt_Condition_2.height));

										Texture tc2;
										if (PiyoPiyoController.selectedStage == 6)
												tc2 = tex_txt_Condition_2_forLastStage;
										else
												tc2 = tex_txt_Condition_2;
										GUI.DrawTexture (OffsetRect.GetOffsetRect (sw * 0.5f - tc2.width / 2, sh * 0.775f, tc2.width, tc2.height), tc2);
										//GUI.DrawTexture(OffsetRect.GetOffsetRect(sw * 0.5f - tex_txt_Condition_2.width/2,sh * 0.775f, tex_txt_Condition_2.width, tex_txt_Condition_2.height), tex_txt_Condition_2);
								}
				
								if (piyopiyoController.timer_GameStart < 1) {
										//tex_CountDown_Show = tex_CountDown_Start[0];
										//GUI.DrawTexture(OffsetRect.GetOffsetRect(sw / 2 - sw * 16 / 45 ,sh / 2 - sh * 5 / 64,sw * 32 / 45,sh * 5 / 32), tex_CountDown_Show);
										texTxt.Show_Txt_CountDown_Start (OffsetRect.GetOffsetRect (sw * 0.15f, sh * 0.45f, sw * 0.7f, sh * 0.12f));
										if (fl_countDown_se_played [0] == false) {
												fl_countDown_se_played [0] = true;
												piyopiyoController.obj_Sound.SendMessage ("Play", 12); // スタートSE
										}
								}
								if (piyopiyoController.timer_GameStart <= 4.0f && piyopiyoController.timer_GameStart > 1.0f) {
										string count = Mathf.FloorToInt (piyopiyoController.timer_GameStart).ToString ();
										switch (count) {
										case "3":
												texTxt.Show_Txt_CountDown (OffsetRect.GetOffsetRect (sw * 0.42f, sh * 0.3f, sw * 0.16f, sh * 0.17f), 3);
												break;
										case "2":
												texTxt.Show_Txt_CountDown (OffsetRect.GetOffsetRect (sw * 0.42f, sh * 0.3f, sw * 0.16f, sh * 0.17f), 2);
												break;
										case "1":
												texTxt.Show_Txt_CountDown (OffsetRect.GetOffsetRect (sw * 0.42f, sh * 0.3f, sw * 0.16f, sh * 0.17f), 1);
												break;
										}
										texTxt.Show_Txt_CountDown_Ready (OffsetRect.GetOffsetRect (sw * 0.15f, sh * 0.5f, sw * 0.7f, sh * 0.12f));
										//GUI.DrawTexture(OffsetRect.GetOffsetRect(sw / 2 - sw / 12 ,sh / 2 - sh / 8,sw / 6,sh / 4), tex_CountDown_Show);

										if (fl_countDown_se_played [int.Parse (count)] == false) {
												fl_countDown_se_played [int.Parse (count)] = true;
												piyopiyoController.obj_Sound.SendMessage ("Play", 11); // カウントダウンSE
										}
								}

								// 「ステージ <数字>」
								texTxt.Show_Txt_Stage_Logo (OffsetRect.GetOffsetRect (sw * 0.320f, sh * 0.1f, sw * 0.21f, sh * 0.07f));
								texTxt.Show_Txt_Stage (OffsetRect.GetOffsetRect (sw * 0.575f, sh * 0.1f, sw * 0.07f, sh * 0.07f), PiyoPiyoController.selectedStage);
								// 「初級ぴよこ」など
								texTxt.Show_Txt_StageSelect (OffsetRect.GetOffsetRect ((sw - 400f) / 2f,
							sh * (0.15f + 0.0279f / 5f * (PiyoPiyoController.selectedStage - 1)),
							0f, 0f), PiyoPiyoController.selectedStage - 1);

						} else {
				
								texTxt.Show_Txt_Stage_Logo (OffsetRect.GetOffsetRect (sw * 0.005f, sh * 0.85f, sw * 0.21f, sh * 0.07f));
								texTxt.Show_Txt_Stage (OffsetRect.GetOffsetRect (sw * 0.26f, sh * 0.85f, sw * 0.07f, sh * 0.07f), PiyoPiyoController.selectedStage);
								for (int i=0; i<miss_count_for_Show; i++) {
										if (i != miss_count_for_Show - 1) {
												GUI.DrawTexture (OffsetRect.GetOffsetRect (8f + sw * i * 0.08f, sh * 0.93f, sw * 0.08f, sw * 0.08f), tex_Stock);
										} else {
												GUI.DrawTexture (OffsetRect.GetOffsetRect (8f + sw * (i * 0.08f + x_miss_Stock), sh * (0.93f + y_miss_Stock), sw * 0.08f, sw * 0.08f), tex_Stock);
												if (y_miss_Stock != 0) {
														if (sh * (0.93f + y_miss_Stock) > sh)
																MinusMissCount ();
														else
																y_miss_Stock += 0.007f;
												}
												if (x_miss_Stock != 0) {
														if (x_miss_Stock > 0.003f || x_miss_Stock < -0.003f)
																mx *= -1;
														x_miss_Stock += mx;
							
												}
										}
								}
			
				
								GUI.DrawTexture (OffsetRect.GetOffsetRect (sw * 0.68f, sh * 0.01f, tex_Score_Logo.width, tex_Score_Logo.height), tex_Score_Logo);
								texNumber.ShowRankRedZero (piyopiyoController.scoreManager.score_For_Show, texNumber.ReturnDigit (piyopiyoController.scoreManager.score_For_Show), OffsetRect.GetOffsetRect (sw * 0.932f, sh * 0.068f, sw * 0.054f, sh * 0.054f), 8);
								//texNumber.ShowRankRedZero(3000000,7, OffsetRect.GetOffsetRect(sw * 0.924f, sh * 0.065f, sw * 0.063f, sh * 0.063f),7);
						
		
								if (piyopiyoController.scoreManager.comboCount >= 2) {
										texNumber.ShowRankBlueZero (piyopiyoController.scoreManager.comboCount, texNumber.ReturnDigit (piyopiyoController.scoreManager.comboCount), OffsetRect.GetOffsetRect (sw * 0.282f, sh * 0f, sw * 0.063f, sh * 0.063f), 5, true);
										texNumber.Show_Combo (OffsetRect.GetOffsetRect (sw * 0.035f, sh * 0.06f, sw * 0.336f, sh * 0.0744f));
								}
								
				
								if (fl_PointShow) {
										Rect rect;
										if (ratio < ratio_point) {
												ratio += ratio_inc;
												rect = OffsetRect.GetOffsetRect (pos_point.x, pos_point.y, sw * ratio, sh * ratio);
										} else {
												rect = OffsetRect.GetOffsetRect (pos_point.x, pos_point.y, sw * ratio_point, sh * ratio_point);
										}
										//rect.x = rect.x -rect.width / 2.5f;
										if (rect.x < FixedScreen.width / 2)
												rect.x -= 1500 * ratio;
										else
												rect.x += 1500 * ratio;
										rect.x += rect.width / 2f;// / 2.5f;
										rect.x = Mathf.Clamp (rect.x, rect.width + 77f, FixedScreen.width - 77f);
										rect.y = rect.y - rect.height / 2;
										rect.y += 1500 * ratio;
										/*
					rect.x = rect.x -rect.width / 2.5f;
					rect.y = rect.y - rect.height / 2;
					*/
										texNumber.ShowRankPop (point, ReturnDigit (point), rect);
										//ShowScore(tex_num, point, ReturnDigit(point), rect); 
					
								}
				
								if (combo_Bonus > 0) {
										int digit = texNumber.ReturnDigit (combo_Bonus);
										float base_x = sw * (0.36f - (7 - digit) * 0.028f);
										texNumber.ShowRankRed (combo_Bonus, digit, OffsetRect.GetOffsetRect (base_x, sh * 0.14f, sw * 0.05f, sh * 0.05f));
										texNumber.Show_Plus (OffsetRect.GetOffsetRect (
									base_x -
												512f * 0.089f * texNumber.ReturnDigit (combo_Bonus) / 1.41f
									, sh * 0.14f, 512f * 0.09f / 1.41f, 512f * 0.113f / 1.41f));
										//texNumber.ShowRankBlue(combo_Bonus, texNumber.ReturnDigit(combo_Bonus), OffsetRect.GetOffsetRect(sw * 0.4f, sh * 0f ,sw * 0.05f, sh * 0.05f));
										//texNumber.ShowRankBlue(1000000, texNumber.ReturnDigit(1000000), OffsetRect.GetOffsetRect(sw * 0.4f, sh * 0f ,sw * 0.05f, sh * 0.05f));
								}
				
								if (fl_StageClear && pos_x_clear_logo <= -sw && fl_resultComfirm) {
										if (fl_resultComfirm_show)
												GUI.color = new Color (1, 1, 1, 1);
										else
												GUI.color = new Color (0, 0, 0, 0);
					// quanvn remove button skip stage
//										if (GUI.Button (OffsetRect.GetOffsetRect (sw * 0.69f, sh - sw * 0.15f, sw * 0.15f, sw * 0.15f), "", style_bt_Result)) {
//												PiyoPiyoController.state = "resultComfirm";
//												piyopiyoController.obj_Sound.SendMessage ("Play", 9);
//												piyopiyoController.obj_BGM.SendMessage ("Pause");
//												Time.timeScale = 0f;
//										}
//					
//										if (GUI.Button (OffsetRect.GetOffsetRect (sw * 0.68f - style_bt_Result_Assist.normal.background.width * 0.9f, sh * 0.99f - style_bt_Result_Assist.normal.background.height, style_bt_Result_Assist.normal.background.width * 0.9f, style_bt_Result_Assist.normal.background.height * 0.9f), "", style_bt_Result_Assist)) {
//												PiyoPiyoController.state = "resultComfirm";
//												piyopiyoController.obj_Sound.SendMessage ("Play", 9);
//												piyopiyoController.obj_BGM.SendMessage ("Pause");
//												Time.timeScale = 0f;
//										}
					
										GUI.color = new Color (1, 1, 1, 1);
					
								}
				
								if (Time.time - timer_resultComfirm_switch > time_resultComfirm_switch) {
										if (Time.time - timer_resultComfirm_long < time_resultComfirm_long) {
												timer_resultComfirm_switch = Time.time;
												fl_resultComfirm_show = !fl_resultComfirm_show;
										} else
												fl_resultComfirm_show = true;
								}
				
								if (fl_StageClear && pos_x_clear_logo >= -sw) {
										timer_resultComfirm_long = Time.time;
										timer_resultComfirm_switch = Time.time;
										//pos_x_clear_logo -= Time.deltaTime * 30;
										if (size_tassei > 1.0f || size_up)
												GUI.DrawTexture (OffsetRect.GetOffsetRect (sw * 0.52f - tex_clear_tassei.width * size_tassei / 2, sh * 0.5f - tex_clear_tassei.height * size_tassei / 2, tex_clear_tassei.width * size_tassei, tex_clear_tassei.height * size_tassei), tex_clear_tassei);
										else if (size_tassei > 0f)
												GUI.DrawTexture (OffsetRect.GetOffsetRect (sw * 0.52f - tex_clear_tassei.width / 2, sh * 0.5f - tex_clear_tassei.height / 2, tex_clear_tassei.width, tex_clear_tassei.height), tex_clear_tassei);
										/*else if(size_tassei < 1.5f){
						GUI.DrawTexture(OffsetRect.GetOffsetRect( sw * 0.5f - tex_clear_tassei.width/2,sh * 0.5f - tex_clear_tassei.height/2, tex_clear_tassei.width, tex_clear_tassei.height),tex_clear_tassei);
					}*/
										GUI.DrawTexture (OffsetRect.GetOffsetRect (pos_x_clear_logo, sh * 0.9f, sw * 1.1f, sh * 0.1f), tex_Condition_Base);
										Texture tcl;
										if (PiyoPiyoController.selectedStage == 6)
												tcl = tex_Clear_Logo_forLastStage;
										else
												tcl = tex_Clear_Logo;
										GUI.DrawTexture (OffsetRect.GetOffsetRect (pos_x_clear_logo + 52f, sh * 0.936f, tcl.width, tcl.height), tcl);
										//GUI.DrawTexture( OffsetRect.GetOffsetRect(pos_x_clear_logo, sh * 0.92f ,sw, sh * 0.06f), tcl);
								}
				
					
						}
			
			
			
			
				} else if (PiyoPiyoController.state == "stop") {
			
			
			
			
						/*GUI.DrawTexture( OffsetRect.GetOffsetRect( 0, 0, sw, sh),tex_Stop_Back);
			//GUI.DrawTexture(OffsetRect.GetOffsetRect(sw * 0.2f, sh * 0.05f, sw * 0.6f, sh * 0.1f),tex_Stop);
			if(GUI.Button(OffsetRect.GetOffsetRect(sw * 0.3f, sh * 0.05f, sw * 0.4f, sh * 0.15f), "Continue") && possibleTouch){
				PiyoPiyoController.state = "play";
				Time.timeScale = 1.0f;
				piyopiyoController.obj_Sound.SendMessage("Play", 2);
				piyopiyoController.obj_BGM.SendMessage("PlayBGM");
			}
			if(GUI.Button(OffsetRect.GetOffsetRect(sw * 0.3f, sh * 0.25f, sw * 0.4f, sh * 0.15f), "Retry") && possibleTouch){
				possibleTouch = false;
				Time.timeScale = 1.0f;
				//StartCoroutine("SoundPlay", 0);
				Application.LoadLevel("MainPiyoPiyo");
				//Application.LoadLevel("Main2");
			}
			if(GUI.Button(OffsetRect.GetOffsetRect(sw * 0.3f, sh * 0.45f, sw * 0.4f, sh * 0.15f), "Top") && possibleTouch){
				possibleTouch = false;
				Time.timeScale = 1.0f;
				//StartCoroutine("SoundPlay", 1);
				Application.LoadLevel("Title");
			}
			if(GUI.Button(OffsetRect.GetOffsetRect(sw * 0.3f, sh * 0.65f, sw * 0.4f, sh * 0.15f), "StageSelect") && possibleTouch){
				possibleTouch = false;
				Time.timeScale = 1.0f;
				//StartCoroutine("SoundPlay", 3);
				Application.LoadLevel("SelectStage");
			}
			AudioSource aud = GameObject.FindWithTag("Item").GetComponent<AudioSource>();
			string message;
			if(aud.volume == 0)
				message = "サウンド：ON";
			else
				message = "サウンド：OFF";
			if(GUI.Button(OffsetRect.GetOffsetRect(sw * 0.3f, sh * 0.85f, sw * 0.4f, sh * 0.1f), message) && possibleTouch){
				//possibleTouch = false;
				//StartCoroutine("SoundPlay", 1);
				//Application.LoadLevel("Title");
				AudioSource sud = GameObject.FindWithTag("Sound").GetComponent<AudioSource>();
				if(aud.volume == 0){
					aud.volume = 1;
					sud.volume = 1;
				}else{
					aud.volume = 0;
					sud.volume = 0;
				}
			}*/
			
			
						GUI.DrawTexture (OffsetRect.GetOffsetRect (0, 0, sw, sh), tex_Stop_Back);
						GUI.DrawTexture (OffsetRect.GetOffsetRect (sw * 0.25f, sh * 0.3f, sw * 0.5f, sw * 0.5f * tex_Pause_txt.height / tex_Pause_txt.width), tex_Pause_txt);
			
			
						if (GUI.Button (OffsetRect.GetOffsetRect (sw * 0.82f, sh * 0.01f, gui_Stop_Resume.normal.background.width, gui_Stop_Resume.normal.background.height), "", gui_Stop_Resume)) {
								PiyoPiyoController.state = "play";
								Time.timeScale = 1.0f;
								piyopiyoController.obj_Sound.SendMessage ("Play", 2);
								piyopiyoController.obj_BGM.SendMessage ("PlayBGM");
						}
			
						//GUI.DrawTexture(OffsetRect.GetOffsetRect(sw * 0.35f, sh * 0.525f,sw * 0.3f,sw * 0.3f * tex_Pause_Retry_txt.height / tex_Pause_Retry_txt.width),tex_Pause_Retry_txt);
						bool retry = GUI.Button (OffsetRect.GetOffsetRect (sw * 0.35f, sh * 0.525f, sw * 0.3f, sw * 0.3f * tex_Pause_Retry_txt.normal.background.height / tex_Pause_Retry_txt.normal.background.width), "", tex_Pause_Retry_txt);
						if (GUI.Button (OffsetRect.GetOffsetRect (sw * 0.1f, sh * 0.5f, sw * 0.15f, sw * 0.15f), "", gui_Stop_Retry) || retry) {
								possibleTouch = false;
								CommonSE.PlayButtonSE ();
								Time.timeScale = 1.0f;
								//StartCoroutine("SoundPlay", 0);
								Application.LoadLevel ("MainPiyoPiyo");
								//Application.LoadLevel("Main2");
						}
			
						//GUI.DrawTexture(OffsetRect.GetOffsetRect(sw * 0.35f, sh * 0.675f,sw * 0.3f,sw * 0.3f * tex_Pause_Sound_txt.height / tex_Pause_Sound_txt.width),tex_Pause_Sound_txt);
						bool sound = GUI.Button (OffsetRect.GetOffsetRect (sw * 0.35f, sh * 0.675f, sw * 0.3f, sw * 0.3f * tex_Pause_Sound_txt.normal.background.height / tex_Pause_Sound_txt.normal.background.width), "", tex_Pause_Sound_txt);

						if (GameData.volume_Sound == 1) {
								gui_Sound.normal.background = (Texture2D)tex_Pause_Sound_on;
								if (GUI.Button (OffsetRect.GetOffsetRect (sw * 0.1f, sh * 0.65f, sw * 0.15f, sw * 0.15f), "", gui_Sound) || sound) {
										AudioManager.SetMute (true);
										GameData.volume_Sound = 0;
								}
						} else {
								gui_Sound.normal.background = (Texture2D)tex_Pause_Sound_off;
								if (GUI.Button (OffsetRect.GetOffsetRect (sw * 0.1f, sh * 0.65f, sw * 0.15f, sw * 0.15f), "", gui_Sound) || sound) {
										AudioManager.SetMute (false);
										GameData.volume_Sound = 1;
										CommonSE.PlayButtonSE ();
								}
						}
						/*
			AudioSource sud = GameObject.FindWithTag("Sound").GetComponent<AudioSource>();
			AudioSource aud = GameObject.FindWithTag("Item").GetComponent<AudioSource>();
			if(aud.volume > 0){
				gui_Sound.normal.background = (Texture2D)tex_Pause_Sound_on;
				if(GUI.Button(OffsetRect.GetOffsetRect(sw * 0.1f,sh * 0.65f,sw * 0.15f,sw * 0.15f),"", gui_Sound) || sound){
					aud.volume = 0;
					GameData.volume_Sound = 0;
				}
			}else{
				gui_Sound.normal.background = (Texture2D)tex_Pause_Sound_off;
				if(GUI.Button(OffsetRect.GetOffsetRect(sw * 0.1f,sh * 0.65f,sw * 0.15f,sw * 0.15f),"", gui_Sound) || sound){
					aud.volume = 0.5f;
					GameData.volume_Sound = 1;
					//audio.PlayOneShot(se_Button);
				}
			}
			*/
			
			
						//GUI.DrawTexture(OffsetRect.GetOffsetRect(sw * 0.35f, sh * 0.825f,sw * 0.55f,sw * 0.55f * tex_Pause_Return_txt.height / tex_Pause_Return_txt.width),tex_Pause_Return_txt);
						bool top = GUI.Button (OffsetRect.GetOffsetRect (sw * 0.35f, sh * 0.825f, sw * 0.55f, sw * 0.55f * tex_Pause_Return_txt.normal.background.height / tex_Pause_Return_txt.normal.background.width), "", tex_Pause_Return_txt);
						if (GUI.Button (OffsetRect.GetOffsetRect (sw * 0.1f, sh * 0.8f, sw * 0.15f, sw * 0.15f), "", gui_Stop_Top) || top) {
								possibleTouch = false;
								CommonSE.PlayButtonSE ();
								Time.timeScale = 1.0f;
								//StartCoroutine("SoundPlay", 1);
								Application.LoadLevel ("Title");
						}
			
			
			
#if DEBUG_GUI
			if(GUI.Button(OffsetRect.GetOffsetRect( sw * 0.2f, sh * 0.1f, sw * 0.2f, sh * 0.2f),
						"DebugDisp: " + (fl_debug_disp ? "ON" : "OFF"))){
				fl_debug_disp = !fl_debug_disp;
			}

			if(GUI.Button(OffsetRect.GetOffsetRect( 0,sh * 0.1f, sw * 0.2f, sh * 0.2f),"9990\nCombo")){
				piyopiyoController.scoreManager.comboCount = 9990;
			}
#endif// DEBUG_GUI
			
			
			
			
				} else if (PiyoPiyoController.state == "clear") {

						GUI.DrawTexture (OffsetRect.GetOffsetRect (0, 0, sw, sh), tex_Stop_Back);
						GUI.DrawTexture (OffsetRect.GetOffsetRect (sw * 0.1f, sh * 0.05f, sw * 0.8f, sh * 0.1f), tex_Clear_Logo);
						GUI.DrawTexture (OffsetRect.GetOffsetRect (sw * 0.15f, sh * 0.2f, sw * 0.25f, sh * 0.07f), tex_ResultScore_Logo);
						ShowScore (tex_num, piyopiyoController.scoreManager.score, ReturnDigit (piyopiyoController.scoreManager.score), OffsetRect.GetOffsetRect (sw * 0.8f, sh * 0.2f, sw * 0.05f, sh * 0.07f));
						GUI.DrawTexture (OffsetRect.GetOffsetRect (sw * 0.15f, sh * 0.3f, sw * 0.2f, sh * 0.07f), tex_Best_Logo);
						ShowScore (tex_num, PlayerPrefs.GetInt ("Score1"), ReturnDigit (PlayerPrefs.GetInt ("Score1")), OffsetRect.GetOffsetRect (sw * 0.8f, sh * 0.3f, sw * 0.05f, sh * 0.07f));
			
						if (piyopiyoController.scoreManager.ReturnRank () != -1) {
								GUI.DrawTexture (OffsetRect.GetOffsetRect (sw * 0.15f, sh * 0.4f, sw * 0.3f, sh * 0.07f), tex_Rank_Logo);
								GUI.DrawTexture (OffsetRect.GetOffsetRect (sw * 0.8f, sh * 0.4f, sw * 0.05f, sh * 0.07f), tex_num [piyopiyoController.scoreManager.ReturnRank ()]);
						}
						if (piyopiyoController.scoreManager.ReturnRank () == 1) {
								GUI.DrawTexture (OffsetRect.GetOffsetRect (sw * 0.3f, sh * 0.5f, sw * 0.4f, sh * 0.07f), tex_NewRecord_Logo);
								ShowScore (tex_num, piyopiyoController.scoreManager.score - PlayerPrefs.GetInt ("Score1"), ReturnDigit (piyopiyoController.scoreManager.score - PlayerPrefs.GetInt ("Score1")), OffsetRect.GetOffsetRect (sw * 0.5f, sh * 0.6f, sw * 0.05f, sh * 0.07f));
						}
			
						if (GUI.Button (OffsetRect.GetOffsetRect (sw * 0.075f, sh * 0.75f, sw * 0.25f, sh * 0.15f), "Retry") && possibleTouch) {
								possibleTouch = false;
								//StartCoroutine("SoundPlay", 0);
								Application.LoadLevel ("MainPiyoPiyo");
								//Application.LoadLevel("Main2");
						}
						if (GUI.Button (OffsetRect.GetOffsetRect (sw * 0.375f, sh * 0.75f, sw * 0.25f, sh * 0.15f), "Top") && possibleTouch) {
								possibleTouch = false;
								//StartCoroutine("SoundPlay", 1);
								Application.LoadLevel ("Title");
						}
						if (GUI.Button (OffsetRect.GetOffsetRect (sw * 0.675f, sh * 0.75f, sw * 0.25f, sh * 0.15f), "StageSelect") && possibleTouch) {
								possibleTouch = false;
								//StartCoroutine("SoundPlay", 3);
								Application.LoadLevel ("SelectStage");
								//Application.LoadLevel("Title");
						}
			
			
						GUI.color = Color.white;
			
				} else if (PiyoPiyoController.state == "end") {
						float result_alpha; // リザルト表示するタイミング調整用の数値
						if (piyopiyoController.scoreManager.score >= piyopiyoController.scoreManager.stageClearCount)
								result_alpha = 2.5f; // 合格ニコピヨ
						else
								result_alpha = 2.3f; // 合格じゃないダダピヨ

						if (alpha > result_alpha) {
								alpha = -1;
								if (PiyoPiyoController.selectedStage < 6)
										next_Stage = PlayerPrefs.GetInt ("Stage" + (PiyoPiyoController.selectedStage + 1).ToString ());
								else
										next_Stage = 0;
								if (SelectStage.newStage_Release)
										possibleTouch = false;
								fl_Show_Result = true;
								GUI.color = Color.white;
						} else if (alpha >= 0) {
								alpha += Time.deltaTime * 0.2f;
								GUI.color = new Color (0, 0, 0, alpha);
						}
						GUI.DrawTexture (OffsetRect.GetOffsetRect (0, 0, sw, sh * 1.002f), tex_GameOver_Mask);
			
						if (alpha > 1.1f && alpha < result_alpha) {
								GUI.color = Color.white;
								int se_num = 7; // ゲームオーバーSE
								if (piyopiyoController.scoreManager.score >= piyopiyoController.scoreManager.stageClearCount) {
										tex_piyo_txt = tex_yoropiyo_txt;
										tex_piyo_cutin = tex_yoropiyo;
										rect_piyo_txt = rects_piyo_txt [3];
										se_num = 14; // ファンファーレ
								}
								GUI.DrawTexture (OffsetRect.GetOffsetRect (0, 0, sw, sh), tex_piyo_cutin);
								GUI.DrawTexture (rect_piyo_txt, tex_piyo_txt);
								GUI.color = new Color (0, 0, 0, alpha);

								if (gameover_cutin_appeared == false) {
										gameover_cutin_appeared = true;
										piyopiyoController.obj_BGM.SendMessage ("Stop");
										piyopiyoController.obj_Sound.SendMessage ("Play", se_num); // 結果次第のSE
										GameObject[] objs = GameObject.FindGameObjectsWithTag ("Target");
										foreach (GameObject obj in objs) {
												obj.SendMessage ("Miss");
										}

										// GameCenter送信
										Debug.Log ("★GameCenter result_rank:" + piyopiyoController.result_rank);
										if (piyopiyoController.result_rank == 1) { // 新記録時のみ
												GameCenterConnector.ReportScore (piyopiyoController.scoreManager.score);
										}
										sns_message = "「ピヨピヨすわいぷ」で\n"
												+ piyopiyoController.scoreManager.score
												+ "スコアでたピヨ…\n(・´ω｀・)";
								}
						}
			
						if (fl_Show_Result) {
								GUI.DrawTexture (OffsetRect.GetOffsetRect (0, 0, sw, sh), tex_piyo_cutin);
								GUI.DrawTexture (OffsetRect.GetOffsetRect (0, 0, sw, sh * 1.002f), tex_GameOver_Mask);
								GUI.DrawTexture (OffsetRect.GetOffsetRect (sw * 0.5f - tex_GameOver_Logo.width / 2, sh * 0.05f, tex_GameOver_Logo.width, tex_GameOver_Logo.height), tex_GameOver_Logo);
								GUI.DrawTexture (OffsetRect.GetOffsetRect (sw * 0.03f, sh * 0.2f, tex_ResultScore_Logo.width, tex_ResultScore_Logo.height), tex_ResultScore_Logo);
								texNumber.ShowRankBig (piyopiyoController.scoreManager.score, texNumber.ReturnDigit (piyopiyoController.scoreManager.score), OffsetRect.GetOffsetRect (sw * 0.85f, sh * 0.185f, sw * 0.27f * 0.36f, sh * 0.25f * 0.36f));
								GUI.DrawTexture (OffsetRect.GetOffsetRect (sw * 0.03f, sh * 0.3f, tex_Best_Logo.width, tex_Best_Logo.height), tex_Best_Logo);
								texNumber.ShowRankBig (PlayerPrefs.GetInt ("Score1"), texNumber.ReturnDigit (PlayerPrefs.GetInt ("Score1")), OffsetRect.GetOffsetRect (sw * 0.85f, sh * 0.285f, sw * 0.27f * 0.36f, sh * 0.25f * 0.36f));
			
								// Debug.Log("★NewRecord? result_rank:"+piyopiyoController.result_rank);
								if (piyopiyoController.result_rank == 1) {
										GUI.DrawTexture (OffsetRect.GetOffsetRect (sw * 0.5f - tex_NewRecord_Logo.width / 2, sh * 0.3f, tex_NewRecord_Logo.width, tex_NewRecord_Logo.height), tex_NewRecord_Logo);
								}
				
								if (GUI.Button (OffsetRect.GetOffsetRect (sw * 0.5f - style_GameOver_Retry.normal.background.width / 2, sh * 0.85f, style_GameOver_Retry.normal.background.width, style_GameOver_Retry.normal.background.height), "", style_GameOver_Retry) && possibleTouch && !SelectStage.newStage_Release) {
										possibleTouch = false;
										CommonSE.PlayButtonSE ();
										Application.LoadLevel ("MainPiyoPiyo");
								}
								if (next_Stage != 0) {
										if (GUI.Button (OffsetRect.GetOffsetRect (sw * 0.8f - style_GameOver_Next.normal.background.width, sh * 0.85f, style_GameOver_Next.normal.background.width, style_GameOver_Next.normal.background.height), "", style_GameOver_Next) && possibleTouch && !SelectStage.newStage_Release) {
												CommonSE.PlayButtonSE ();
												PiyoPiyoController.selectedStage += 1;

												if (PlayerPrefs.GetInt ("Stage" + PiyoPiyoController.selectedStage.ToString ()) == 1 // 1: 解放済みで初回説明見てない
														&& PiyoPiyoController.selectedStage != 1
														&& PiyoPiyoController.selectedStage != 6
						  ) {
														// 初回説明表示
														PlayerPrefs.SetInt ("Stage" + PiyoPiyoController.selectedStage.ToString (), 2);
														fl_Show_Help = true;
														fl_Show_Result = false;
												} else {
														// 初回じゃないので説明なし
														possibleTouch = false;
														Application.LoadLevel ("MainPiyoPiyo");
												}
										}
								}
								if (GUI.Button (OffsetRect.GetOffsetRect (sw * 0.2f, sh * 0.85f, style_GameOver_Select.normal.background.width, style_GameOver_Select.normal.background.height), "", style_GameOver_Select) && possibleTouch && !SelectStage.newStage_Release) {
										possibleTouch = false;
										CommonSE.PlayButtonSE ();
										Application.LoadLevel ("SelectStage");
								}
				
								ShowComment ();
				
#if WITH_FACEBOOK // Facebookあり
								//Kyhov Editor
								//fl_FaceBook = GUI.Button(OffsetRect.GetOffsetRect(pos_Facebook.x, pos_Facebook.y, tex_Facebook.width, tex_Facebook.height), tex_Facebook, GUIStyle.none);
#endif
								fl_Wechat = GUI.Button (OffsetRect.GetOffsetRect (pos_Wechat.x, pos_Wechat.y, tex_Wechat.width, tex_Wechat.height), tex_Wechat, GUIStyle.none);
	
								if (fl_Wechat && possibleTouch) {
										//Kyhov Editor
#if UNITY_IPHONE
					IOSBridge.SharedText_Moment ("黑人碰到大麻烦啦！谁快来救救他!\nhttps://itunes.apple.com/cn/app/jue-wang!-yu-zhi-he-mei-qiu/id892050338");
#endif
								}

				
								if (SelectStage.newStage_Release) {
										GUI.color = new Color (1, 1, 1, _a);
										GUI.DrawTexture (OffsetRect.GetOffsetRect (0, 0, sw, sh * 1.1f), tex_GameOver_Mask);
										GUI.color = Color.white;
										GUI.DrawTexture (OffsetRect.GetOffsetRect (sw * _x - tex_txtMask.width / 2, sh * 0.5f - tex_txtMask.height / 2, tex_txtMask.width, tex_txtMask.height), tex_txtMask);

										Texture trt;
										if (PiyoPiyoController.selectedStage == 6)
												trt = tex_Release_txt_forLastStage;
										else
												trt = tex_Release_txt;
										GUI.DrawTexture (OffsetRect.GetOffsetRect (sw * _x - trt.width / 2, sh * 0.4f, trt.width, trt.height), trt);
										//GUI.DrawTexture(OffsetRect.GetOffsetRect(sw * _x - tex_Release_txt.width/2, sh*0.4f , tex_Release_txt.width, tex_Release_txt.height), tex_Release_txt);

										if (_x > 0.5f && !possibleTouch) {
												_x -= 1.3f * Time.deltaTime;
												_a += 2 * Time.deltaTime;
												//GUI.DrawTexture(OffsetRect.GetOffsetRect(sw * _x, sh * 0.3f, sw * 0.4f, sh * 0.4f), texs_SecretCharacter[returnID]);
										} else if (possibleTouch) {
												_x -= 1.3f * Time.deltaTime;
												_a -= 2 * Time.deltaTime;
										} else if (GUI.Button (OffsetRect.GetOffsetRect (sw * _x - style_Ok_Bt.normal.background.width / 2, sh * 0.5f, style_Ok_Bt.normal.background.width, style_Ok_Bt.normal.background.height), "", style_Ok_Bt)) {
												possibleTouch = true;
						
												Invoke ("OffFl_NewStageRelease", 1.0f);
										} else {
												_x = 0.5f;
												GUI.DrawTexture (OffsetRect.GetOffsetRect (sw * _x - tex_OK.width / 2, sh * 0.515f, tex_OK.width, tex_OK.height), tex_OK);
										}
								}
						} else if (fl_Show_Help) {
								SelectStage.ShowFirstHelp (sw, sh, ref possibleTouch);
						}
			
				} else if (PiyoPiyoController.state == "resultComfirm") {
			
						GUI.DrawTexture (OffsetRect.GetOffsetRect (sw * 0.5f - tex_resultComfirm_window.width / 2, sh * 0.5f, tex_resultComfirm_window.width, tex_resultComfirm_window.height), tex_resultComfirm_window);
						GUI.DrawTexture (OffsetRect.GetOffsetRect (sw * 0.5f - tex_resultComfirm_text.width / 2, sh * 0.6f, tex_resultComfirm_text.width, tex_resultComfirm_text.height), tex_resultComfirm_text);
			
			
						if (GUI.Button (OffsetRect.GetOffsetRect (sw * 0.535f, sh * 0.68f, sw * 0.3f, style_bt_resultComfirm_no.normal.background.height), "", style_bt_resultComfirm_no)) {
								//fl_resultComfirm = false;
								PiyoPiyoController.state = "play";
								Time.timeScale = 1.0f;
								piyopiyoController.obj_Sound.SendMessage ("Play", 2);
								piyopiyoController.obj_BGM.SendMessage ("PlayBGM");
						}
			
						if (GUI.Button (OffsetRect.GetOffsetRect (sw * 0.465f - sw * 0.3f, sh * 0.68f, sw * 0.3f, style_bt_resultComfirm_yes.normal.background.height), "", style_bt_resultComfirm_yes)) {
								//fl_resultComfirm = false;
								Time.timeScale = 1.0f;
								piyopiyoController.result_rank = piyopiyoController.scoreManager.ReturnRank ();
								piyopiyoController.scoreManager.RecordRank ();
								PiyoPiyoController.state = "end";	
				
						}
			
						GUI.DrawTexture (OffsetRect.GetOffsetRect (sw * 0.522f - tex_resultComfirm_yesnotext.width / 2, sh * 0.7f, tex_resultComfirm_yesnotext.width, tex_resultComfirm_yesnotext.height), tex_resultComfirm_yesnotext);
			
			
			
			
				}
		
		}
	
		public void PointShow (int s, Vector3 pos)
		{
				if (IsInvoking ("OffPointShow")) {
						CancelInvoke ("OffPointShow");
						OffPointShow ();
				}
				point = s;
				//point = 100;
				pos_point = pos;

				/*
		if(ReturnDigit(point) == 1){
			ratio_inc = 0.002f;
			ratio_point = 0.05f;
		}else if(ReturnDigit(point) == 2){
			ratio_inc = 0.01f;
			ratio_point = 0.1f;
		}else{
			ratio_inc = 0.01f;
			ratio_point = 0.13f;
		}
		*/
				ratio_inc = 0.003f;
				ratio_point = 0.09f;
			
				fl_PointShow = true;

				Invoke ("OffPointShow", 1.0f);
				/*
		if(!IsInvoking())
			Invoke("OffPointShow", 1.0f);
			*/
		
		}
		public void OffPointShow ()
		{
				//combo_Bonus = 0;
				ratio = 0;
				fl_PointShow = false;
				piyopiyoController.scoreManager.WriteScoreForShow ();
		}
	
		public void WriteComboBonus (int s)
		{
				combo_Bonus = s;
				piyopiyoController.obj_Sound.SendMessage ("Play", 8); // コンボボーナス音
				Invoke ("OffComboBonus", 1.0f);
		}
		public void OffComboBonus ()
		{
				combo_Bonus = 0;
		}
	
		IEnumerator SoundPlay (int i)
		{
				piyopiyoController.obj_Sound.SendMessage ("Play", 1);
				yield return new WaitForSeconds (1.5f);
				if (i == 0) {
						Application.LoadLevel ("MainPiyoPiyo");
				} else if (i == 1) {
						Application.LoadLevel ("Title");
				} else if (i == 2) {
						PiyoPiyoController.state = "end";
						piyopiyoController.obj_Sound.SendMessage ("Play", 7);
				} else {
						Application.LoadLevel ("SelectStage");	
				}
		
				possibleTouch = true;
		
		}
	
		public void ShowComment ()
		{
				int sw = FixedScreen.width;
				int sh = FixedScreen.height;
				int score = piyopiyoController.scoreManager.score;
				int border = piyopiyoController.scoreManager.stageClearCount;
		
				GUI.DrawTexture (OffsetRect.GetOffsetRect (sw * 0.5f - tex_GameOver_Piyo.width / 2, sh * 0.5f, tex_GameOver_Piyo.width, tex_GameOver_Piyo.height), tex_GameOver_Piyo);
		
				/*if(fl_StageClear && (int)(Time.time % 2) == 0){
			GUI.DrawTexture(OffsetRect.GetOffsetRect(0, sh * 0.53f, texs_Comment[5].width, texs_Comment[5].height), texs_Comment[5]);
		}else{*/
				int result_id;
				if (score >= border * 5)
						result_id = 4;
				else if (score >= border * 2)
						result_id = 3;
				else if (score >= border)
						result_id = 2;
				else if (score >= border / 2)
						result_id = 1;
				else
						result_id = 0;

				texTxt.Show_Txt_Result (OffsetRect.GetOffsetRect (sw * 0.208f, sh * 0.55f, sw * 0.567f, sh * 0.13f * 0.435f), result_id);
				//}
		
				GUI.DrawTexture (OffsetRect.GetOffsetRect (sw * 0.1f, sh * 0.78f, tex_GameOver_HelpPiyo.width, tex_GameOver_HelpPiyo.height), tex_GameOver_HelpPiyo);
				texNumber.ShowRankBlueZero (piyopiyoController.scoreManager.correctCount, texNumber.ReturnDigit (piyopiyoController.scoreManager.correctCount), OffsetRect.GetOffsetRect (sw * 0.8f, sh * 0.777f, sw * 0.1f * 0.05f / 0.07f, tex_GameOver_HelpPiyo.height), 6, false);
				//texTxt.Show_Txt_Result(OffsetRect.GetOffsetRect(0,0,sw,sh),4);
		
		}
	
		public void ShowScore (Texture[] tex_num, int s, int digit, Rect rect)
		{
				while (true) {
						if (digit == 0)
								break;
			
						GUI.DrawTexture (new Rect (rect.x - rect.width * (digit - 1), rect.y, rect.width, rect.height), tex_num [(s / (int)Mathf.Pow (10, digit - 1))]);
			
						s -= (s / (int)Mathf.Pow (10, digit - 1)) * (int)Mathf.Pow (10, digit - 1);
						digit--;
			
			
			
				}
		}
	
		public int ReturnDigit (int s)
		{
				int i = 0;
				while (true) {
						s /= 10;
						i++;
						if (s == 0) {
								break;
						}
				}
				return i;
		}
	
		public void StageClear ()
		{
				timer_resultComfirm_long = Time.time;
				timer_resultComfirm_switch = Time.time;
		
				fl_StageClear = true;
				piyopiyoController.obj_Sound.SendMessage ("Play", 10);
		}
	
		public void OffFl_NewStageRelease ()
		{
				SelectStage.newStage_Release = false;
				possibleTouch = true;
		}
	
		public void ShakeMissCount ()
		{
				x_miss_Stock = 0.001f;	
		}
	
		public void MissCountDown ()
		{
				y_miss_Stock = 0.001f;
				x_miss_Stock = 0;
		}
	
		public void MinusMissCount ()
		{
				miss_count_for_Show -= 1;
				y_miss_Stock = 0;
		}
}
