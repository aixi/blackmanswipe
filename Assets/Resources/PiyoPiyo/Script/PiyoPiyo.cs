﻿using UnityEngine;
using System.Collections;

public class PiyoPiyo : MonoBehaviour {
	private PiyoPiyoController piyopiyoController;

	public PiyoPiyoData piyoData;
	public Material[] materials_Color;
	public Material[] face_Color;
	public GameObject[] objs_Literal;
	public GameObject obj_Literal;
	public GameObject obj_Renderer;
	public GameObject obj_Face;
	public GameObject obj_Prominence;
	public GameObject obj_Fire;
	public GameObject obj_Califlower;
	
	public AnimationClip[] anims_Face;
	public float[][] array_Face = new float[][]{new float[]{1,1,0,0}, new float[]{1,1,0.5f,0}, new float[]{1,1,1,0.5f}, new float[]{1,1,0.5f,0.5f}};
	
	
	
	void Start () {
		if(piyoData.num_Literal == 0){
			if(piyoData.num_Material_Color == 2){
				if(piyoData.direction_Correct){
					gameObject.transform.eulerAngles = new Vector3(0, 90, 0);	
				}else{
					gameObject.transform.eulerAngles = new Vector3(0, -90, 0);	
				}
			}else{
				if(PiyoPiyoController.selectedStage >= 3)
					gameObject.transform.eulerAngles = new Vector3(0, 90 * Random.Range(-1, 2), 0);
				else
					gameObject.transform.eulerAngles = new Vector3(0, 0, 0);
			}
		}

		piyopiyoController = GameObject.FindWithTag("GameController").GetComponent<PiyoPiyoController>();
	}
	
	
	
	void FixedUpdate () {
		if(piyoData.piyoID == PiyoPiyoManager.possible_Catch_piyoId){
			if((int)(Time.time * 5f )% 2 == 0)
				obj_Renderer.renderer.material.SetColor("_OutlineColor", Color.white);
			else
				obj_Renderer.renderer.material.SetColor("_OutlineColor", Color.black);
		}
		if(!piyoData.fl_Field){
			
			
			if(!piyoData.fl_Caught){
				//Debug.Log(gameObject.rigidbody.velocity.y);
				if(gameObject.rigidbody.velocity.y <= -1){
					//if(!obj_Face.animation.IsPlaying(anims_Face[2].name))
					//	obj_Face.animation.Play(anims_Face[2].name);
					FaceChange(2);
				}else{
					//if(!obj_Face.animation.IsPlaying(anims_Face[0].name))
					//	obj_Face.animation.Play(anims_Face[0].name);
					//Debug.Log(gameObject.rigidbody.velocity.y);
					FaceChange(0);
				}
				/*if(piyoData.num_Material_Color == 2 && piyoData.num_Literal == 0){
					if(piyoData.direction_Correct){
						gameObject.transform.eulerAngles = new Vector3(0, 90, 0);	
					}else{
						gameObject.transform.eulerAngles = new Vector3(0, -90, 0);	
					}
				}*/
				
				if(!animation.IsPlaying("wate01")) // replaced "wate01"
					animation.Play("wate01");
				if(piyoData.obj_Board != null){
					if(!piyoData.onBoard){
						gameObject.rigidbody.velocity = new Vector3(0,-piyoData.speed * Time.deltaTime, -piyoData.obj_Board.transform.position.z);
						transform.position = new Vector3(transform.position.x, transform.position.y, piyoData.obj_Board.transform.position.z);
					}else{
						if(transform.position.y < 1.3f)
							transform.position = new Vector3(transform.position.x, 1.3f, piyoData.obj_Board.transform.position.z);
						else
							transform.position = new Vector3(transform.position.x, transform.position.y, piyoData.obj_Board.transform.position.z);
						gameObject.rigidbody.velocity = new Vector3(0,Physics.gravity.y, 0);
					}
					/*if(piyoData.obj_Board.transform.position.z < -25)
						piyoData.obj_Board.rigidbody.velocity = new Vector3( 0, Physics.gravity.y, piyoData.array_Speed[0] * Time.deltaTime);
					else*/
					
						piyoData.obj_Board.rigidbody.velocity = new Vector3( 0, Physics.gravity.y, piyoData.speed * Time.deltaTime);
				}
				
				
			}else{
				//if(!obj_Face.animation.IsPlaying(anims_Face[1].name))
				//	obj_Face.animation.Play(anims_Face[1].name);
				FaceChange(1);
				
				if(!animation.IsPlaying("catch"))
					animation.Play("catch");
				rigidbody.velocity = Vector3.zero;
				
				float screen_x = FixedScreen.width / 2 + Input.mousePosition.x - piyoData.caught_mouse_x;
				float padding_x = 80f;
				screen_x = Mathf.Clamp(screen_x, padding_x, FixedScreen.width - padding_x);
				//Debug.Log("screen_x: " + screen_x);
				Vector3 pos = piyoData.cam.ScreenToWorldPoint(new Vector3(screen_x, Input.mousePosition.y, piyoData.caught_z));
				if(pos.y != 2.5f){
					//float sub = 2.5f - pos.y;
					//pos.z += sub / 4;
					pos.y = 6.4f;
				}
				pos.z = transform.position.z;
//				Debug.Log(pos);
				transform.position = pos;
				//piyoData.caught_y = transform.position.z;
				
			}
			
		}else if(!piyoData.fl_Fire){
			gameObject.transform.position = new Vector3(transform.position.x, 2.5f, transform.position.z);
			if(transform.eulerAngles.y > 175 && transform.eulerAngles.y < 185)
				gameObject.rigidbody.velocity = new Vector3(0, Physics.gravity.y, -5f * piyoData.speed * Time.deltaTime);
			else
				gameObject.rigidbody.velocity = new Vector3(-1f * piyoData.speed * Time.deltaTime * (-piyoData.direction_Left), Physics.gravity.y, 0);
			if(!animation.IsPlaying("run"))
				animation.Play("run");
			
			
		}else{
			gameObject.rigidbody.velocity = new Vector3(0, Physics.gravity.y, 0);
		}
	}
	
	public void OnField(Vector3 pos){
		piyoData.fl_Field = true;
		transform.position = pos;
	}
	
	public void Caught(float distance){
		piyoData.fl_Caught = true;
		gameObject.layer = 10;
//		Debug.Log(distance);
//		Debug.Log(Vector3.Distance(transform.position,GameObject.FindWithTag("MainCamera").transform.position));
		//piyoData.caught_y = Vector3.Distance(transform.position,GameObject.FindWithTag("MainCamera").transform.position);
		//piyoData.caught_y = distance;
		piyoData.caught_z = distance;
		piyoData.caught_mouse_x = Input.mousePosition.x;

		int se_num = 3; // 3と4がつかみSE別パターン→黒だけ変えてみる
		if (piyoData.num_Material_Color == 2) se_num = 4; //0→黄色,1→緑色,2→黒色
		piyopiyoController.obj_Sound.SendMessage("Play", se_num);
	}
	
	public void Release(){
		//Vector3 pos = cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, cam_y));
		//transform.position = pos;
		piyoData.fl_Caught = false;
	//	if(transform.position.z < 25)
			piyoData.fl_Fire = !Check();
	//	else
	//		piyoData.fl_Fire = false;
		
		if(piyoData.fl_Fire){
			Invoke("FireOff", 1.0f);
		}
		//FireOff();
		
		
		
	}
	
	public bool Check(){
		PiyoPiyoController controller = piyopiyoController;//GameObject.FindWithTag("GameController").GetComponent<PiyoPiyoController>();
		Vector3 pos = transform.position;
		if(transform.position.x >= piyoData.border_x){
			piyoData.fl_Field = true;
			piyoData.direction_Left = 1;
			gameObject.tag = "UnTarget";
			if(piyoData.direction_Correct){
				transform.eulerAngles = new Vector3(0,90,0);
				controller.Judge(true, piyoData.speedLevel, pos);
				return true;
			}else{
				transform.eulerAngles = new Vector3(0,90,0);
				controller.Judge(false, piyoData.speedLevel, pos);
				FireBreath(new Vector3(0,90,0));

				// obj_Renderer.renderer.material = materials_Color[3];

				//obj_Face.animation.Play(anims_Face[3].name);
				FaceChange(3);
				InstantiateCaliflower();
				return false;
			}
		}
		
		if(transform.position.x < -piyoData.border_x){
			piyoData.fl_Field = true;
			piyoData.direction_Left = -1;
			gameObject.tag = "UnTarget";
			if(piyoData.direction_Correct){
				transform.eulerAngles = new Vector3(0,-90,0);
				controller.Judge(false, piyoData.speedLevel, pos);
				FireBreath(new Vector3(0,-90,0));
				// Obj Cange Material wehn mismath 

				//obj_Renderer.renderer.material = materials_Color[3];


				//obj_Face.animation.Play(anims_Face[3].name);
				FaceChange(3);
				InstantiateCaliflower();
				return false;
			}else{
				transform.eulerAngles = new Vector3(0,-90,0);
				controller.Judge(true, piyoData.speedLevel, pos);
				return true;
			}
		}
		if(obj_Literal != null)
			Destroy(obj_Literal);
		
		return false;
	}
	
	public void ChangeMaterial(){
		obj_Renderer.renderer.material = materials_Color[piyoData.num_Material_Color];
		//Kyhov Editor
		Debug.Log ("Color Index : " + piyoData.num_Material_Color + " !");

		obj_Face.renderer.material = face_Color[piyoData.num_Material_Color];
		if(piyoData.num_Literal != 0){
			obj_Literal = Instantiate(objs_Literal[piyoData.num_Literal-1]) as GameObject;
			obj_Literal.transform.position = new Vector3(transform.position.x, transform.position.y + 7.5f, transform.position.z + 4f);
			obj_Literal.transform.parent = transform;
		}
	}
	
	public void Miss(){
		Instantiate(obj_Prominence, transform.position, Quaternion.identity);
		Destroy(gameObject);	
	}
	
	public void FireBreath(Vector3 dir){
		transform.eulerAngles = Vector3.zero;
		//Instantiate(obj_Fire, transform.position, Quaternion.Euler(dir));
	}
	
	public void FireOff(){
		piyoData.fl_Fire = false;
		/*if(transform.position.x >= piyoData.border_x) 
			transform.eulerAngles = new Vector3(0,90,0);
		else
			transform.eulerAngles = new Vector3(0,-90,0);*/
		transform.eulerAngles = new Vector3(0,180,0);
		
	}
	
	public void InstantiateCaliflower(){
		GameObject obj = Instantiate(obj_Califlower) as GameObject;
		obj.transform.parent = transform;
		obj.transform.localPosition = new Vector3(0f, 4.0f, 1.5f);
		//obj.transform.localPosition = new Vector3(0, 2.5f, 2.2f);
	}
	
	public void FaceChange(int id){
		obj_Face.renderer.material.mainTextureScale = new Vector2(array_Face[id][0], array_Face[id][1]);
		obj_Face.renderer.material.mainTextureOffset = new Vector2(array_Face[id][2], array_Face[id][3]);
	}
	
	void OnCollisionEnter(Collision info){
		if(info.gameObject.tag == "Board"){
			piyoData.onBoard = true;
		}
	}
	
}
