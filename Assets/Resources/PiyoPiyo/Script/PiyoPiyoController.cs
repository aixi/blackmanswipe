﻿// #define DEBUG_GUI
using UnityEngine;
using System.Collections;

public class PiyoPiyoController : MonoBehaviour {
	
	public static string state = "play";
	public static int selectedStage = 0;
	
	public GameObject obj_Sound;
	public GameObject obj_BGM;
	
	public bool fl_GameStart;
	public float timer_GameStart;

	public ScoreManager scoreManager;
	public PiyoPiyoManager piyoManager;
	
	public GameObject obj_Effect_Confetti;

	public int result_rank; // 結果用ランクイン順位を保持
		
	// Use this for initialization
	void Start () {
		SelectStage.newStage_Release = false;
		state = "play";
		//state = "clear";
		//state = "end";
		//SelectStage.newStage_Release = true;
		fl_GameStart = false;
		timer_GameStart = 4.5f;
		scoreManager = new ScoreManager(selectedStage);
		piyoManager = new PiyoPiyoManager(selectedStage);

		obj_Sound = CommonSE.Instance.gameObject;
		obj_BGM = GameBGM.Instance.gameObject;

		obj_BGM.SendMessage("Stop");
		obj_BGM.SendMessage("PlayBGM");
	}
	
	// Update is called once per frame
	void Update () {
		if (state == "play") {
			
						if (!fl_GameStart)
								CountDownGameStart ();
						else
								piyoManager.Run ();
			
			
				} else if (state == "stop") {

				} else if (state == "resultComfirm") {

				}
	}

#if DEBUG_GUI
	void OnGUI(){
		int sw = FixedScreen.width;
		int sh = FixedScreen.height;

		if (GetComponent<GUIController>().fl_debug_disp) {
			GUI.color = Color.black;
			string strSpeed = "Speed: " + piyoManager.speedData.current_SpeedLevel;
			GUI.Label(OffsetRect.GetOffsetRect(0, 0, sw, sh), strSpeed);
		}
	}
#endif// DEBUG_GUI


	public void Judge(bool judge, int speedLevel, Vector3 pos){
		if(state == "play"){
			if(judge){
		//		Debug.Log("correct");
				PlayerPrefs.SetInt("Total", PlayerPrefs.GetInt("Total")+1);
				pos = GameObject.FindWithTag("MainCamera").GetComponent<Camera>().WorldToScreenPoint(pos);
				pos.y = FixedScreen.height - pos.y;
				GetComponent<GUIController>().PointShow(scoreManager.PlusPoint(speedLevel), pos);	
				
				// 2コンボ以上かどうかの区別なく正解音とする
				obj_Sound.SendMessage("Play", 5); // 正解音
				/*
				if(scoreManager.comboCount > 1){
					obj_Sound.SendMessage("Play", 8);
				}else
					obj_Sound.SendMessage("Play", 5);
					*/
				
				if(scoreManager.ReturnClear() && PlayerPrefs.GetInt("Stage"+(PiyoPiyoController.selectedStage+1)) == 0){
					PlayerPrefs.SetInt("Stage" + (selectedStage+1).ToString(),1);
					SendMessage("StageClear");
					// Kyhov Editor
					//GameObject.FindWithTag("SubCamera").SendMessage("Clear");

					Instantiate(obj_Effect_Confetti);
					SelectStage.newStage_Release = true;
					//obj_BGM.SendMessage("Pause");
					//obj_Sound.SendMessage("Play",10);
					//state = "clear";
				}else{
					//if(scoreManager.score >= piyoManager.speedData.ReturnRelativeSpeedLevel() * (int)(scoreManager.stageClearCount / piyoManager.speedData.sub_SpeedLevel)){
					if(piyoManager.speedData.LevelUpCheck(scoreManager.correctCount)){
						piyoManager.speedData.SpeedUp();
						Debug.Log("up");
					}
				}
				
				if(PiyoPiyoManager.possible_Catch_piyoId >= 30)
					PiyoPiyoManager.possible_Catch_piyoId = 0;
				else
					PiyoPiyoManager.possible_Catch_piyoId++;
				
			}else{
		//		Debug.Log("uncorrect");
				GameObject.FindWithTag("MainCamera").SendMessage("Fl_On_Moving");
				Time.timeScale = 0;
				obj_Sound.SendMessage("Play", 6);
				scoreManager.Miss();
				if(scoreManager.ReturnGameOver()){
//					Debug.Log("a");
					// RecordRankすると今回の順位が記録されてしまうのでその前に保持
					result_rank = scoreManager.ReturnRank();
					Debug.Log("result_rank:"+result_rank);
					scoreManager.RecordRank();
					/*
					if(result_rank==1)
						MyUploader.Current.Uploader.Request( new GProNetworkPacket(ResourceManager.General.Application.GetAppMessage(GProNetworkPacket.AppActionDetailIDs.HighScore)).ToWWWForm() ) ;
					else if(scoreManager.ReturnClear())
						MyUploader.Current.Uploader.Request( new GProNetworkPacket(ResourceManager.General.Application.GetAppMessage(GProNetworkPacket.AppActionDetailIDs.StageClear)).ToWWWForm() ) ;
					*/
					state = "end";
				}
				if(PiyoPiyoManager.possible_Catch_piyoId >= 30)
					PiyoPiyoManager.possible_Catch_piyoId = 0;
				else
					PiyoPiyoManager.possible_Catch_piyoId++;
				
				/*GameObject[] objs = GameObject.FindGameObjectsWithTag("Target");
				foreach(GameObject obj in objs){
					obj.SendMessage("Miss");
					
					if(PiyoPiyoManager.possible_Catch_piyoId >= 30)
						PiyoPiyoManager.possible_Catch_piyoId = 0;
					else
						PiyoPiyoManager.possible_Catch_piyoId++;
				}*/
				
			}
		}
	
	}
	
	void CountDownGameStart(){
		timer_GameStart -= Time.deltaTime;
		if(timer_GameStart <= 0.0){
			fl_GameStart = true;
		} 
	}
	
	public static string ReturnState(){
		return state;	
	}
	

}
