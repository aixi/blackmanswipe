﻿using UnityEngine;
using System.Collections;

public class PiyoPiyoData {
	public int piyoID;
	
	public bool fl_Caught;
	public bool fl_Field;
	public Camera cam;
	public float cam_y;
	public int speedLevel = 0;
	public float[] array_Speed = new float[]{800, 900, 1050, 1200, 1400, 1600, 1800, 2050, 2300, 2550, 2850, 3150}; // いくちゃん調整
	//public float[] array_Speed = new float[]{500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1600, 2000, 2500};
	public float speed;
	//public float caught_y;
	public float caught_z;
	public float caught_mouse_x;
	
	public float border_x = 0;
	public float border_z = 30;
	public float onField_y = 3;
	
	public GameObject obj_Board;
	
	public int direction_Left =1;
	
	public int num_Material_Color;
	public int num_Literal;
	public bool direction_Correct;//true→左が正解,false→右が正解
	public int direction = 1;
	
	public bool fl_Fire = false;
	
	public bool onBoard = false;

	public PiyoPiyoData(int id, int s, GameObject board){
		piyoID = id;
		speedLevel = s;
		fl_Caught = false;
		fl_Field = false;
		cam_y = 28;
		speed = array_Speed[speedLevel-1];
		caught_z = 10;
		//caught_y = 10;
		border_x = 0;
		onField_y = 3;
		obj_Board = board;
		fl_Fire = false;
		
		cam = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
		
		SelectCorrect();
	}
	
	public void SelectCorrect(){
		SelectColor();
		SelectLiteral();
		
		/*if(num_Literal == 0){
			if(num_Material_Color == 0)
				direction_Correct = true;
			else if(num_Material_Color == 1)
				direction_Correct = false;
			else if(Random.Range(0,2) == 0)
				direction_Correct = true;	
			else
				direction_Correct = false;
		}else{
			if(num_Literal % 2 == 1)
				direction_Correct = true;
			else
				direction_Correct = false;
		}*/
		
		if(num_Material_Color == 0){
			direction_Correct = true;
		}else if(num_Material_Color == 1){
			direction_Correct = false;
		}else{
			if(num_Literal == 0){
				if(Random.Range(0,2) == 0)
					direction_Correct = true;	
				else
					direction_Correct = false;
			}else{
				if(num_Literal % 2 == 1)
					direction_Correct = true;
				else
					direction_Correct = false;
			}
		}
	}
	
	public void SelectColor(){
		while(true){
			num_Material_Color = Random.Range(0,3);//0→黄色,1→緑色,2→黒色
			if(PiyoPiyoController.selectedStage  != 1)
				break;	
			else{
				if(num_Material_Color != 2)
					break;
			}
		}
	}
	
	public void SelectLiteral(){
		while(true){
			num_Literal = Random.Range(0,5);//0→文字なし,1→黄,2→緑,3→左,4→右
			/*if(PiyoPiyoController.selectedStage  >= 6 || PiyoPiyoController.selectedStage == 3)
				break;	
			else if(PiyoPiyoController.selectedStage == 5 || PiyoPiyoController.selectedStage == 2){
				if(num_Literal <= 2)
					break;
			}else{
				num_Literal = 0;
				break;
			}*/
			if(PiyoPiyoController.selectedStage <= 3){
				num_Literal = 0;
				break;
			}else if(PiyoPiyoController.selectedStage == 4){
				if(num_Material_Color == 2)
					break;
				else{
					num_Literal = 0;
					break;
				}
			}else{
				break;
			}
		}
	}
	
}
