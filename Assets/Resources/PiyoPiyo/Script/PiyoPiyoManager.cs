﻿using UnityEngine;
using System.Collections;

public class PiyoPiyoManager {
	public CameraWork cameraWork;
	
	public GameObject obj_PiyoPiyo;
	public GameObject obj_Board;
	public Vector3 pos_Generate;
	
	public static int current_piyoId;
	public static int possible_Catch_piyoId;
	
	public TouchAction touchAction;
	public GameObject caughtGameObject = null;
	
	public float timer_Generate;
	public SpeedData speedData;
	
	private string path = "PiyoPiyo/Prefab/piyo 11";
	private string path_Board = "PiyoPiyo/Prefab/Board";
	
	public PiyoPiyoManager(int stage){
		cameraWork = GameObject.FindWithTag("MainCamera").GetComponent<CameraWork>();
		//pos_Generate = new Vector3(0,0,-45);
		possible_Catch_piyoId = 0;
		current_piyoId = 0;
		obj_PiyoPiyo = Resources.Load(path) as GameObject;
		obj_Board = Resources.Load(path_Board) as GameObject;
		
		speedData = new SpeedData(stage);
		timer_Generate = 0;
		
		touchAction = new TouchAction();
	}
	// Update is called once per frame
	public void Run () {
		if(timer_Generate < 0){
			timer_Generate = 1.5f / (float)Mathf.Sqrt(speedData.current_SpeedLevel);
			//timer_Generate = 2.5f / (float)Mathf.Sqrt(speedData.current_SpeedLevel);
			Generate();
		}else{
			timer_Generate -= Time.deltaTime;
		}
		
		
		if(Input.GetButtonDown("Fire1") && PiyoPiyoController.state == "play"){
			float distance = 0;
			caughtGameObject = touchAction.Catch(out distance);
			if(caughtGameObject != null){
				//PiyoPiyoData d = caughtGameObject.GetComponent<PiyoPiyo>().piyoData;
				PiyoPiyoData d = caughtGameObject.GetComponent<blackman>().piyoData;
				if(d.onBoard && !cameraWork.fl_move)
					caughtGameObject.SendMessage("Caught", distance);
			}
		}
		
		if(Input.GetButtonUp("Fire1")){
			if(caughtGameObject != null){
				//PiyoPiyoData d = caughtGameObject.GetComponent<PiyoPiyo>().piyoData;
				PiyoPiyoData d = caughtGameObject.GetComponent<blackman>().piyoData;
				if(caughtGameObject.tag == "Target" && d.fl_Caught){
					caughtGameObject.SendMessage("Release");
				}
				caughtGameObject = null;
			}
		}
		
	}
	
	void Generate(){
		GameObject board = Object.Instantiate(obj_Board) as GameObject;
		Vector3 pos = board.transform.position;
		//pos.y = 1.65f;
		pos.y = 20f;
		GameObject obj = Object.Instantiate(obj_PiyoPiyo, pos, Quaternion.identity) as GameObject;

		obj.GetComponent<blackman>().piyoData = new PiyoPiyoData( current_piyoId, speedData.current_SpeedLevel, board);
		//obj.GetComponent<PiyoPiyo>().piyoData = new PiyoPiyoData( current_piyoId, speedData.current_SpeedLevel, board);

		obj.SendMessage("ChangeMaterial");
		if(current_piyoId >= 30)
			current_piyoId = 0;
		else
			current_piyoId++;
	}
}
