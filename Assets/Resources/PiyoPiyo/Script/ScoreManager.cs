﻿using UnityEngine;
using System.Collections;

public class ScoreManager {
	private int[] array_Base_GetPoints = new int[]{1,2,3,4,5,6,7,8,9,10};
	
	/*
	 * いくちゃん調整 2013/04/06a
	 * 次ステージ開放スコア
	 * stage1…  5,000点で合格（stage2開放
	 * stage2… 20,000点で合格（stage3開放
	 * stage3… 40,000点で合格（stage4開放
	 * stage4… 50,000点で合格（stage5開放
	 * stage5… 80,000点で合格（stage6開放
	 * stage6…120,000点で合格
	 */

	private int[] array_StageClear_Count = new int[]{ 5000, 20000, 40000, 50000, 80000, 120000, 100,100,100,100};//次ステージ開放条件 2013/04/06a いくちゃん
	//private int[] array_StageClear_Count = new int[]{ 10000, 30000, 40000, 50000, 70000, 100000, 100,100,100,100};//次ステージ開放条件 2013/04/02a いくちゃん
	//private int[] array_StageClear_Count = new int[]{50,50,50,50,50,50,50,50,50,50};//次ステージ開放条件 for all debug
	//private int[] array_StageClear_Count = new int[]{50,25000,75000,150000,250000,400000,100,100,100,100};//次ステージ開放条件 nishiura 軽め debug
	
	private int[] array_Miss_Count = new int[]{3,3,3,3,3,3,3,3,3,3};//各ステージ残機数
	private float[] array_SpeedRate = new float[]{1.1f,1.15f,1.2f,1.25f,1.3f,1.35f,1.4f,1.45f,1.5f,1.55f,1.6f,1.65f};
	private float[][] array_comborate = new float[][]{ new float[]{2,20,1.1f}, new float[]{21,50,1.2f}, new float[]{51,100,1.3f}, new float[]{101, 200, 1.4f}, new float[]{201, 500, 1.6f}, new float[]{501, 1000, 1.8f}, new float[]{1001,10000,2.0f}};
	private int[] array_combobonus = new int[]{1000, 2000, 3000, 5000, 10000, 20000, 1000000};
	
	
	public int base_GetPoint;
	public int score;
	public int score_For_Show;
	public int comboCount;
	public int correctCount;
	public int missCount;
	public int stageClearCount;

	public const int MAX_SCORE = 99999999;
	
	public ScoreManager(int stage){
		base_GetPoint = array_Base_GetPoints[stage-1];
		score = 0;
		score_For_Show = 0;
		comboCount = 0;
		correctCount = 0;
		missCount = array_Miss_Count[stage-1];
		stageClearCount = array_StageClear_Count[stage-1];
	}
	
	public int PlusPoint(int speedLevel){
		comboCount++;
		correctCount++;
		if(comboCount == 10001)
			comboCount = 1;
//		int s = (int)(array_SpeedRate[speedLevel-1] * (float)base_GetPoint * Mathf.Pow(1.1f, comboCount-1));
		int s;
		if(comboCount >= 2)
			s = (int)(speedLevel * 100 * ReturnComboRate() + 0.5f); // 0.5f: 丸め誤差対策
		else
			s = (int)(speedLevel * 100);
		score += s;
		ClampScore();
		
		return s;
	}
	
	public void Miss(){
		missCount--;
		comboCount=0;
	}
	
	public bool ReturnClear(){
		if(score >= stageClearCount && PiyoPiyoController.selectedStage < 7)
			return true;
		else
			return false;
	}
	
	public bool ReturnGameOver(){
		if(missCount <= 0)
			return true;
		else
			return false;
	}
	
	public int ReturnRank(){
		for(int i=0;i<10;i++){
			//Debug.Log("score:" + score + ", Score"+(i+1).ToString()+":"+PlayerPrefs.GetInt("Score"+(i+1).ToString()));
			if(score > PlayerPrefs.GetInt("Score"+(i+1).ToString())){
				return i+1;
			}
		}
		
		return -1;
	}
	
	public void RecordRank(){
		int rank = ReturnRank();
		int s = score;
		int tmp;
		if(rank != -1){
			for(int i=rank;i<11;i++){
				tmp = PlayerPrefs.GetInt("Score"+i.ToString());
				PlayerPrefs.SetInt("Score"+i.ToString(),s);
				s = tmp;
			}
		}
	}
	
	public float ReturnComboRate(){
		//return comboCount * comboCount * comboCount;//debug
		for(int i=0;i<array_comborate.Length;i++){
			if( comboCount  >= array_comborate[i][0] && array_comborate[i][1] >= comboCount){
				if(comboCount == array_comborate[i][1]){
					GameObject.FindWithTag("GameController").SendMessage("WriteComboBonus", array_combobonus[i]);
					score += array_combobonus[i];
					ClampScore();
				}
				return array_comborate[i][2];
			}
		}
		
		return array_comborate[9][2];
	}
	
	public void WriteScoreForShow(){
		score_For_Show = score;	
	}

	private void ClampScore() {
		score = System.Math.Min(score, MAX_SCORE);
	}

}
