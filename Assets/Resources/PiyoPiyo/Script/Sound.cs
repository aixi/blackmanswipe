﻿using UnityEngine;
using System.Collections;

public class Sound : MonoBehaviour {
	
	public AudioClip audioClip_Correct;
	public AudioClip audioClip_Miss;
	
	public AudioClip[] array_audio;

	public void Correct(){
		audio.PlayOneShot(audioClip_Correct);
	}
	
	public void Miss(){
		audio.PlayOneShot(audioClip_Miss);
	}
	
	public void Play(int i){
		audio.PlayOneShot(array_audio[i]);
	}
	
	public void PlayBGM(){
		audio.Play();	
	}
	
}
