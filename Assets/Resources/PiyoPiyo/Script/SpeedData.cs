﻿using UnityEngine;
using System.Collections;

public class SpeedData {
	public int[][] speedLevelRange_eachStage = new int[][]{new int[]{1,7}, new int[]{2,8}, new int[]{3,9}, 
		new int[]{4,10}, new int[]{5,11}, new int[]{6,12}, 
		new int[]{3,12}, new int[]{5,10}, new int[]{5,11}, 
		new int[]{6,12}};
	
    // 20,50,100,200,500,1000羽でスピードUP
	public int[] speedLevelUpLine = new int[]{20,50,100,200,500,1000};
//    // 50,100,200,400,700,1000羽でスピードUP
//	public int[] speedLevelUpLine = new int[]{50,100,200,400,700,1000};
	//public int[] speedLevelUpLine = new int[]{2,5,10,11,12,20};

	public int current_SpeedLevel;
	public int max_SpeedLevel;
	public int min_SpeedLevel;
	public int sub_SpeedLevel;
	
	public SpeedData (int stage){
		min_SpeedLevel = speedLevelRange_eachStage[stage-1][0];
		max_SpeedLevel = speedLevelRange_eachStage[stage-1][1];
		sub_SpeedLevel = max_SpeedLevel - min_SpeedLevel + 1;
		current_SpeedLevel = min_SpeedLevel;
	}
	
	public void SpeedUp(){
//		Debug.Log("a");
		current_SpeedLevel++;
	}
	
	public int ReturnRelativeSpeedLevel(){
		return current_SpeedLevel - min_SpeedLevel;	
	}
	
	public bool LevelUpCheck(int count){
		if(max_SpeedLevel > current_SpeedLevel){
			if(count >= speedLevelUpLine[ReturnRelativeSpeedLevel()]){
				return true;	
			}
		}
		
		return false;
	}
	
}
