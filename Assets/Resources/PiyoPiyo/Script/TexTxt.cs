using UnityEngine;
using System.Collections;

public class TexTxt {
	public Texture tex_txt_Help;
	public Rect[] rects_txt_Help;
	public float[] y_txt_Help = new float[]{0.085f,0.255f,0.395f,0.526f,0.64f,0.794f};
	
	
	public Texture tex_txt_Result;
	public Rect[] rects_txt_Result;
	
	public Texture tex_txt_Stage;
	public Rect[] rects_txt_Stage;
	public Rect rect_txt_Stage_Logo;
	
	public Texture tex_txt_Condition;
	public Rect[] rects_txt_Condition;
	public Rect rect_txt_Condition_1;
	
	public Texture tex_txt_CountDown;
	public Rect[] rects_txt_CountDown;
	public Rect rect_txt_CountDown_ready;
	public Rect rect_txt_CountDown_start;
	
	public Texture tex_txt_StageSelect;
	public Rect[] rects_txt_StageSelect;
	private float[] y_txt_StageSelect = new float[]{0.05f,0.208f, 0.37f, 0.53f, 0.688f,0.847f};
	
	public TexTxt(){
//		tex_txt_Help = (Texture)Resources.Load("PiyoPiyo/Texture402/help_txt_001"); // Replace ready 
		tex_txt_Help = (Texture)Resources.Load("PiyoPiyo/Textur_Fix/help_txt_001"); // Replace ready
		rects_txt_Help = new Rect[6];
		
		tex_txt_Result = (Texture)Resources.Load("PiyoPiyo/Texture309/result_hukidashi_txt"); // Replace ready
		rects_txt_Result = new Rect[5];
		
		tex_txt_Stage = (Texture)Resources.Load("PiyoPiyo/Texture308/stage_txt"); // Replace ready
		rects_txt_Stage = new Rect[6];
		
		tex_txt_Condition = (Texture)Resources.Load("PiyoPiyo/Texture308/txt_next_stage_info_01"); 
		rects_txt_Condition = new Rect[10];
		
		tex_txt_CountDown = (Texture)Resources.Load("PiyoPiyo/Texture308/countdown"); // Replace ready
		rects_txt_CountDown = new Rect[3];
		
		tex_txt_StageSelect = (Texture)Resources.Load("PiyoPiyo/Texture309/stage_moji_02"); // Replace ready
		rects_txt_StageSelect = new Rect[6];
		
		SetUp();
		
	}
	
	public void Show_Txt_Help(Rect rect, int id){
		GUI.DrawTextureWithTexCoords(rect, tex_txt_Help, rects_txt_Help[id]);	
	}
	
	public void Show_Txt_Result(Rect rect, int id){
		GUI.DrawTextureWithTexCoords(rect, tex_txt_Result, rects_txt_Result[id]);	
	}
	
	public void Show_Txt_Stage(Rect rect, int id){
		//GUI.DrawTextureWithTexCoords(rect, tex_txt_Stage, rects_txt_Stage[id-1]);
        // rectのwidthとheight無視して等倍表示
		GUI.DrawTextureWithTexCoords(
                new Rect(rect.x, rect.y,
                    tex_txt_Stage.width * rects_txt_Stage[id-1].width,
                    tex_txt_Stage.height * rects_txt_Stage[id-1].height),
                tex_txt_Stage,
                rects_txt_Stage[id-1]);
	}
	
	public void Show_Txt_Stage_Logo(Rect rect){
        // rectのwidthとheight無視して等倍表示
		GUI.DrawTextureWithTexCoords(
                new Rect(rect.x, rect.y,
                    tex_txt_Stage.width * rect_txt_Stage_Logo.width,
                    tex_txt_Stage.height * rect_txt_Stage_Logo.height),
                tex_txt_Stage,
                rect_txt_Stage_Logo);
	}
	
	public void Show_Txt_Condition(Rect rect, int id){
		GUI.DrawTextureWithTexCoords(rect, tex_txt_Condition, rects_txt_Condition[id]);	
	}
	
	public void Show_Txt_Condition_1(Rect rect){
		GUI.DrawTextureWithTexCoords(rect, tex_txt_Condition, rect_txt_Condition_1);	
	}
	
	public void Show_Txt_CountDown(Rect rect, int id){
		GUI.DrawTextureWithTexCoords(rect, tex_txt_CountDown, rects_txt_CountDown[id-1]);
	}
	
	public void Show_Txt_CountDown_Ready(Rect rect){
		GUI.DrawTextureWithTexCoords(rect, tex_txt_CountDown, rect_txt_CountDown_ready);	
	}
	
	public void Show_Txt_CountDown_Start(Rect rect){
		GUI.DrawTextureWithTexCoords(rect, tex_txt_CountDown, rect_txt_CountDown_start);
	}
	
	public void Show_Txt_StageSelect(Rect rect, int id){
        // rectのwidthとheight無視して等倍表示
		GUI.DrawTextureWithTexCoords(
                new Rect(rect.x, rect.y,
                    tex_txt_StageSelect.width * rects_txt_StageSelect[id].width,
                    tex_txt_StageSelect.height * rects_txt_StageSelect[id].height),
                tex_txt_StageSelect,
                rects_txt_StageSelect[id]);
		//GUI.DrawTextureWithTexCoords(rect, tex_txt_StageSelect, rects_txt_StageSelect[id]);
	}
	
	
	public void SetUp(){
		for (int i=0;i<rects_txt_Help.Length;i++) {
			rects_txt_Help[i] = new Rect(0,y_txt_Help[i],1,0.13f);
		}
		
		for (int i=0;i<rects_txt_Result.Length;i++) {
			rects_txt_Result[i] = new Rect(0,i * 0.213f,1,0.13f);
		}
		
		for (int i=0;i<rects_txt_Stage.Length;i++) {
			rects_txt_Stage[i] = new Rect(0.271f + 0.087f*i,0, 0.09f,1);
		}
		
		rect_txt_Stage_Logo = new Rect(0,0,0.28f,1);
		
		for (int i=0;i<rects_txt_Condition.Length;i++){
			rects_txt_Condition[i] = new Rect(0.01f + 0.0316f*i,0,0.03f,1);
		}
		
		rect_txt_Condition_1 = new Rect(0.33f, 0, 0.25f, 1);
		
		for (int i=0;i<rects_txt_CountDown.Length;i++){
			rects_txt_CountDown[i] = new Rect(0.698f - 0.309f*i,0.55f,0.26f,0.375f);
		}
		
		rect_txt_CountDown_ready = new Rect(0, 0.325f, 1,0.208f);
		rect_txt_CountDown_start = new Rect(0, 0.04f, 1.0f, 0.26f);
		
		for(int i=0;i<rects_txt_StageSelect.Length;i++){
            float h = 1f / 6f;
			rects_txt_StageSelect[i] = new Rect(0, 1f - (i+1)*h, 1f, h);
			//rects_txt_StageSelect[i] = new Rect(0, y_txt_StageSelect[rects_txt_StageSelect.Length-1-i], 1, 0.115f);
		}
		
	}
	
	
	
	public void ShowTxtConditionZero( int score, int digit, Rect rect, int needDigit){
		if(needDigit <= 0)
			return;
		
		if(needDigit < digit){
			while(true){
				if(needDigit == 0)
					break;
				Show_Txt_Condition( new Rect(rect.x - rect.width * (needDigit - 1) * 0.9f, rect.y, rect.width, rect.height), 9);
				needDigit--;
			}
		}else{
		
			while(true){
				
				if(needDigit == 0)
					break;
				if(needDigit > digit){
				//	Show_Txt_Condition( new Rect(rect.x - rect.width * (needDigit - 1) * 0.9f, rect.y, rect.width, rect.height), 0);
				}else{
					Show_Txt_Condition( new Rect(rect.x - rect.width * (digit - 1) * 0.9f, rect.y, rect.width, rect.height), score/(int)Mathf.Pow(10, digit-1));
					score -= (score/(int)Mathf.Pow(10, digit-1)) * (int)Mathf.Pow(10, digit-1);
					digit--;
				}
					
				needDigit--;

			}
		}
	}
}
