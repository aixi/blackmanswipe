﻿using UnityEngine;
using System.Collections;

public class TouchAction{
	public float range = 0.05f;
	
	public TouchAction(){
		range = (int)(range*FixedScreen.width);
	}
	
	public GameObject Catch (out float distance) {
		/*for(int i=-(int)range; i<range; i++){
			for(int j=-(int)range; j<range; j++){
				Vector2 pos = new Vector2(Input.mousePosition.x+i,Input.mousePosition.y+j);
				Ray ray = GameObject.FindWithTag("MainCamera").GetComponent<Camera>().ScreenPointToRay (pos);
				RaycastHit hit = new RaycastHit();
				if (Physics.Raycast(ray, out hit, 100)) {
					distance = hit.distance;
					if(hit.collider.gameObject.tag == "Target")
						return hit.collider.gameObject;
				}
			}
		}*/
		if(TouchInScreen()){
			GameObject[] objs = GameObject.FindGameObjectsWithTag("Target");
			foreach( GameObject obj in objs){
				//if(obj.GetComponent<PiyoPiyo>().piyoData.piyoID == PiyoPiyoManager.possible_Catch_piyoId && obj.transform.position.z < 30.5f){
				if(obj.GetComponent<blackman>().piyoData.piyoID == PiyoPiyoManager.possible_Catch_piyoId && obj.transform.position.z < 30.5f){
					distance = Vector3.Distance(GameObject.FindWithTag("MainCamera").transform.position,obj.transform.position);
					return obj;
				}
			}
		}

		distance = 0;
		return null;
	}
	
	public bool TouchInScreen(){
		if(Input.mousePosition.x >= 0 && Input.mousePosition.x <= FixedScreen.width && Input.mousePosition.y >= 200 && Input.mousePosition.y <= FixedScreen.height)
			return true;
		else
			return false;
	}
	
}
