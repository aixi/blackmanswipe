﻿using UnityEngine;

public class AdUnderlay : SingletonMonoBehaviour<AdUnderlay> {

	public Texture tex_AdUnderlay;
	public bool isView;
    void Start ()
    {
		isView = true;
    }

    void OnGUI()
    {
		if (isView) {
			GUI.DrawTexture( OffsetRect.GetOffsetRect( 0, -100, tex_AdUnderlay.width, tex_AdUnderlay.height), tex_AdUnderlay);
		}
    }
}

