﻿using UnityEngine;
using System.Collections;

public class AspectUtility : MonoBehaviour {
	// 縦横比です。インスペクタから修正します。
	public float m_x_aspect = 4.0f;
	public float m_y_aspect = 3.0f;

    public bool fl_StageClearCamera = false;

	void Awake(){
		// カメラを検索します。
		Camera camera = GetComponent<Camera>();
		// 指定された比率からサイズを出します。
		Rect rect = calcAspect(m_x_aspect, m_y_aspect);
		// カメラの比率を変更します。
		camera.rect = rect;
	}

	// アスペクト比計算
	public Rect calcAspect(float width, float height){
		float target_aspect = width / height;
		float window_aspect = (float)Screen.width / (float)Screen.height;
		float scale_height = window_aspect / target_aspect;

		// 広告を含めたアスペクト比算出（Y座標計算のため）
		float ad_height = 100f;
		float target_aspect_with_ad = width / (height + ad_height);
		float scale_height_with_ad = window_aspect / target_aspect_with_ad;

		Rect rect = new Rect(0.0f, 0.0f, 1.0f, 1.0f);
		if(1.0f > scale_height){
			rect.x = 0;

			rect.y = (1.0f - scale_height_with_ad) / 2.0f; // 広告を上に配置する場合
			//rect.y = (1.0f - scale_height) / 2.0f;

			rect.width = 1.0f;
			rect.height = scale_height;
		}
		else{
			float scale_width = 1.0f / scale_height;
			rect.x = (1.0f - scale_width) / 2.0f;
			rect.y = 0.0f;
			rect.width = scale_width;
			rect.height = 1.0f;
		}

		if (fl_StageClearCamera)
			rect.height /= 10f;

		return rect;
	}
}

