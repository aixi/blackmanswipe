﻿using UnityEngine;
using System.Collections;

// タイトルなど最初にロードするシーンに、
// AudioManagerを持ったオブジェクトを置いておいてずっと使う。
public class AudioManager : SingletonMonoBehaviour<AudioManager> {

    // インスペクタで、AudioSourceを持ったオブジェクトをリンクしておく
	public GameObject[] objs_Audio;

    private bool fl_Mute;

	// Use this for initialization
	void Start () {
        Instance.fl_Mute = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public static void SetMute(bool bMute)
    {
        foreach(GameObject obj in Instance.objs_Audio){
            obj.audio.mute = bMute;
        }
        Instance.fl_Mute = bMute;
    }

    public static bool IsMute()
    {
        return Instance.fl_Mute;
    }
}

