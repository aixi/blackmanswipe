﻿using UnityEngine;
using System.Collections;

public class Billboard : MonoBehaviour
{
	void Update()
	{
		Vector3 rot = Camera.main.transform.TransformDirection(Vector3.forward);
		transform.rotation = Quaternion.LookRotation(-rot);
	}
}

