﻿using UnityEngine;
using System.Collections;

// 共通SE
public class CommonSE : SingletonMonoBehaviour<CommonSE> {

    private Sound sound;

	// Use this for initialization
	void Start () {
        Instance.sound = GetComponent<Sound>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public static void PlayButtonSE() {
        Instance.sound.Play(0); // ボタンSE
    }
}

