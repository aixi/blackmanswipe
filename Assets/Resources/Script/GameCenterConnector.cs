﻿//#define DEBUG_GUI
using UnityEngine;
using System.Collections;
using UnityEngine.SocialPlatforms;
using UnityEngine.SocialPlatforms.GameCenter;
 
public class GameCenterConnector : SingletonMonoBehaviour<GameCenterConnector> {

	private const string LeaderBoardID_BestScore = "ishi.kurosuwa";

    // GameCenterのユーザ認証を行う
    void Start ()
    {
		AuthenticationCheck();
    }
 

#if DEBUG_GUI
    void OnGUI()
    {
        // スコアの送信
        if (GUI.Button(new Rect(0, 0, 200, 50), "Send Score"))
        {
			/*
            int score = 10;
            Social.ReportScore(score, "Butsukyu.BestScore", SendScoreCallback);
			*/
            //Social.ReportScore(score, "me.mkgames.touchandcrash.highscore", SendScoreCallback);
			ReportScore(100);
        }
 
		/*
        // 実績の送信
        if (GUI.Button(new Rect(0, 100, 200, 50), "Send Achievement"))
        {
            Social.ReportProgress("me.mkgames.touchandcrash.achievement", 50.0f, SendAchievementCallback);
        }
		*/
 
        // スコアボードの閲覧
        if (GUI.Button(new Rect(250, 0, 200, 50), "Show LeaderBoard"))
        {
            //Social.ShowLeaderboardUI();
			ShowLeaderboardUI();
        }
 
		/*
        // 実績の閲覧
        if (GUI.Button(new Rect(0, 300, 200, 50), "Show Achievement"))
        {
            Social.ShowAchievementsUI();
        }
		*/
    }
#endif// DEBUG_GUI
 
    // スコアの送信が完了したときに呼ぶ関数
    static void SendScoreCallback(bool isSuccess)
    {
        Debug.Log("Send Score Finished:" + isSuccess);
    }
 
    // 実績の送信が完了したときに呼ぶ関数
    static void SendAchievementCallback(bool isSuccess)
    {
        Debug.Log("Send Achievement Finished:" + isSuccess);
    }

	// 認証されてなかったらする。
	// 認証されていたらtrueを返す。
	static bool AuthenticationCheck()
	{
        if (Social.localUser.authenticated) {
			Debug.Log("Already Authenticated");
			return true;
		}

		Debug.Log("Not Authenticated");

        Social.localUser.Authenticate (success =>
        {
            if (success)
            {
                Debug.Log("Authentication Successful");
                string userInfo = "Username:" + Social.localUser.userName + "¥nUser ID:" + Social.localUser.id + "¥nInUnderage:" + Social.localUser.underage;
                Debug.Log(userInfo);
            }
            else
            {
                Debug.Log("Failed");
            }
        });
		
		return false;
	}


	public static void ShowLeaderboardUI()
	{
		// 認証済みのときリーダーボード表示
		// 認証されてなかったら認証ダイアログを出す
		if ( AuthenticationCheck() )
            Social.ShowLeaderboardUI();
	}

	public static void ReportScore(int score)
	{
		// 認証済みのときのみスコア送信
		// 認証されてなかったらなにもしない
		if ( Social.localUser.authenticated ) {
            Social.ReportScore(score, LeaderBoardID_BestScore, SendScoreCallback);
		}
	}

}

