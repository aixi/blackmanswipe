﻿using UnityEngine;
using System.Collections;

public class HelpScreen : MonoBehaviour {
	public Texture tex_Help_Back;
	public Texture tex_Help_Logo;
	public Texture tex_Title_Back;
	
	void OnGUI () {
		int sw = FixedScreen.width;
		int sh = FixedScreen.height;
		GUI.DrawTexture( OffsetRect.GetOffsetRect( 0, 0, sw, sh),tex_Title_Back);
		GUI.DrawTexture( OffsetRect.GetOffsetRect( sw * 0.125f, sh * 0.125f, sw *0.75f, sh*0.75f),tex_Help_Back);
		GUI.DrawTexture( OffsetRect.GetOffsetRect( sw / 3, sh / 2 - sh * 3 / 16, sw / 3, sh * 3 / 8),tex_Help_Logo);
		
		if(Input.GetButtonDown("Fire1") && Input.mousePosition.x >= 0 && Input.mousePosition.x <= sw && Input.mousePosition.y >= 0 && Input.mousePosition.y <= sh){
			Application.LoadLevel("Title");	
		}
	}
}
