using UnityEngine;

// 広告配置を考慮したオフセット付きRectを取得するためのクラス
public class OffsetRect  {

#if UNITY_IPHONE
	private static int AD_HEIGHT = 100; // 広告の高さ
#else
	private static int AD_HEIGHT = 0; // 広告の高さ
#endif// UNITY_IPHONE

	private static bool first = true;
	private static int sw = 0;
	private static int sh = 0;
	private static int offset_x = 0;
	private static int offset_y = 0;

	public static Rect GetOffsetRect(float left, float top, float width, float height)
	{
		if (first) {
			sw = Screen.width;
			sh = Screen.height;

			// iPhone4, iPhone5系のみで良いのでひとまず縦方向のみ対応
			offset_y = (sh - AD_HEIGHT - FixedScreen.height) / 2 + AD_HEIGHT;
		}
		return new Rect(left + offset_x, top + offset_y, width, height);
	}

	public static Rect GetOffsetRect(Rect rc)
	{
		return GetOffsetRect(rc.left, rc.top, rc.width, rc.height);
	}


}

