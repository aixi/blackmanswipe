﻿using UnityEngine;
using System.Collections;

public class RankingScreen : MonoBehaviour {
	
	public Texture tex_Back;
	
	public Texture tex_Ranking_Back;
	public Texture tex_Ranking;
	public Texture tex_RankScore;
	
	public Texture tex_Total;
	public Texture tex_Hiki;
	
	public GUIStyle style_Back;
	
	public TexNumber texNumber;
	
	public AudioClip se_Button;
	
	void Start(){
		texNumber = new TexNumber();

		//PlayerPrefs.SetInt ("Score1", 0);
		//PlayerPrefs.SetInt ("Score2", 0);
		//PlayerPrefs.SetInt ("Score3", 0);
		//PlayerPrefs.SetInt ("Total", 0);
	}
	
	void OnGUI(){
		int sw = FixedScreen.width;
		int sh = FixedScreen.height;
		
		
		GUI.DrawTexture(OffsetRect.GetOffsetRect(0,0,sw,sh), tex_Back);
		
		GUI.DrawTexture(OffsetRect.GetOffsetRect(sw * 0.085f, sh * 0.01f, tex_Ranking_Back.width, tex_Ranking_Back.height), tex_Ranking_Back);
		GUI.DrawTexture(OffsetRect.GetOffsetRect(sw * 0.39f - tex_Ranking.width / 2, sh * 0.033f, tex_Ranking.width, tex_Ranking.height), tex_Ranking);
		
		if(GUI.Button(OffsetRect.GetOffsetRect(sw * 0.9f - sh * 0.053f, sh * 0.01f, style_Back.normal.background.width, style_Back.normal.background.height), "", style_Back)){
			CommonSE.PlayButtonSE();
			//Invoke("DestroyBGM", 0.5f);
			Application.LoadLevel("Title");//タイトルへ
		}
		
		GUI.DrawTexture(OffsetRect.GetOffsetRect(sw * 0.08f,sh * 0.18f,tex_RankScore.width,tex_RankScore.height), tex_RankScore);
		
//		GUI.Label(OffsetRect.GetOffsetRect(sw * 0.325f,sh * 0.2f, sw, sh), "Total: " + PlayerPrefs.GetInt("total").ToString() + "m");//総飛距離
		
		for(int i=1; i<=3; i++){
			
			texNumber.ShowRankBigZero(i, texNumber.ReturnDigit(i), OffsetRect.GetOffsetRect(sw*0.14f, sh*(0.28f + 0.255f*0.5f*(i-1)), sw*0.27f*0.36f,sh*0.25f*0.40f ),2);
			texNumber.ShowRankBig(PlayerPrefs.GetInt("Score" + i.ToString()), texNumber.ReturnDigit(PlayerPrefs.GetInt("Score" + i.ToString())), OffsetRect.GetOffsetRect(sw*0.85f, sh*(0.28f + 0.255f*0.5f*(i-1)), sw*0.27f*0.36f,sh*0.25f*0.40f ));
		//	texNumber.Show_Meter(OffsetRect.GetOffsetRect(sw*0.86f, sh*(0.1f + 0.08f*i), sw*0.1f, sh*0.08f ));
			//GUI.Label(OffsetRect.GetOffsetRect(sw*0.3f,sh*0.3f + sh*0.05f*(i-1),sw,sh),i.ToString() + "位");
			//GUI.Label(OffsetRect.GetOffsetRect(sw*0.6f,sh*0.3f + sh*0.05f*(i-1),sw,sh),PlayerPrefs.GetInt(i.ToString()).ToString());
		}
		
		GUI.DrawTexture( OffsetRect.GetOffsetRect(sw*0.4f - tex_Total.width / 2, sh * 0.79f, tex_Total.width, tex_Total.height),tex_Total);
		texNumber.ShowRankBig(PlayerPrefs.GetInt("Total"), texNumber.ReturnDigit(PlayerPrefs.GetInt("Total")), OffsetRect.GetOffsetRect(sw*0.70f, sh * 0.86f, sw*0.23f*0.5f, sh*0.25f*0.5f ));
		GUI.DrawTexture( OffsetRect.GetOffsetRect(sw*0.88f - tex_Hiki.width / 2, sh * 0.89f, tex_Hiki.width, tex_Hiki.height),tex_Hiki);
	
		
		
		//texNumber.Show_Big(9,OffsetRect.GetOffsetRect(0,0,sw,sh));
		
	}
	
	/*
	void DestroyBGM(){
			Destroy(GameObject.FindWithTag("Item"));
			Application.LoadLevel("Title");//タイトルへ
	}
	*/
}
