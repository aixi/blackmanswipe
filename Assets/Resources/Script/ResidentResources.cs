﻿using UnityEngine;
using System.Collections;
[ExecuteInEditMode]
// 常駐リソース
public class ResidentResources : SingletonMonoBehaviour<ResidentResources> {

	public Texture[] texs_asobikata;
	public Texture tex_piyo;
	public TexTxt texTxt;

	public GUIStyle style_GoButton;
	public Texture tex_Go;

	public Texture tex_Mask;

	void Awake() {
		texTxt = new TexTxt();
	}

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}

	public static Texture[] GetTexsAsobikata() {
		return Instance.texs_asobikata;
	}
	public static Texture GetTexPiyo() {
		return Instance.tex_piyo;
	}
	public static TexTxt GetTexTxt() {
		return Instance.texTxt;
	}
	public static GUIStyle GetStyleGoButton() {
		return Instance.style_GoButton;
	}
	public static Texture GetTexGo() {
		return Instance.tex_Go;
	}

	public static void DrawBGScroll() {
		int sw = FixedScreen.width;
		int sh = FixedScreen.height;
		Texture tex = Instance.tex_Mask;
		GUI.DrawTextureWithTexCoords(
				OffsetRect.GetOffsetRect(0, 0, sw, sh),
				tex,
				OffsetRect.GetOffsetRect(
					(Time.time / 6f) - Mathf.Floor(Time.time / 6f), // 0f〜1fに収める。大きくなると画像乱れる。
					(Time.time / 6f) - Mathf.Floor(Time.time / 6f),
					(float)sw/tex.width, (float)sh/tex.height));
	}
}

