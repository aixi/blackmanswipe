﻿// #define DEBUG_GUI
using UnityEngine;
using System.Collections;

public class SelectStage : MonoBehaviour {
	
	public Texture tex_Select;
	
	public Texture tex_Lock;
	//public Texture[] tex_Stage;
	
	public Texture tex_Title;
	
	public GUIStyle[] tex_Stage;
	
	public GUIStyle style_Back;
	
	public bool possible_Touch;
	
	public Texture[] texs_Help_For_First;
	
	private bool play_first;
	
	public Texture tex_Select_Explain;
	public Texture tex_Stage_Txt;
	
	public GUIStyle style_possible_Stage;
	public Texture tex_possible_Stage;
	public Texture tex_impossible_Stage;
	
	public static bool newStage_Release;
	public Texture tex_mask;
	public Texture tex_window;
	public Texture tex_txt;
	public Texture tex_ok;
	public GUIStyle style_OkButton;
	
//	public Texture tex_piyo;
	private Texture[] texs_asobikata;
	
//	public GUIStyle style_GoButton;
//	public Texture tex_Go;
	
	private static TexTxt texTxt;

	private bool[] fl_StageOpened = new bool[6];

	// Use this for initialization
	void Start () {
//		newStage_Release = true;
		//PlayerPrefs.SetInt("Initialize", 0);
		play_first = false;
		possible_Touch = true;
		
		texTxt = ResidentResources.GetTexTxt();
		//texTxt = new TexTxt();

		// 常駐リソースからセット
		texs_asobikata = ResidentResources.GetTexsAsobikata();

		UpdateStageOpenedState();
	}

	void UpdateStageOpenedState() {
		for (int i=0; i<6; i++){
			fl_StageOpened[i] = PlayerPrefs.GetInt("Stage"+(i+1).ToString()) != 0;
		}
		fl_StageOpened[0] = true;
	}
	
	void OnGUI () {
		int sw = FixedScreen.width;
		int sh = FixedScreen.height;
		
		ResidentResources.DrawBGScroll();

		if(!play_first){

			GUI.DrawTexture( OffsetRect.GetOffsetRect( sw * 0.05f, sh * 0.03f, tex_Select.width, tex_Select.height),tex_Select);
			GUI.DrawTexture( OffsetRect.GetOffsetRect( sw * 0.5f-tex_Select_Explain.width/2, sh * 0.15f, tex_Select_Explain.width, tex_Select_Explain.height),tex_Select_Explain);
			
			for (int i=0; i<6; i++){
				if(!fl_StageOpened[i]){
				//if(PlayerPrefs.GetInt("Stage"+(i+1).ToString()) == 0){
					GUI.DrawTexture( OffsetRect.GetOffsetRect( sw * 0.5f - tex_impossible_Stage.width/2, sh * (0.278f + 0.114f * i), tex_impossible_Stage.width, tex_impossible_Stage.height),tex_impossible_Stage);
				}else{
//					if(GUI.Button(OffsetRect.GetOffsetRect(sw * 0.5f - style_possible_Stage.normal.background.width / 2, sh * (0.278f + i * 0.114f), style_possible_Stage.normal.background.width, style_possible_Stage.normal.background.height),"",style_possible_Stage)){
					if(GUI.Button(OffsetRect.GetOffsetRect(sw * 0.5f - tex_possible_Stage.width / 2, sh * (0.278f + i * 0.114f), tex_possible_Stage.width, tex_possible_Stage.height),tex_possible_Stage,style_possible_Stage)){
						if(fl_StageOpened[i] && possible_Touch){
						//if(PlayerPrefs.GetInt("Stage"+(i+1).ToString()) != 0 && possible_Touch){
						//GameController.selectedStage = i+1;
							PiyoPiyoController.selectedStage = i+1;
							PlayerPrefs.SetInt("LastSelectedStage", PiyoPiyoController.selectedStage);
							if(PlayerPrefs.GetInt( "Stage"+PiyoPiyoController.selectedStage.ToString() ) == 1 && i != 0 && i != 5){
								play_first = true;
								// PlayerPrefs.SetInt("Stage"+PiyoPiyoController.selectedStage.ToString(), 2);
							}else{
								possible_Touch = false;
								Application.LoadLevel("MainPiyoPiyo");
								//StartCoroutine("SoundPlay", 0);
							}
							CommonSE.PlayButtonSE();
						//Application.LoadLevel("Main2");
						}
					}
				}
				texTxt.Show_Txt_StageSelect(OffsetRect.GetOffsetRect(sw * 0.193f, sh * (0.271f + 0.1187f * i),sw * 0.6f,sh * 0.1f),i);
			}
			
			
			//GUI.DrawTexture( OffsetRect.GetOffsetRect( sw * 0.5f-tex_Stage_Txt.width/2, sh * 0.28f, tex_Stage_Txt.width, tex_Stage_Txt.height),tex_Stage_Txt);
			
			
			if(GUI.Button(OffsetRect.GetOffsetRect(sw * 0.9f - sh * 0.045f, sh * 0.01f, style_Back.normal.background.width, style_Back.normal.background.height), "", style_Back)){
				//Application.LoadLevel("Title");
				possible_Touch = false;
				CommonSE.PlayButtonSE();
				//Application.LoadLevel("Title");
				//StartCoroutine("SoundPlay", 1);
				//Invoke("DestroyBGM", 0.5f);
				Application.LoadLevel("Title");//タイトルへ
			}
			
#if DEBUG_GUI
			bool fl_reset = GUI.Button(OffsetRect.GetOffsetRect(sw * 0.0f,sh * 0.11f,sw * 0.2f,sh * 0.05f), "Reset");
			if(fl_reset && possible_Touch){
				Reset();
				UpdateStageOpenedState();
			}
			
			bool fl_allClear = GUI.Button(OffsetRect.GetOffsetRect(sw * 0.25f,sh * 0.11f, sw * 0.2f,sh * 0.05f), "AllClear");
			if(fl_allClear && possible_Touch){
				AllClear();	
				UpdateStageOpenedState();
			}

			bool fl_uraguchi = GUI.Button(OffsetRect.GetOffsetRect(sw * 0.50f,sh * 0.11f, sw * 0.2f,sh * 0.05f), "Uraguchi");
			if(fl_uraguchi && possible_Touch){
				for (int i=0; i<6; i++){
					fl_StageOpened[i] = true;
				}
			}

			GUI.color = Color.black;
			for (int i=0; i<6; i++) {
				GUI.Label(OffsetRect.GetOffsetRect(sw * 0.02f,sh * (0.325f + i * 0.114f), sw * 0.2f,sh * 0.05f),
						(PlayerPrefs.GetInt("Stage"+(i+1).ToString()).ToString())
						);
			}
			GUI.Label(OffsetRect.GetOffsetRect(sw * 0.02f,sh * 0.97f, sw * 0.9f,sh * 0.05f),
						(PlayerPrefs.GetInt("Stage"+(6+1).ToString()).ToString()) + " <- Final Stage Cleared."
						);

#endif// DEBUG_GUI
			
			
			/*if(newStage_Release){
				GUI.DrawTexture(OffsetRect.GetOffsetRect( 0,0,tex_mask.width, tex_mask.height), tex_mask);
				GUI.DrawTexture(OffsetRect.GetOffsetRect(sw * 0.5f - tex_window.width / 2, sh*0.34f, tex_window.width, tex_window.height),tex_window);
				GUI.DrawTexture(OffsetRect.GetOffsetRect(sw * 0.5f - tex_txt.width/2, sh*0.45f , tex_txt.width, tex_txt.height), tex_txt);

				if(GUI.Button(OffsetRect.GetOffsetRect(sw * 0.5f - style_OkButton.normal.background.width/2, sh * 0.55f, style_OkButton.normal.background.width, style_OkButton.normal.background.height), "", style_OkButton))
					newStage_Release = false;

				GUI.DrawTexture(OffsetRect.GetOffsetRect(sw * 0.5f - tex_ok.width/2, sh*0.57f , tex_ok.width, tex_ok.height), tex_ok);
			}*/
			
			
		}else{
			
			ShowFirstHelp(sw, sh, ref possible_Touch);
		}
		
		
	}

	public static void ShowFirstHelp(int sw, int sh, ref bool possible_Touch) {
		Texture tex_piyo = ResidentResources.GetTexPiyo();
		GUI.DrawTexture( OffsetRect.GetOffsetRect(sw*0.5f-tex_piyo.width/2, sh * 0.6f, tex_piyo.width, tex_piyo.height),tex_piyo);

		Texture[] texs = ResidentResources.GetTexsAsobikata();
		switch(PiyoPiyoController.selectedStage){
			case 2:
				GUI.DrawTexture(OffsetRect.GetOffsetRect( sw*0.5f-texs[2].width/2,sh*0.2f, texs[2].width, texs[2].height),texs[2]);
				texTxt.Show_Txt_Help(OffsetRect.GetOffsetRect(sw * 0.1f, sh * 0.625f, sw * 0.8f, sh * 0.15f),2); // 2行
				break;
			case 3:
				GUI.DrawTexture(OffsetRect.GetOffsetRect( sw*0.5f-texs[3].width/2,sh*0.2f, texs[3].width, texs[3].height),texs[3]);
				texTxt.Show_Txt_Help(OffsetRect.GetOffsetRect(sw * 0.1f, sh * 0.605f, sw * 0.8f, sh * 0.15f),3); // 3行
				break;
			case 4:
				GUI.DrawTexture(OffsetRect.GetOffsetRect( sw*0.5f-texs[4].width/2,sh*0.2f, texs[4].width, texs[4].height),texs[4]);
				texTxt.Show_Txt_Help(OffsetRect.GetOffsetRect(sw * 0.1f, sh * 0.605f, sw * 0.8f, sh * 0.15f),1); // 3行
				break;
			case 5:
				GUI.DrawTexture(OffsetRect.GetOffsetRect( sw*0.5f-texs[5].width/2,sh*0.2f, texs[5].width, texs[5].height),texs[5]);
				texTxt.Show_Txt_Help(OffsetRect.GetOffsetRect(sw * 0.1f, sh * 0.605f, sw * 0.8f, sh * 0.15f),0); // 3行
				break;
		}

		GUIStyle style_GoButton = ResidentResources.GetStyleGoButton();
		Texture tex_Go = ResidentResources.GetTexGo();

		if(GUI.Button(OffsetRect.GetOffsetRect(sw * 0.5f - style_GoButton.normal.background.width/2,sh*0.8f,style_GoButton.normal.background.width,style_GoButton.normal.background.height),"", style_GoButton)){
			possible_Touch = false;
			CommonSE.PlayButtonSE();
			Application.LoadLevel("MainPiyoPiyo");
			//StartCoroutine("SoundPlay", 0);
		}
		GUI.DrawTexture(OffsetRect.GetOffsetRect(sw * 0.5f - tex_Go.width/2, sh * 0.815f, tex_Go.width, tex_Go.height), tex_Go);
	}
	
	public IEnumerator SoundPlay(int i){
		if(i==0)
			GameObject.FindWithTag("Item").SendMessage("Play",1);
		else
			GameObject.FindWithTag("Item").SendMessage("Play",2);
		yield return new WaitForSeconds(1.5f);
		GameObject.FindWithTag("Item").SendMessage("Stop");
		if(i == 0){
			Application.LoadLevel("MainPiyoPiyo");
		}else{
			Application.LoadLevel("Title");
		}
		
	}

	public void Reset(){
			PlayerPrefs.SetInt("Stage1", 1);
			PlayerPrefs.SetInt("Stage2", 0);
			PlayerPrefs.SetInt("Stage3", 0);
			PlayerPrefs.SetInt("Stage4", 0);
			PlayerPrefs.SetInt("Stage5", 0);
			PlayerPrefs.SetInt("Stage6", 0);
			PlayerPrefs.SetInt("Stage7", 0);
			PlayerPrefs.SetInt("Stage8", 0);
			PlayerPrefs.SetInt("Stage9", 0);
			PlayerPrefs.SetInt("Stage10", 0);
	}
	
	public void AllClear(){
			PlayerPrefs.SetInt("Stage1", 1);
			PlayerPrefs.SetInt("Stage2", 1);
			PlayerPrefs.SetInt("Stage3", 1);
			PlayerPrefs.SetInt("Stage4", 1);
			PlayerPrefs.SetInt("Stage5", 1);
			PlayerPrefs.SetInt("Stage6", 1);
			PlayerPrefs.SetInt("Stage7", 1);
			PlayerPrefs.SetInt("Stage8", 0);
			PlayerPrefs.SetInt("Stage9", 0);
			PlayerPrefs.SetInt("Stage10",0);
	}

	
	/*
	void DestroyBGM(){
			Destroy(GameObject.FindWithTag("Item"));
			Application.LoadLevel("Title");//タイトルへ
	}
	void DestroyBGMToGame(){
			Destroy(GameObject.FindWithTag("Item"));
			Application.LoadLevel("MainPiyoPiyo");//タイトルへ
	}
	*/
}
