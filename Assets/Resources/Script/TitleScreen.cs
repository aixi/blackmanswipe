﻿// #define DEBUG_GUI
#define WITH_FACEBOOK // Facebook共有ありなら定義

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DiceCreative.TsukubaLab.JTKNetwork;
[ExecuteInEditMode]
public class TitleScreen : MonoBehaviour {
#if DEBUG_GUI
	private const string VERSION = "4/7b";
#endif// DEBUG_GUI

	// -------
	public Camera gameCamera;
	// AppStore情報
	public static string APPSTORE_URL = "https://itunes.apple.com/jp/app/piyopiyosuwaipu/id632044441?mt=8";
	
	// -------

	// Twitter アプリ情報
	//
	// ■ぴよぴよスワイプ
	// コンシューマキー
	public static string TWITTER_CONSUMER_KEY = "YYoUfFXHyVvfcc78X00tLQ";
	// コンシューマシークレットキー
	public static string TWITTER_CONSUMER_SECRET = "Zv3ZSkLurvuXdjlOjWTkRKNWtRMZp0KqpyIhA5y4";

	// -------

	public Texture tex_Loading;//ローディング中のテクスチャ
	
	public Texture tex_Title_Back;//タイトル画面背景テクスチャ
	public Texture tex_Title_Back_NormalPiyo;//タイトル画面通常ぴよこ
	public Texture tex_Title_Back_KaidenPiyo;//タイトル画面免許皆伝ぴよこ
	public Texture tex_Title_Logo;//タイトルロゴテクスチャ
	
	public Texture tex_Help_Back;//説明画面背景テクスチャ
	public Texture[] texs_Help;//説明内容テクスチャの配列
	public Texture tex_Help_Logo;//説明内容テクスチャ（表示用）
	public Texture tex_Help_Logo_Back;
	public Texture tex_Help_mask;
	public Texture tex_Help_SceenShot;
	public Texture tex_Help_txt;
	public Texture tex_Help_BonusList;
	
	public Texture tex_Title_Ranking_Logo;//タイトル画面ランキング文字テクスチャ
	
	
	public GUIStyle guiStyle_BtPlay;//ゲームスタートボタン用GUIスタイル
	public GUIStyle guiStyle_BtHelp;//説明ボタン用GUIスタイル
	private bool fl_Play;//ゲームスタートボタンが押されたかどうかフラグ（以下同様）
	public Texture tex_Play;
	private bool fl_Help;
//	private bool fl_MyRanking;
//	public Texture tex_MyRanking;
//	private bool fl_WorldRanking;
	private bool fl_Friend;
	private bool fl_Review;
	private bool fl_Community;
	private bool fl_OtherGame;
	
	private string state = "normal";//タイトルの状態　normal→タイトル画面, help→説明画面, loading→ローディング画面
	
	public bool possible_Touch;//タッチ入力受付中か
	
	private float long_help = 3;//説明画面を表示するの長さ
	private int pointer_help_texs;//説明画面のポインター
	
	public float need_Distance_Swipe = 0.3f;
	private float distanceSwipe = 0;
	private bool fl_Start_Swipe = false;
	public GUIStyle gui_Help_Top;
	public GUIStyle gui_Help_Skip_Start;
	public GUIStyle gui_Help_Skip_Last;
	
	private int num_page = 6;
	public GUIStyle[] styles_Back_Bt;

	public GameObject obj_ClearCamera;
	
	private TexTxt texTxt;
	public Texture tex_piyo;
	
	private Texture[] texs_asobikata;
	private int[] pointer_txt_asobikata = new int[]{5,4,2,3,1,0};

	//
	public Texture tex_SNS;
	//public Texture tex_Facebook;
	public Texture tex_Wechat;	
	public Texture tex_Evaluation;

	public Texture tex_Ranking;
	public Texture tex_WorldRanking;
	public Texture tex_MyRanking;
	
	public Texture tex_Preference;
	public Texture tex_Credit;
	public Texture tex_SoundVolumeOff;
	public Texture tex_SoundVolumeOn;
	public Texture tex_CreditList;
	public Texture tex_OtherGames_Kyhov;
	public Texture tex_BackFromOtherGames;
	
	private bool fl_SNS;
//	private bool fl_Facebook;
	private bool fl_Wechat;	
	private bool fl_Evaluation;
	
	private bool fl_Ranking;
	private bool fl_WorldRanking;
	private bool fl_MyRanking;
	
	private bool fl_Preference;
	private bool fl_Credit;
	private bool fl_SoundVolume;
	private bool fl_OtherGames;

#if WITH_FACEBOOK // Facebookあり
	private Vector2	pos_SNS			= new Vector2(20,728);
	private Vector2 dist_SNS		= new Vector2(0,-91);

	private Vector2	pos_Ranking		= new Vector2(120,728);
	private Vector2 dist_Ranking	= new Vector2(0,-91);

	private Vector2	pos_Preference	= new Vector2(220,728);
	private Vector2 dist_Preference	= new Vector2(0,-91);

	private Vector2 pos_Facebook	= new Vector2(0,712);
	private Vector2 pos_Wechat		= new Vector2(0,712);
	private Vector2 pos_Evaluation	= new Vector2(0,712);

	private Vector2 pos_WorldRanking= new Vector2(100,712);
	private Vector2 pos_MyRanking	= new Vector2(100,712);

	private Vector2 pos_Credit		= new Vector2(200,712);
	private Vector2 pos_SoundVolume = new Vector2(200,712);

	private Vector2	pos_OtherGames	= new Vector2(450,735);
	private Vector2	pos_BackFromOtherGames	= new Vector2(542,762);
	//private Vector2	pos_BackFromOtherGames	= new Vector2(200,200);
#else // Facebookなし（4月リリース用）
	private Vector2	pos_SNS			= new Vector2(20,728);
	private Vector2 dist_SNS		= new Vector2(0,-91);

	private Vector2	pos_Ranking		= new Vector2(20,728);
	private Vector2 dist_Ranking	= new Vector2(0,-91);

	private Vector2	pos_Preference	= new Vector2(120,728);
	private Vector2 dist_Preference	= new Vector2(0,-91);

	private Vector2 pos_Facebook	= new Vector2(0,712);
	private Vector2 pos_Evaluation	= new Vector2(0,712);
	
	private Vector2 pos_WorldRanking= new Vector2(100,712);
	private Vector2 pos_MyRanking	= new Vector2(100,712);

	private Vector2 pos_Credit		= new Vector2(200,712);
	private Vector2 pos_SoundVolume = new Vector2(200,712);
	
	private Vector2 pos_Twitter		= new Vector2(345,728);
	private Vector2	pos_OtherGames	= new Vector2(450,735);
	private Vector2	pos_BackFromOtherGames	= new Vector2(542,762);
	//private Vector2	pos_BackFromOtherGames	= new Vector2(200,200);
#endif

	private string icon_path;
//	private string sns_message = "「ピヨピヨすわいぷ」\nやってピヨ(´・ω・`)";
	
	private float startTime_SNS;
	private bool  isPositiveExpandSNS;
	
	private float startTime_Ranking;
	private bool  isPositiveExpandRanking;
	
	private float startTime_Preference;
	private bool  isPositiveExpandPreference;
	
	private float timeLimit = 0.2f;

	// 他のゲームへ関連
	private string OtherGamesUrl ;
	WebViewObject webViewObject;
	

	// Use this for initialization
	void Start () {
		Debug.Log (Screen.width +"|"+ Screen.height);
		// Google analytic 
		if( GoogleAnalytics.instance){
			Debug.LogError("Tracking");
			GoogleAnalytics.instance.LogScreen("起動");
		}

		switch(Application.systemLanguage){
			case SystemLanguage.Japanese:
			break;
			default:
				APPSTORE_URL = APPSTORE_URL.Replace("/jp/","/us/");
			break;
		}

		//Init Wechat AppID
		#if UNITY_IPHONE && !UNITY_EDITOR
			IOSBridge.Init ();
		#endif

		// PlayerPrefs.DeleteAll(); // 設定リセットデバッグ debug

		//PlayerPrefs.SetInt("Initialize", 0);
		if(PlayerPrefs.GetInt("Initialize") != 1){
			state = "loading";
			pointer_help_texs = 0;
			//tex_Help_Logo = texs_Help[pointer_help_texs % texs_Help.Length];
			//Invoke("ChangeTitleState", long_help);
			PlayerPrefs.SetInt("Score1", 0);
			PlayerPrefs.SetInt("Score2", 0);
			PlayerPrefs.SetInt("Score3", 0);
			PlayerPrefs.SetInt("Score4", 0);
			PlayerPrefs.SetInt("Score5", 0);
			PlayerPrefs.SetInt("Score6", 0);
			PlayerPrefs.SetInt("Score7", 0);
			PlayerPrefs.SetInt("Score8", 0);
			PlayerPrefs.SetInt("Score9", 0);
			PlayerPrefs.SetInt("Score10", 0);
			//PlayerPrefs.SetFloat("Pos_y", 0);
			//PlayerPrefs.SetFloat("Timer", 0);
			//PlayerPrefs.SetInt("Check", 0);

			PlayerPrefs.SetInt("Stage1", 1);
			PlayerPrefs.SetInt("Stage2", 0);
			PlayerPrefs.SetInt("Stage3", 0);
			PlayerPrefs.SetInt("Stage4", 0);
			PlayerPrefs.SetInt("Stage5", 0);
			PlayerPrefs.SetInt("Stage6", 0);
			PlayerPrefs.SetInt("Stage7", 0);
			// PlayerPrefs.SetInt("Stage8", 0);
			// PlayerPrefs.SetInt("Stage9", 0);
			// PlayerPrefs.SetInt("Stage10", 0);	

			PlayerPrefs.SetInt("LastSelectedStage", 0);
			
			PlayerPrefs.SetInt("Initialize", 1);
			
			PlayerPrefs.SetInt("Total", 0);
		}
		
		if(GameData.volume_Sound == 0)
			AudioManager.SetMute(true);
		else
			AudioManager.SetMute(false);
		
		// 常駐リソースからセット
		texs_asobikata = ResidentResources.GetTexsAsobikata();
		//texTxt = ResidentResources.GetTexTxt();
		texTxt = new TexTxt();
		
		//DontDestroyOnLoad( GameObject.FindWithTag("Item"));
		
	//	ChangeTitleState();
	//	PlayerPrefs.SetInt("Initialize", 0);
	//	pointer_help_texs = 5;
	//	tex_Help_Logo = texs_Help[pointer_help_texs % texs_Help.Length];
		
		
		possible_Touch = true;
		
		need_Distance_Swipe *= FixedScreen.width;

		// 画面外クリア用カメラ作成
		GameObject clearCamera = GameObject.Find("/ClearCamera(Clone)");
		if (clearCamera == null) {
			Debug.Log("Instantiate ClearCamera.");
			GameObject.Instantiate(obj_ClearCamera);
		}
		else {
			Debug.Log("ClearCamera Exists.");
		}
		
		startTime_SNS = startTime_Ranking = startTime_Preference = -timeLimit;

		icon_path   = Application.dataPath + "/../Icon@2x.png";
	}
	
	void Update(){
		
		if(state == "help"){
			if(Input.GetButtonDown("Fire1") && possible_Touch && FixedScreen.height - Input.mousePosition.y < 0.8f * FixedScreen.height && FixedScreen.height - Input.mousePosition.y > 0.2f * FixedScreen.height){
				possible_Touch = false;
				distanceSwipe = Input.mousePosition.x;
				fl_Start_Swipe = true;
			}
			if(Input.GetButtonUp("Fire1") && fl_Start_Swipe){
				possible_Touch = true;
				fl_Start_Swipe = false;
				if(Input.mousePosition.x - distanceSwipe > need_Distance_Swipe){
					if(pointer_help_texs > 0){
						pointer_help_texs--;
						//tex_Help_Logo = texs_Help[pointer_help_texs % texs_Help.Length];
					}
				}
				if(Input.mousePosition.x - distanceSwipe < -need_Distance_Swipe){
					if(pointer_help_texs < num_page-1){
						pointer_help_texs++;
						//tex_Help_Logo = texs_Help[pointer_help_texs % texs_Help.Length];
					}
						
				}
			}
		}
		
		if(Input.GetButtonUp("Fire1")){
			possible_Touch = true;
		}

		if(state == "closing_webview") {
			if(webViewObject != null)
				webViewObject.SetVisibility(false); // 隠すだけ
			state = "normal";
			AdUnderlay.Instance.isView = true;
		}
	}
	
	
	void OnGUI () {

		//state = "othergames";
		int sw = FixedScreen.width;
		int sh = FixedScreen.height;
		if (state == "othergames") {
			// 他のゲームへWebView表示中
			// 戻るボタン

			if( GUI.Button( new Rect( Screen.width - 140,Screen.height - 120 , tex_BackFromOtherGames.width*1.5f, tex_BackFromOtherGames.height*1.5f), tex_BackFromOtherGames, GUIStyle.none ) ){
				if(webViewObject != null)
					webViewObject.SetVisibility(false); // 隠すだけ
				CommonSE.PlayButtonSE();
				state = "normal";
				gameCamera.backgroundColor = Color.black;
				AdUnderlay.Instance.isView = true;
			}
			// その他のものを描画しないで帰る
			return;
		}

		GUI.DrawTexture( OffsetRect.GetOffsetRect( 0, 0, sw, sh),tex_Title_Back);

		float rx;
		float ry;
		Texture tex_bg_piyo;

		// 免許皆伝前後でぴよこ画像変化
		if (0 == PlayerPrefs.GetInt("Stage"+(6+1).ToString())) { // 皆伝前
			rx = 0.20f;
			ry = 0.36f;
			tex_bg_piyo = tex_Title_Back_NormalPiyo;
		}
		else { // 皆伝後
			rx = 0.185f;
			ry = 0.555f;
			tex_bg_piyo = tex_Title_Back_KaidenPiyo;
		}
		GUI.DrawTexture( OffsetRect.GetOffsetRect( sw * rx, sh * ry, tex_bg_piyo.width, tex_bg_piyo.height),tex_bg_piyo);

		if(state == "normal"){
			if( GUI.Button( OffsetRect.GetOffsetRect( pos_OtherGames.x, pos_OtherGames.y, tex_OtherGames_Kyhov.width, tex_OtherGames_Kyhov.height), tex_OtherGames_Kyhov, GUIStyle.none ) ){
				ShowOtherGamesWebView();
				CommonSE.PlayButtonSE();
				state = "othergames";
			}
#if DEBUG_GUI
			// デバッグ用バージョン番号
			GUI.color = Color.black;
			GUI.Label(OffsetRect.GetOffsetRect(sw * 0.0f,sh * 0.0f, sw, sh), "Ver." + VERSION);
			GUI.color = Color.white;
#endif// DEBUG_GUI
		
						//GUI.DrawTexture( OffsetRect.GetOffsetRect( sw * 0.05f, sh * 0.05f, sw * 0.9f, sh * 0.4f),tex_Title_Logo);
			
			fl_Play = GUI.Button( OffsetRect.GetOffsetRect( sw * 0.48f, sh * 0.47f, styles_Back_Bt[0].normal.background.width, styles_Back_Bt[0].normal.background.height),"",styles_Back_Bt[0]);
			GUI.DrawTexture( OffsetRect.GetOffsetRect( sw * 0.93f - tex_Play.width, sh * 0.51f, tex_Play.width, tex_Play.height),tex_Play);
			
			
			
			//fl_Help = GUI.Button( OffsetRect.GetOffsetRect( sw  - guiStyle_BtHelp.normal.background.width * 1.1f, sh - guiStyle_BtHelp.normal.background.height * 2.0f, guiStyle_BtHelp.normal.background.width, guiStyle_BtHelp.normal.background.height),"",guiStyle_BtHelp);
			fl_Help = GUI.Button( OffsetRect.GetOffsetRect( sw  - guiStyle_BtHelp.normal.background.width * 1.1f, sh - guiStyle_BtHelp.normal.background.height * 1.45f, guiStyle_BtHelp.normal.background.width, guiStyle_BtHelp.normal.background.height),"",guiStyle_BtHelp);
			//fl_Help = GUI.Button( OffsetRect.GetOffsetRect( sw * 0.85f, sh - guiStyle_BtHelp.normal.background.height * 1.3f, guiStyle_BtHelp.normal.background.width, guiStyle_BtHelp.normal.background.height),"",guiStyle_BtHelp);
//			GUI.DrawTexture( OffsetRect.GetOffsetRect( sw * 0.34f, sh * 0.86f, sw * 0.32f, sh * 0.05f),tex_Help);
			
	//		fl_MyRanking = GUI.Button( OffsetRect.GetOffsetRect( sw * 0.48f, sh * 0.61f, styles_Back_Bt[0].normal.background.width, styles_Back_Bt[0].normal.background.height), "",styles_Back_Bt[0]);
	//		GUI.DrawTexture( OffsetRect.GetOffsetRect( sw * 0.89f - tex_Play.width, sh * 0.65f, tex_MyRanking.width , tex_MyRanking.height), tex_MyRanking);
			
//			fl_WorldRanking = GUI.Button( OffsetRect.GetOffsetRect( sw * 0.51f, sh * 0.65f, sw * 0.32f, sh * 0.05f), "",styles_Back_Bt[3]);
//			GUI.DrawTexture( OffsetRect.GetOffsetRect( sw * 0.51f, sh * 0.65f, sw * 0.32f, sh * 0.05f), tex_WorldRanking);
			
//			fl_Friend = GUI.Button( OffsetRect.GetOffsetRect( sw * 0.3f, sh * 0.72f, sw * 0.4f, sh * 0.05f), "",styles_Back_Bt[1]);
//			GUI.DrawTexture( OffsetRect.GetOffsetRect( sw * 0.3f, sh * 0.72f, sw * 0.4f, sh * 0.05f), tex_Friend);
			
//			fl_Review = GUI.Button( OffsetRect.GetOffsetRect( sw * 0.17f, sh * 0.79f, sw * 0.32f, sh * 0.05f), "",styles_Back_Bt[0]);
//			GUI.DrawTexture( OffsetRect.GetOffsetRect( sw * 0.17f, sh * 0.79f, sw * 0.32f, sh * 0.05f), tex_Review);
			
//			fl_Community = GUI.Button( OffsetRect.GetOffsetRect( sw * 0.51f, sh * 0.79f, sw * 0.32f, sh * 0.05f), "",styles_Back_Bt[0]);
//			GUI.DrawTexture( OffsetRect.GetOffsetRect( sw * 0.51f, sh * 0.79f, sw * 0.32f, sh * 0.05f), tex_Community);
			
//			fl_OtherGame = GUI.Button( OffsetRect.GetOffsetRect( sw * 0.55f, sh * 0.71f, sw * 0.6f, sw * 0.6f * 96 / 398), "",styles_Back_Bt[0]);
//			GUI.DrawTexture( OffsetRect.GetOffsetRect( sw * 0.66f, sh * 0.74f, sw * 0.25f, sw * 0.25f * tex_OtherGame.height / tex_OtherGame.width), tex_OtherGame);
			
			
			if(fl_Play && possible_Touch){
				possible_Touch = false;
				CommonSE.PlayButtonSE();
				//StartCoroutine("SoundPlay", 0);
				Application.LoadLevel("SelectStage");
			}else if(fl_Help && possible_Touch){
				possible_Touch = false;
				CommonSE.PlayButtonSE();
				//StartCoroutine("SoundPlay", 1);
				pointer_help_texs = 0;
				//tex_Help_Logo = texs_Help[pointer_help_texs % texs_Help.Length];
				state = "help";
				//Invoke("ChangeTitleState", long_help);
			}else if(fl_MyRanking && possible_Touch){
				possible_Touch = false;
				CommonSE.PlayButtonSE();
				Application.LoadLevel("Ranking");
				//StartCoroutine("SoundPlay", 2);
			}
				
				/*
			}else if(fl_WorldRanking && possible_Touch){
				//possible_Touch = false;
				
			}else if(fl_Friend && possible_Touch){
				//possible_Touch = false;
				
			}else if(fl_Review && possible_Touch){
				//possible_Touch = false;
				
			}else if(fl_Community && possible_Touch){
				//possible_Touch = false;
				
			}else if(fl_OtherGame && possible_Touch){
				//possible_Touch = false;
				
			}
			*/

			// 他のゲームへ
			// Kyhov Editor
			// Don't need Other Game



			
#if WITH_FACEBOOK
			if(fl_SNS || startTime_SNS>0){
		
				if( fl_Wechat = GUI.Button( OffsetRect.GetOffsetRect( pos_Wechat.x, 		pos_Wechat.y, 		tex_Wechat.width,		tex_Wechat.height),	tex_Wechat,	GUIStyle.none ) ){
					//Kyhov Editor
#if UNITY_IPHONE
					IOSBridge.SharedText_Moment("黑人碰到大麻烦啦！谁快来救救他!\nhttps://itunes.apple.com/cn/app/jue-wang!-yu-zhi-he-mei-qiu/id892050338");
#endif
				}
				// 2013/04/03 4月リリース時は封印 nishiura
				//fl_Evaluation	= GUI.Button( OffsetRect.GetOffsetRect( pos_Evaluation.x, 	pos_Evaluation.y,	tex_Evaluation.width,	tex_Evaluation.height), tex_Evaluation,	GUIStyle.none );
			}
			GUI.color = (pos_Facebook == pos_SNS) ? Color.white : Color.gray;
			if( GUI.Button( OffsetRect.GetOffsetRect( pos_SNS.x, pos_SNS.y, tex_SNS.width, tex_SNS.height), tex_SNS, GUIStyle.none ) ){
				if(startTime_SNS<0){
					startTime_SNS = Time.time;
					isPositiveExpandSNS = fl_SNS = !fl_SNS;
				}
			}
			GUI.color = Color.white;
			float time_SNS = Time.time-startTime_SNS;
			if(time_SNS<timeLimit){
				if(isPositiveExpandSNS){
					pos_Facebook	= 2*dist_SNS*(time_SNS/timeLimit) + pos_SNS;
					pos_Wechat	 	= 1*dist_SNS*(time_SNS/timeLimit) + pos_SNS;
					// pos_Evaluation	= dist_SNS*(time_SNS/timeLimit) + pos_SNS; // 2013/04/03 4月リリース時は封印 nishiura
				}else{
					float rest = timeLimit-time_SNS;
					pos_Facebook 	= 2*dist_SNS*(rest/timeLimit) + pos_SNS;
					pos_Wechat	 	= 1*dist_SNS*(rest/timeLimit) + pos_SNS;
					// pos_Evaluation	= dist_SNS*(rest/timeLimit) + pos_SNS; // 2013/04/03 4月リリース時は封印 nishiura

				}
			}else{
				if(isPositiveExpandSNS){
					pos_Facebook	= 2*dist_SNS + pos_SNS;
					pos_Wechat	 	= 1*dist_SNS + pos_SNS;
					// pos_Evaluation	= dist_SNS + pos_SNS; // 2013/04/03 4月リリース時は封印 nishiura
				}else{
					float rest = timeLimit-time_SNS;
					pos_Facebook 	= pos_SNS;
					pos_Wechat	 	= pos_SNS;
					// pos_Evaluation	= pos_SNS; // 2013/04/03 4月リリース時は封印 nishiura

				}
				startTime_SNS = -timeLimit;
			}
#else// WITH_FACEBOOK SNSの列の代わりにTwitterのみ
			if( fl_Twitter = GUI.Button( OffsetRect.GetOffsetRect( pos_Twitter.x, 		pos_Twitter.y, 		tex_Twitter.width,		tex_Twitter.height),	tex_Twitter,	GUIStyle.none ) ){
				TwitterBinding.init( TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET);
				TwitterBinding.showTweetComposer(
						sns_message,
						icon_path,
						APPSTORE_URL
						);

				Debug.Log("Kyhov Clicked on Wechat Button 2");
			}
#endif// WITH_FACEBOOK
			
			
			if(fl_Ranking || startTime_Ranking>0){
				fl_WorldRanking = GUI.Button( OffsetRect.GetOffsetRect( pos_WorldRanking.x, pos_WorldRanking.y, tex_WorldRanking.width,	tex_WorldRanking.height),	tex_WorldRanking,	GUIStyle.none );
				fl_MyRanking 	= GUI.Button( OffsetRect.GetOffsetRect( pos_MyRanking.x, 	pos_MyRanking.y,	tex_MyRanking.width,	tex_MyRanking.height),		tex_MyRanking,		GUIStyle.none );
			}
			if(fl_WorldRanking && possible_Touch){
				possible_Touch = false;
				CommonSE.PlayButtonSE();
				GameCenterConnector.ShowLeaderboardUI();
			}
			if(fl_MyRanking && possible_Touch){
				possible_Touch = false;
				CommonSE.PlayButtonSE();
				Application.LoadLevel("Ranking");
			}

			GUI.color = (pos_WorldRanking == pos_Ranking) ? Color.white : Color.gray;
			if( GUI.Button( OffsetRect.GetOffsetRect( pos_Ranking.x, pos_Ranking.y, tex_Ranking.width, tex_Ranking.height), tex_Ranking, GUIStyle.none ) ){
				if(startTime_Ranking<0){
					startTime_Ranking = Time.time;
					isPositiveExpandRanking = fl_Ranking = !fl_Ranking;
				}
			}
			GUI.color = Color.white;
			float time_Ranking = Time.time-startTime_Ranking;
			if(time_Ranking<timeLimit){
				if(isPositiveExpandRanking){
					pos_WorldRanking= 2*dist_Ranking*(time_Ranking/timeLimit) + pos_Ranking;
					pos_MyRanking	= dist_Ranking*(time_Ranking/timeLimit) + pos_Ranking;
				}else{
					float rest = timeLimit-time_Ranking;
					pos_WorldRanking= 2*dist_Ranking*(rest/timeLimit) + pos_Ranking;
					pos_MyRanking	= dist_Ranking*(rest/timeLimit) + pos_Ranking;

				}
			}else{
				if(isPositiveExpandRanking){
					pos_WorldRanking= 2*dist_Ranking + pos_Ranking;
					pos_MyRanking	= dist_Ranking + pos_Ranking;
				}else{
					float rest = timeLimit-time_Ranking;
					pos_WorldRanking= pos_Ranking;
					pos_MyRanking	= pos_Ranking;
				}
				startTime_Ranking = -timeLimit;
			}

			
			if(fl_Preference || startTime_Preference>0){
				fl_Credit 		= GUI.Button( OffsetRect.GetOffsetRect( pos_Credit.x,		pos_Credit.y, 		tex_Credit .width,		tex_Credit.height),			tex_Credit,			GUIStyle.none );
				Texture tex_SoundVolume;
				if(GameData.volume_Sound == 0)
					tex_SoundVolume = tex_SoundVolumeOff;
				else
					tex_SoundVolume = tex_SoundVolumeOn;
				fl_SoundVolume	= GUI.Button( OffsetRect.GetOffsetRect( pos_SoundVolume.x,	pos_SoundVolume.y,	tex_SoundVolume.width,	tex_SoundVolume.height),	tex_SoundVolume,	GUIStyle.none );
				if (fl_SoundVolume) {
					if(GameData.volume_Sound == 0) {
						GameData.volume_Sound = 1;
						AudioManager.SetMute(false);
					}
					else {
						GameData.volume_Sound = 0;
						AudioManager.SetMute(true);
					}
				}
			}
			GUI.color = (pos_Credit == pos_Preference) ? Color.white : Color.gray;
			if( GUI.Button( OffsetRect.GetOffsetRect( pos_Preference.x, pos_Preference.y, tex_Preference.width, tex_Preference.height), tex_Preference, GUIStyle.none) ){
				if(startTime_Preference<0){
					startTime_Preference = Time.time;
					isPositiveExpandPreference = fl_Preference	= !fl_Preference;;
				}
			}
			GUI.color = Color.white;
			float time_Preference = Time.time-startTime_Preference;
			if(time_Preference<timeLimit){
				if(isPositiveExpandPreference){
					pos_Credit		= 2*dist_Preference*(time_Preference/timeLimit) + pos_Preference;
					pos_SoundVolume	= dist_Preference*(time_Preference/timeLimit) + pos_Preference;
				}else{
					float rest = timeLimit-time_Preference;
					pos_Credit		= 2*dist_Preference*(rest/timeLimit) + pos_Preference;
					pos_SoundVolume	= dist_Preference*(rest/timeLimit) + pos_Preference;
				}
			}else{
				if(isPositiveExpandPreference){
					pos_Credit	= 2*dist_Preference + pos_Preference;
					pos_SoundVolume	= dist_Preference + pos_Preference;
				}else{
					float rest = timeLimit-time_Preference;
					pos_Credit	= pos_Preference;
					pos_SoundVolume	= pos_Preference;
				}
				startTime_Preference = -timeLimit;
			}

			if (fl_Credit && possible_Touch) {
				state = "credit";
			}
			
//			GUI.DrawTexture(OffsetRect.GetOffsetRect(sw*0.33f,sh*0.65f,sw*0.34f,sh*0.05f),tex_Title_Ranking_Logo);
			
		}else if(state == "help"){
			ResidentResources.DrawBGScroll();
			
			GUI.DrawTexture( OffsetRect.GetOffsetRect( sw * 0.15f, sh * 0.05f, tex_Help_Logo_Back.width, tex_Help_Logo_Back.height),tex_Help_Logo_Back);
			GUI.DrawTexture( OffsetRect.GetOffsetRect( sw * 0.465f - tex_Help_Logo.width / 2, sh * 0.075f, tex_Help_Logo.width,tex_Help_Logo.height),tex_Help_Logo);
			
			
			GUI.DrawTexture( OffsetRect.GetOffsetRect(sw*0.5f-tex_piyo.width/2, sh * 0.6f, tex_piyo.width, tex_piyo.height),tex_piyo);
			
			float txt_y;
			switch (pointer_help_texs) {
				case 0:
				case 1:
				case 2:
					txt_y = sh * 0.605f; // 2行用Y座標
					break;
				default:
					txt_y = sh * 0.605f; // 3行用Y座標
					break;
			}
			texTxt.Show_Txt_Help(OffsetRect.GetOffsetRect(sw * 0.1f, txt_y, sw * 0.8f, sh * 0.15f), pointer_txt_asobikata[pointer_help_texs]);
		
			//if(pointer_help_texs == 0)
				GUI.DrawTexture(OffsetRect.GetOffsetRect( sw*0.5f-texs_asobikata[pointer_help_texs].width/2,sh*0.2f, texs_asobikata[pointer_help_texs].width, texs_asobikata[pointer_help_texs].height),texs_asobikata[pointer_help_texs]);


			
			if(GUI.Button( OffsetRect.GetOffsetRect( sw * 0.9f - gui_Help_Top.normal.background.width / 2, sh * 0.05f, gui_Help_Top.normal.background.width, gui_Help_Top.normal.background.height), "", gui_Help_Top) && possible_Touch){
				Debug.Log("a");
				CommonSE.PlayButtonSE();
				ChangeTitleState();
				possible_Touch = false;
			}
			if(pointer_help_texs != 0){
				if(GUI.Button( OffsetRect.GetOffsetRect( sw * 0.05f, sh * 0.885f,gui_Help_Skip_Start.normal.background.width, gui_Help_Skip_Start.normal.background.height), "", gui_Help_Skip_Start) && possible_Touch){
					CommonSE.PlayButtonSE();
					pointer_help_texs--;
					//tex_Help_Logo = texs_Help[pointer_help_texs % texs_Help.Length];
					possible_Touch = false;
				}
			}
			if(pointer_help_texs != num_page-1){
				if(GUI.Button( OffsetRect.GetOffsetRect( sw * 0.8f, sh * 0.885f,gui_Help_Skip_Last.normal.background.width, gui_Help_Skip_Last.normal.background.height), "", gui_Help_Skip_Last) && possible_Touch){
					CommonSE.PlayButtonSE();
					pointer_help_texs++;
					//tex_Help_Logo = texs_Help[pointer_help_texs % texs_Help.Length];
					possible_Touch = false;
				}
			}
		}else if(state == "loading"){
			ResidentResources.DrawBGScroll();
			
			GUI.DrawTexture( OffsetRect.GetOffsetRect( sw * 0.5f - tex_Help_Logo_Back.width/2, sh * 0.05f, tex_Help_Logo_Back.width, tex_Help_Logo_Back.height),tex_Help_Logo_Back);
			GUI.DrawTexture( OffsetRect.GetOffsetRect( sw * 0.5f - tex_Help_Logo.width / 2, sh * 0.075f, tex_Help_Logo.width,tex_Help_Logo.height),tex_Help_Logo);
			
			
			
			GUI.DrawTexture( OffsetRect.GetOffsetRect(sw*0.5f-tex_piyo.width/2, sh * 0.6f, tex_piyo.width, tex_piyo.height),tex_piyo);
			
			texTxt.Show_Txt_Help(OffsetRect.GetOffsetRect(sw * 0.1f, sh * 0.625f, sw * 0.8f, sh * 0.15f), pointer_txt_asobikata[pointer_help_texs]);
		
			if(pointer_help_texs == 0)
				GUI.DrawTexture(OffsetRect.GetOffsetRect( sw*0.5f-texs_asobikata[0].width/2,sh*0.2f, texs_asobikata[0].width, texs_asobikata[0].height),texs_asobikata[0]);
			else
				GUI.DrawTexture(OffsetRect.GetOffsetRect( sw*0.5f-texs_asobikata[1].width/2,sh*0.2f, texs_asobikata[1].width, texs_asobikata[1].height),texs_asobikata[1]);
			if(pointer_help_texs != 0){
				
				if(GUI.Button( OffsetRect.GetOffsetRect( sw * 0.05f, sh * 0.885f,gui_Help_Skip_Start.normal.background.width, gui_Help_Skip_Start.normal.background.height), "", gui_Help_Skip_Start) && possible_Touch){
					CommonSE.PlayButtonSE();
					pointer_help_texs = 0;
					//tex_Help_Logo = texs_Help[pointer_help_texs % texs_Help.Length];
					possible_Touch = false;
				}
			}
			if(GUI.Button( OffsetRect.GetOffsetRect( sw * 0.8f, sh * 0.885f,gui_Help_Skip_Last.normal.background.width, gui_Help_Skip_Last.normal.background.height), "", gui_Help_Skip_Last) && possible_Touch){
				if(pointer_help_texs != 1){
					CommonSE.PlayButtonSE();
					pointer_help_texs = 1;
					//tex_Help_Logo = texs_Help[pointer_help_texs % texs_Help.Length];
					possible_Touch = false;
				}else{
					ChangeTitleState();
					possible_Touch = false;
				}
			}
		}else if(state == "credit"){
			fl_Credit = GUI.Button( OffsetRect.GetOffsetRect( 0, 0, tex_CreditList .width, tex_CreditList.height), tex_CreditList, GUIStyle.none );
			if (fl_Credit && possible_Touch) {
				ChangeTitleState();
			}
		}
	}
	
	public void ChangeTitleState(){
		state = "normal";
		possible_Touch = true;
		//GameObject.FindWithTag("Item").SendMessage("PlayBGM");
	}

	// WebViewでGプロの他のゲームを紹介
	public void ShowOtherGamesWebView(){
		AdUnderlay.Instance.isView = false;
		if (webViewObject != null) {
			webViewObject = null;
		}
		gameCamera.backgroundColor = Color.white;
		CheckDeviceAndLanguage ();
		switch(Application.platform){
		#if UNITY_IPHONE
			case RuntimePlatform.IPhonePlayer:
				// WebViewObjectがまだ生成されていない場合、ネットワーク接続できるならば、WebViewObjectを生成
			break;	
		#endif
		}
		if (webViewObject == null && Application.internetReachability != NetworkReachability.NotReachable) {
			Debug.LogError("Load default");
			webViewObject = (new GameObject("WebViewObject")).AddComponent<WebViewObject>();
			webViewObject.Init( (msg)=>{
				Debug.LogError("msg:"+msg);
				if (msg == "close") {
					state = "closing_webview"; // webViewObject.SetVisibility(false);
				}
			} );
			
			webViewObject.LoadURL(OtherGamesUrl);
			
			webViewObject.EvaluateJS(
				"window.addEventListener('load', function() {" +
				"	window.Unity = {" +
				"		call:function(msg) {" +
				"			var iframe = document.createElement('IFRAME');" +
				"			iframe.setAttribute('src', 'unity:' + msg);" +
				"			document.documentElement.appendChild(iframe);" +
				"			iframe.parentNode.removeChild(iframe);" +
				"			iframe = null;" +
				"		}" +
				"	}" +
				"}, false);"
				);
			
			webViewObject.EvaluateJS(
				"window.addEventListener('load', function() {" +
				"	window.addEventListener('click', function() {" +
				"		Unity.call('clicked');" +
				"	}, false);" +
				"}, false);"
				);
			
			    webViewObject.SetMargins(0, 0, 0, 100);
		}
		else{
			webViewObject = null;
		}
		
		if (webViewObject != null) // WebViewObjectが生成できている場合、表示
				webViewObject.SetVisibility (true);

	}
	public void CheckDeviceAndLanguage(){
		#if UNITY_ANDROID 
		if(Application.systemLanguage == SystemLanguage.Japanese){
			OtherGamesUrl = "http://ishi-international.jp/applist/android_ja.html";
		}
		else{
			OtherGamesUrl = "http://ishi-international.jp/applist/android.html";
		}
		#elif UNITY_IPHONE 
		if(Application.systemLanguage == SystemLanguage.Japanese){
			OtherGamesUrl = "http://ishi-international.jp/applist/ios_ja.html";
		}
		else{
			OtherGamesUrl = "http://ishi-international.jp/applist/ios.html";
		}
		#endif
	}
}
