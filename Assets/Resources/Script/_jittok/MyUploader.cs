using UnityEngine;
using System.Collections;

using DiceCreative.TsukubaLab.JTKNetwork;

[RequireComponent(typeof(WWWUploader))]
public class MyUploader : MonoBehaviour {
	// private static MyUploader instance;
	// public  static MyUploader Current{ get{ return instance; } }
	// void Awake(){	instance = this;	}
	
	private  WWWUploader uploader;
	public   WWWUploader Uploader{ get{ if(uploader==null) uploader = GetComponent<WWWUploader>(); return uploader; } }
	

	void OnDidBecomeActive(string message){
		if(PlayerPrefs.GetInt("FirstUpload",0) != 1){
			Debug.Log("startfirst");
			Uploader.Request( new GProNetworkPacket(ResourceManager.General.Application.GetAppMessage(GProNetworkPacket.AppActionDetailIDs.GameStartFirst)).ToWWWForm() ) ;
			Uploader.OnCompletedAction += FirstUploadEndAction;
		}
		Debug.Log("startnormal");
		Uploader.Request( new GProNetworkPacket(ResourceManager.General.Application.GetAppMessage(GProNetworkPacket.AppActionDetailIDs.GameStart)).ToWWWForm() ) ;
	}
	void FirstUploadEndAction(string url, string content){
		if(content == "OK"){
			Debug.Log("FirstUpload");
			PlayerPrefs.SetInt("FirstUpload",1);
		}else
			Uploader.CancelAll();
		Uploader.OnCompletedAction -= FirstUploadEndAction;
	}
	

	void OnWillResignActive(string message){
		Debug.Log("cancel");
		Uploader.CancelAll();
	}
}
