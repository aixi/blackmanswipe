using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

using DiceCreative.TsukubaLab.JTKNetwork;

public class GProNetworkPacket : UnityPacket{	// Gプロで使用するサーバ送信用の情報を管理（厳密に言うと本来のパケット情報の一部のみ扱う）
	public enum AppActionTypeIDs{	// action_typeフィールドの値についてのID番号の列挙体
		Charge = 0,
		Point,
		Item,
		Start,
		Event,
		Level,
		Stage
	}
	
	public enum AppActionDetailIDs{	// action_detailフィールドの値についてのID番号の列挙体
		None = -1,
		ChargeRequest,
		ChargeResponse,
		ChargeRepayRequest,
		ChargeRepayResponse,
		ChargeError,
		PointPut,
		PointUse,
		PointRepay,
		PointError,
		ItemPut,
		ItemUse,
		ItemRepay,
		ItemError,
		GameStartFirst,
		GameStart,
		GameEnd,
		EventJoin,
		LevelUp,
		StageIn,
		StageOut,
		StageClear,
		HighScore
	}

	[Serializable]
	public struct AppActionDetail{	// action_detail と action_type の組について
		public readonly static string[] AppActionTypeNames = new string[]{	// action_typeフィールドの値
			"charge",
			"point",
			"item",
			"start",
			"event",
			"level",
			"stage"
		};

		public AppActionDetailIDs ID		{ get;  private set; }	// action_detailのID
		public string			  Name		{ get;  private  set; }	// action_detailの値
		public AppActionTypeIDs   TypeID	{ get;  private  set; }	// action_typeのID
		public string			  TypeName	{ get;  private  set; }	// action_typeの値

		public AppActionDetail(AppActionDetailIDs id, AppActionTypeIDs typeid, string name){
			ID 		 = id;
			Name	 = name;
			TypeID	 = typeid;
			TypeName = AppActionTypeNames[(int)TypeID];
		}
	}
	public readonly static ReadOnlyCollection<AppActionDetail> AppActionDetails = Array.AsReadOnly(
		new AppActionDetail[]{	// AppActionDetailの一覧。これらオブジェクトのうち１つを使ってパケット生成を行う
			new AppActionDetail( AppActionDetailIDs.ChargeRequest,			AppActionTypeIDs.Charge,	"chargereq"),
			new AppActionDetail( AppActionDetailIDs.ChargeResponse,			AppActionTypeIDs.Charge,	"chargeres"),
			new AppActionDetail( AppActionDetailIDs.ChargeRepayRequest,		AppActionTypeIDs.Charge,	"chargerepayreq"),
			new AppActionDetail( AppActionDetailIDs.ChargeRepayResponse,	AppActionTypeIDs.Charge,	"chargerepayres"),
			new AppActionDetail( AppActionDetailIDs.ChargeError,			AppActionTypeIDs.Charge,	"chargeerror"),
			new AppActionDetail( AppActionDetailIDs.PointPut,				AppActionTypeIDs.Point,		"pointput"),
			new AppActionDetail( AppActionDetailIDs.PointUse,				AppActionTypeIDs.Point,		"pointuse"),
			new AppActionDetail( AppActionDetailIDs.PointRepay,				AppActionTypeIDs.Point,		"pointrepay"),
			new AppActionDetail( AppActionDetailIDs.PointError,				AppActionTypeIDs.Point,		"pointerror"),
			new AppActionDetail( AppActionDetailIDs.ItemPut,				AppActionTypeIDs.Item,		"itemput"),
			new AppActionDetail( AppActionDetailIDs.ItemUse,				AppActionTypeIDs.Item,		"itemuse"),
			new AppActionDetail( AppActionDetailIDs.ItemRepay,				AppActionTypeIDs.Item,		"itemrepay"),
			new AppActionDetail( AppActionDetailIDs.ItemError,				AppActionTypeIDs.Item,		"itemerror"),
			new AppActionDetail( AppActionDetailIDs.GameStartFirst,			AppActionTypeIDs.Start,		"startfirst"),
			new AppActionDetail( AppActionDetailIDs.GameStart,				AppActionTypeIDs.Start,		"startnormal"),
			new AppActionDetail( AppActionDetailIDs.GameEnd,				AppActionTypeIDs.Start,		"appclose"),
			new AppActionDetail( AppActionDetailIDs.EventJoin,				AppActionTypeIDs.Event,		"eventjoin"),
			new AppActionDetail( AppActionDetailIDs.LevelUp,				AppActionTypeIDs.Level,		"levelup"),
			new AppActionDetail( AppActionDetailIDs.StageIn,				AppActionTypeIDs.Stage,		"stagein"),
			new AppActionDetail( AppActionDetailIDs.StageOut,				AppActionTypeIDs.Stage,		"stageout"),
			new AppActionDetail( AppActionDetailIDs.StageClear,				AppActionTypeIDs.Stage,		"stageclear"),
			new AppActionDetail( AppActionDetailIDs.HighScore,				AppActionTypeIDs.Stage,		"highscore")
		}
	);
	
	// アプリ種別（静的な変数はGProNetworkPacketConnectorから取得）
	public static string VenderIdentifier{	get{ return GProNetworkPacketConnector.VenderIdentifier; }	}	// ○ vender		ベンダーを示すID(64文字まで)
	public static string AppIdentifier{		get{ return GProNetworkPacketConnector.AppIdentifier; }	}		// ○ app		アプリを示すID(64文字まで)
	public static string AppVersion{		get{ return GProNetworkPacketConnector.AppVersion; }	}		// appver		アプリのバージョン(64文字まで)

	// 環境・状態系
	public long   Time { get; private set; }	// ○ time		local日時 "yyyy-mm-dd hh:mm:ss"

	public static string TrackingIdentifier{	get{ return GProNetworkPacketConnector.TrackingIdentifier; }	}	// ○ koa_tracking_id	ユーザー(端末)を識別する一意の値
	public static string GUID{					get{ return GProNetworkPacketConnector.GUID; }	}					// ○ guid				ゲーム内におけるユーザーを識別する一意の値(64文字まで)
	public static string PlatfromOSIdentifier{	get{ return GProNetworkPacketConnector.PlatfromOSIdentifier; }	}			// ○ os					OS種別(64文字まで)
	public static string PlatfromOSVersion{		get{ return GProNetworkPacketConnector.PlatfromOSVersion;	}	}				// ○ osver				OSのバージョン情報(64文字まで)
	public static string DeviceIdentifier{		get{ return GProNetworkPacketConnector.DeviceIdentifier;	}	}		// device				デバイス情報(256文字まで)
	public static string CountryIdentifier{		get{ return GProNetworkPacketConnector.CountryIdentifier;	}	}	// country				アプリが起動しているOSの言語、国・地域コード	
	public static string TelephoneNumber{		get{ return GProNetworkPacketConnector.TelephoneNumber;	}	}	// tel					電話番号(11文字まで)
	public static int    CurrentPoint{			get{ return GProNetworkPacketConnector.CurrentPoint;	}	}		// restpoint				ユーザーの残りポイント(数字を入力)	
	
	
	// （動的な変数はAppMessagesから取得） 
	[Serializable]
	public struct AppMessages{	//  動的な情報初期化のための構造体
		public AppActionDetail ActionDetail;	// ○ acton_detail  アクション種別詳細(64文字まで) // ○ acton_type    アクション種別(64文字まで)

		public int    Level;					// ○ level		ユーザーのレベル(数字を入力)
		public string StageIdentifier;			// stage		ステージ情報(64文字まで)
		public string ItemIdentifier;			// △item		アイテムを示すID(64文字まで)	
		public string EventIdentifier;			// △event		イベントを示すＩＤ(64文字まで)	
	
		public int ChargePrice;					// △ price		課金金額(数字を入力)
		public int PointPrice;					// △ point		ゲーム内通貨等のポイント(数字を入力)

		public string OptionMessage1;			// opt01		アクションによって異なるログの付加情報(64文字まで)
		public string OptionMessage2;			// opt02		アクションによって異なるログの付加情報(64文字まで)
		public string OptionMessage3;			// opt03		アクションによって異なるログの付加情報(64文字まで)
		
		public string ErrorMessage;				// code			エラー等発生時のエラーコード(64文字まで)

		// コンストラクタ（必須情報順に細分化）
		public AppMessages(AppActionDetailIDs actionID, int level)
			: this( actionID, level, 0, 0 ){}
		public AppMessages(
			AppActionDetailIDs actionID, int level, 
			int charge, int point
		)
			: this( actionID, level, charge, point, "default" ){}
		public AppMessages(
			AppActionDetailIDs actionID, int level, 
			int charge, int point,
			string itemIdentifier 
		)
			: this( actionID, level, charge, point, itemIdentifier, "default" ){}
		public AppMessages(
			AppActionDetailIDs actionID, int level, 
			int charge, int point,
			string itemIdentifier, 
			string stageID
		)
			: this( actionID, level, charge, point, itemIdentifier, stageID, "default", "default", "default" ){}
		public AppMessages(
			AppActionDetailIDs actionID, int level, 
			int charge, int point,
			string itemIdentifier, 
			string stageID,
			string optionMessage1, string optionMessage2, string optionMessage3
		) 
			: this( actionID, level, charge, point, itemIdentifier, stageID, optionMessage1, optionMessage2, optionMessage3, "default"){}
		public AppMessages(
			AppActionDetailIDs actionID, int level, 
			int charge, int point,
			string itemIdentifier, 
			string stageID,
			string optionMessage1, string optionMessage2, string optionMessage3,
			string eventIdentifier
		) 
			: this( actionID, level, charge, point, itemIdentifier, stageID, optionMessage1, optionMessage2, optionMessage3, eventIdentifier, "default"){}

		public AppMessages(
			AppActionDetailIDs actionID, int level, 
			int charge, int point,
			string itemIdentifier,
			string stageID,
			string optionMessage1, string optionMessage2, string optionMessage3,
			string eventIdentifier, 
			string errorMessage
		){
			ActionDetail	= AppActionDetails[(int)actionID];
			Level			= level;
			
			ChargePrice		= charge;
			PointPrice		= point;

			ItemIdentifier	= itemIdentifier;
			
			EventIdentifier = eventIdentifier;
			
			OptionMessage1	= optionMessage1;
			OptionMessage2	= optionMessage2;
			OptionMessage3	= optionMessage3;
			
			StageIdentifier = stageID;

			ErrorMessage	= errorMessage;
		}
	}

	private AppMessages AppMessage;	//↑のインスタンス
	
	// アクション系 （動的な変数はAppMessagesから取得） 
	public AppActionDetail ActionDetail		{ get{ return AppMessage.ActionDetail; } }
	
	public int    Level						{ get{ return AppMessage.Level; } }
	public string StageIdentifier			{ get{ return AppMessage.StageIdentifier; } }
	public string ItemIdentifier			{ get{ return AppMessage.ItemIdentifier; } }
	public string EventIdentifier			{ get{ return AppMessage.EventIdentifier; } }
	
	public int ChargePrice					{ get{ return AppMessage.ChargePrice; } }
	public int PointPrice					{ get{ return AppMessage.PointPrice; } }
	
	public string OptionMessage1			{ get{ return AppMessage.OptionMessage1; } }
	public string OptionMessage2			{ get{ return AppMessage.OptionMessage2; } }
	public string OptionMessage3			{ get{ return AppMessage.OptionMessage3; } }
		
	public string ErrorMessage				{ get{ return AppMessage.ErrorMessage; } }
	
	
	private const int FieldIDBase = 17;
	private Dictionary<string, Func<string, UnityPacket.Field>> fieldFactoryDictionary = null;	// 送信に使用するフィールド辞書作成のための、このインスタンスから各フィールドの生成方法が書かれた辞書（バッファ）

	// コンストラクタ
	public GProNetworkPacket(AppMessages appMessage){
		AppMessage = appMessage;
	}

	public override WWWForm ToWWWForm(){	// WWFormに変換
		base.FieldDictionary = GetFieldDictionary();
		return base.ToWWWForm();
	}
	private Dictionary<string,Field> GetFieldDictionary(){	// フィールド辞書を取得
		Time = DateTime.Now.Ticks;	// タイムスタンプの取得

		Dictionary<string, UnityPacket.Field> fieldDictionary = new Dictionary<string, UnityPacket.Field>();		// フィールド辞書の初期化
		Dictionary<string, Func<string,UnityPacket.Field>> FieldFactoryDictionary = GetFieldFactoryDictionary();	// フィールド生成辞書の初期化
		
		foreach(string key in FieldFactoryDictionary.Keys)	// フィールド生成辞書にかかれた全てのフィールド名について
			fieldDictionary[key] = FieldFactoryDictionary[key](key);	// フィールド（とその値）を生成し代入

		return fieldDictionary;	// 完成したフィールド辞書を返す
	}
	private Dictionary<string, Func<string,UnityPacket.Field>> GetFieldFactoryDictionary(){	// フィールド生成辞書を取得
		if(fieldFactoryDictionary==null){	// まだ未生成であれば生成
			int id_seed = FieldIDBase;

			fieldFactoryDictionary = new Dictionary<string, Func<string,UnityPacket.Field>>{
				{ "time",			 (name)=>{ return new UnityPacket.Field<string>(id_seed,	name,	GetStringHead( new DateTime(Time).ToString("yyyy-MM-dd HH:mm:ss"), 64) );  } },
				{ "action_type",	 (name)=>{ return new UnityPacket.Field<string>(id_seed+1,  name,	GetStringHead( ActionDetail.TypeName, 64) );  } },
				{ "action_detail",	 (name)=>{ return new UnityPacket.Field<string>(id_seed+2,  name,	GetStringHead( ActionDetail.Name, 64) );  } },
				{ "vender",			 (name)=>{ return new UnityPacket.Field<string>(id_seed+3,  name,	GetStringHead( VenderIdentifier, 64) );  } },
				{ "app"	,			 (name)=>{ return new UnityPacket.Field<string>(id_seed+4,  name,	GetStringHead( AppIdentifier, 64) );  } },
				{ "appver",			 (name)=>{ return new UnityPacket.Field<string>(id_seed+5,  name,	GetStringHead( AppVersion, 64) );  } },
				{ "os",				 (name)=>{ return new UnityPacket.Field<string>(id_seed+6,  name,	GetStringHead( PlatfromOSIdentifier, 64) );  } },
				{ "osver",			 (name)=>{ return new UnityPacket.Field<string>(id_seed+7,  name,	GetStringHead( PlatfromOSVersion, 64) );  } },
				{ "koa_tracking_id", (name)=>{ return new UnityPacket.Field<string>(id_seed+8,  name,	GetStringHead( TrackingIdentifier, 64) );  } },
				{ "guid",			 (name)=>{ return new UnityPacket.Field<string>(id_seed+9,  name,	GetStringHead( GUID, 64) );  } },
				{ "device",			 (name)=>{ return new UnityPacket.Field<string>(id_seed+10, name,	GetStringHead( DeviceIdentifier, 64) );  } },
				{ "level",			 (name)=>{ return new UnityPacket.Field<int>(id_seed+11,	name,	Level ); } },
				{ "stage",			 (name)=>{ return new UnityPacket.Field<string>(id_seed+12, name,	GetStringHead( StageIdentifier, 64) );  } },
				{ "item",			 (name)=>{ return new UnityPacket.Field<string>(id_seed+13, name,	GetStringHead( ItemIdentifier, 64) );  } },
				{ "event",			 (name)=>{ return new UnityPacket.Field<string>(id_seed+14, name,	GetStringHead( EventIdentifier, 64) );  } },
				{ "restpoint",		 (name)=>{ return new UnityPacket.Field<int>(id_seed+15, 	name,	CurrentPoint ); } },
				{ "code",			 (name)=>{ return new UnityPacket.Field<string>(id_seed+16, name,	GetStringHead( ErrorMessage, 64) );  } },
				{ "country",		 (name)=>{ return new UnityPacket.Field<string>(id_seed+17, name,	GetStringHead( CountryIdentifier, 64) );  } },
				{ "opt01",			 (name)=>{ return new UnityPacket.Field<string>(id_seed+18, name,	GetStringHead( OptionMessage1, 64) );  } },
				{ "opt02",			 (name)=>{ return new UnityPacket.Field<string>(id_seed+19, name,	GetStringHead( OptionMessage2, 64) );  } },
				{ "opt03",			 (name)=>{ return new UnityPacket.Field<string>(id_seed+20, name,	GetStringHead( OptionMessage3, 64) );  } },
				{ "tel",			 (name)=>{ return new UnityPacket.Field<string>(id_seed+21, name,	GetStringHead( TelephoneNumber, 11) );  } },
				{ "price",			 (name)=>{ return new UnityPacket.Field<int>(id_seed+22,	name,	ChargePrice); } },
				{ "point",			 (name)=>{ return new UnityPacket.Field<int>(id_seed+23,	name,	PointPrice ); } },
			};
		}
		return fieldFactoryDictionary;
		/*
		○ time	日時
		○ action_type	アクション種別(64文字まで)
		○ action_detail	アクション種別詳細(64文字まで)
		○ vender	ベンダーを示すID(64文字まで)
		○ app	アプリを示すID(64文字まで)
		appver	アプリのバージョン(64文字まで)
		○ os	OS種別(64文字まで)
		osver	OSのバージョン情報(64文字まで)
		○ koa_tracking_id	ユーザー(端末)を識別する一意の値
		○ guid	ゲーム内におけるユーザーを識別する一意の値(64文字まで)
		device	デバイス情報(256文字まで)
		level	ユーザーのレベル(数字を入力)
		stage	ステージ情報(64文字まで)
		△ item	アイテムを示すID(64文字まで)
		△ event	イベントを示すＩＤ(64文字まで
		restpoint	ユーザーの残りポイント	(数字を入力)
		code	エラー等発生時のエラーコード(64文字まで)
		country	アプリが起動しているOSの言語、国・地域コード		
		opt01	アクションによって異なるログの付加情報(64文字まで)
		opt02	アクションによって異なるログの付加情報(64文字まで)
		opt03	アクションによって異なるログの付加情報(64文字まで)
		tel	電話番号(11文字まで)
		△ price	課金金額(数字を入力)
		△ point	ゲーム内通貨等のポイント(数字を入力)
		*/
	}
	private string GetStringHead(string str, int count){	// strについて、先頭から長さcountまでの文字列を取得する(string.Substringのエラー排除版)
		if(str!=null){
			if(count<0)
				return "";
			else if(count<=str.Length)
				return str.Substring(0,count);
			else
				return str;
		}else
			return "";
	}
	
	public override string ToString(){	// デバッグのための文字列化
		base.FieldDictionary = GetFieldDictionary();
		return base.ToString();
	}
}