using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;

public class GProNetworkPacketConnector{	// GProNetworkPacketに必要な、System外で定義する外部参照。主にstaticなもの 
	// アプリ種別
	public static string VenderIdentifier{	get{ return ResourceManager.General.Vender.Identifier; }	}		// ○ vender		ベンダーを示すID(64文字まで)
	public static string AppIdentifier{		get{ return ResourceManager.General.Application.Identifier; }	}	// ○ app		アプリを示すID(64文字まで)
	public static string AppVersion{		get{ return ResourceManager.General.Application.Version; }	}		// appver		アプリのバージョン(64文字まで)

	// 環境・状態系
	public static string TrackingIdentifier{	get{ return ResourceManager.General.Device.TrackingIdentifier; }	}	// ○ koa_tracking_id	ユーザー(端末)を識別する一意の値
	public static string GUID{					get{ return ResourceManager.General.Device.GUID; }	}					// ○ guid				ゲーム内におけるユーザーを識別する一意の値(64文字まで)
	public static string PlatfromOSIdentifier{	get{ return ResourceManager.General.OS.Identifier; }	}				// ○ os					OS種別(64文字まで)
	public static string PlatfromOSVersion{		get{ return ResourceManager.General.OS.Version;	}	}					// ○ osver				OSのバージョン情報(64文字まで)
	public static string DeviceIdentifier{		get{ return ResourceManager.General.Device.Identifier;	}	}			// device				デバイス情報(256文字まで)
	public static string CountryIdentifier{		get{ return ResourceManager.General.OS.CountryIdentifier;	}	}		// country				アプリが起動しているOSの言語、国・地域コード	
	public static string TelephoneNumber{		get{ return "00000000000";	}	}	// tel					電話番号(11文字まで)
	public static int    CurrentPoint{			get{ return 0;	}	}				// restpoint				ユーザーの残りポイント(数字を入力)	
}