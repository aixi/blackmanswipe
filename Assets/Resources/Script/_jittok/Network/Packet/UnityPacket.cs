using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;

// using DiceCreative.TsukubaLab.JTKGeneral;

namespace DiceCreative.TsukubaLab.JTKNetwork{

	public class UnityPacket{	// Unity(C#)での送信パケット（厳密にはペイロードのみ）を定義		
		public class Field{	// HTTPなどで使用されるフィールド情報を管理。総称体への継承用で、値定義なし
			public int ID			{ get; protected set; }	// ID
			public string Name		{ get; protected set; }	// 内容に対する名前。フィールド名、ファイル名、画像名など

			// コンストラクタ
			public Field(int id, string name){
				ID		= id;
				Name	= name;
			}

			// ハッシュコードを得る
			// public override int GetHashCode(){	return ID;	}	// IDでいい

			public override string ToString(){		// デバッグのための文字列化
				return "{ ID = "+ ID +",\tValue = None }:" + Name;
			}
		}
		public class Field<T> : Field{	// Fieldの総称体。フィールドの内容を定義。パケット化できるのはintとstringのみ
			public T Content		{ get; private set; }	// フィールドの内容
		
			// コンストラクタ
			public Field(int id, string name, T content) : base(id,name){
				Content	= content;
			}
		
			public override string ToString(){	// デバッグのための文字列化
				return "{ ID = " + ID + ",\tValue = "+ Content.ToString() +" }:" + Name;
			}
		}
		public class FieldBytes : Field<byte[]>{	// バイナリを扱えるようにしたField。付帯情報が必要になる
			public string MIME			{ get; private set; }	// MIMEと呼ばれる、型情報を表す文字列
		
			// コンストラクタ
			public FieldBytes(int id, string name, byte[] bytes, string mime) : base(id,name,bytes){
				MIME = mime;
			}
		
			public override string ToString(){	// デバッグのための文字列化
				return "{ ID = " + ID + ",\tbytes=" + Content.Length + "b ,\tMIME=" + MIME + " }:" + Name;
			}
		}
		
		
		public Dictionary<string,Field> FieldDictionary	{ get; protected set; }	// フィールド辞書<Field名,Fieldインスタンス>
	
		// コンストラクタ
		public UnityPacket(){
			FieldDictionary = new Dictionary<string,Field>();
		}
	

		public virtual WWWForm ToWWWForm(){	// UnityPacketを送信パケットを表すWWWFormへの変換
			WWWForm wwwForm = new WWWForm();
		
			foreach(string key in FieldDictionary.Keys){	// FieldDictionaryのキーである全てのフィールド名keyについて、WWWFormの定義に従い、フィールドインスタンスと共にパケット化（intとstring以外は調整中）
				Field field = FieldDictionary[key];
				if(field is Field<int>)
					wwwForm.AddField( key, ((Field<int>)field).Content );
				else if(field is Field<string>)
 					wwwForm.AddField( key, ((Field<string>)field).Content );
				else if(field is FieldBytes){
					FieldBytes bin = ((FieldBytes)field);
					wwwForm.AddBinaryData( key, bin.Content, bin.Name, bin.MIME );
				}
			}

			/*
				Debug.Log(
					"WWWUploader.canUpload = " +WWWUploader.canUpload.ToString() + "\n"
				  + "This UnityPacket = [\n" + this.ToString() + "\n]"
				);
			*/
			return wwwForm;	
		}

		public override string ToString(){	// デバッグのための文字列化
			string str = "";
			foreach(Field field in FieldDictionary.Values)
				str +=  field.ToString() + "\n";
			return str.TrimEnd();
		}
	}

}