using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace DiceCreative.TsukubaLab.JTKNetwork{

	public class WWWManager<REQUEST,CONTENT> : MonoBehaviour{	// WWWクラス経由でダウンロードを実行する
		/* 	// 継承する時は、次のメソッドをオーバーライドすること
		public virtual string  GetURLFrom(REQUEST request);          //  型REQUESTを使ったURLの取り出し方
		public virtual WWW     GetWWWFrom(REQUEST request);	// 型REQUESTを使ったWWWFormの呼び出し方
		public virtual CONTENT    GetContentFrom(WWW www);	// WWWクラスインスタンスからの目的のデータの取り出し方
		public virtual void DestroyContent(CONTENT content);	// ダウンロードしたデータの破棄の仕方
		*/
		// フィールド
		public float MaxUploadWaitTime			= 6f;			// １URLに対するアップロードの最長待ち時間。この時間を超えると失敗とみなす
		public float MinUploadRetryPeriod		= 3f;			// １アップロードあたりの最短間隔
		public float MaxDownloadWaitTime		= 12f;		// １ダウンロードあたりの最長待ち時間。この時間を超えると失敗とみなす
		public float MaxConfirmationPeriod		= 1/31f;	// 最長確認間隔

		public int	WWWSlotMax	 			= 1;		// １度にアップロード・ダウンロードできる数。ロースペック機（iPhone3あたり）や低速ネットワーク対象なら１推奨。対象スペックによっては増やすのも可
		protected	List<WWW>	WWWSlots = new List<WWW>();		// ダウンロードスロット（List）。 実際にダウンロードが進行している
		private int	WWWSlotCount { get{ return (WWWSlots.Count); } }	// 使用中のダウンロードスロットの実際の数

		protected List<REQUEST>	WaitingSlots  =  new List<REQUEST>();	// 待ちスロット（List）。ダウンロードしないで待機しておく

		protected Dictionary<string,CONTENT> URL2ContentDictionary = new Dictionary<string,CONTENT>();	// URLとダウンロードデータのDictionary

		public int	DownloadContentCount { get; private set; }	// ダウンロードしたコンテンツ数
		public int	DisposedContentCount { get; private set; }	// 破棄したコンテンツ数

		public event Action<REQUEST> 				OnStartedAction;	// アップロード開始時のアクション
		public event Action<string,CONTENT>	OnCompletedAction;	// ダウンロード終了時のアクション（成功失敗を問わず呼び出す）	


		protected void Awake(){  Reset();  }	// おなじみMonoBehaviour生成時に呼ばれる処理
	
		public virtual void Reset(){	// 初期化
			if(WWWSlots.Count>0 && WaitingSlots.Count>0)
				CancelAll();

			if(URL2ContentDictionary.Keys.Count>0)
				DisposeAll();

			DownloadContentCount = 0;
			DisposedContentCount = 0;		
		}
	
		// 継承する時は、次のメソッドをオーバーライドすること
		public virtual string  GetURLFrom(REQUEST request){ return ""; }            //  型REQUESTを使ったURLの取り出し方
		public virtual WWW     GetWWWFrom(REQUEST request){ return new WWW( GetURLFrom(request) ); }	// 型REQUESTを使ったWWWFormの呼び出し方
		public virtual CONTENT GetContentFrom(WWW www){ return default(CONTENT); }  // WWWクラスインスタンスからの目的のデータの取り出し方
		public virtual void    DestroyContent(CONTENT content){}                    // ダウンロードしたデータの破棄の仕方
		//
	
		public bool isDownloaded(string url){	// URLのダウンロードが終了していればtrue
			return URL2ContentDictionary.ContainsKey(url); // キーがある場合、ダウンロードが終了しているとみなす
		}
	
		public bool isDownloading(string url){	// URLがダウンロード中ならばtrue
			foreach(WWW slot in WWWSlots)	
				if(url==slot.url)				// URLがダウンロード中でなければ
					return true;
			return false;
		}
		public bool isDownloading(){ return WWWSlots.Count>0; }	// ダウンロードしているか
	
		public bool isWaiting(){ return WaitingSlots.Count>0; }	// 待ちが発生しているか
	
	
		public bool TryGetURLContent(string url, out CONTENT content){	// URLのダウンロードした内容を取り出してみる。成功すればtrue、失敗したらfalse
			if( isDownloaded(url) ){					// ダウンロードが終了している場合
				content = URL2ContentDictionary[url];	// contentにデータを格納して
				return true;					// trueを返す
			}
			else{								// ダウンロードされていない場合
				content = default(CONTENT);			// デフォルト値を格納して
				return false;					// falseを返す
			}
		}
	
		public void Request(REQUEST request){	// urlに対するダウンロードリクエストを処理する。
			if( WWWSlotCount < WWWSlotMax && !isDownloading(GetURLFrom(request)))	// ダウンロードスロットに空きがあり、ダウンロード中のURLがなければ
				StartCoroutine( Download(request) );		// ダウンロード開始
			else if(!WaitingSlots.Contains(request))
				WaitingSlots.Add(request);					// そうでなく、同一のURLもなければ、待ちスロットに追加
		}
		public void FastRequest(REQUEST request){	// urlに対する急ぎのダウンロードリクエストを処理する。
			if( WWWSlotCount < WWWSlotMax && !isDownloading(GetURLFrom(request)))	// ダウンロードスロットに空きがあり、ダウンロード中のURLがなければ
				StartCoroutine( Download(request) );		// ダウンロード開始
			else if(!WaitingSlots.Contains(request))
				WaitingSlots.Insert(0,request);				// そうでなく、同一のURLもなければ、待ちスロットの最前に追加
		}

	
		private IEnumerator Download(REQUEST request){	// urlのダウンロードをはじめる
			CONTENT content = default(CONTENT);		// content初期化
			
			if(OnStartedAction!=null)
				OnStartedAction(request);	// OnDownloadStartedActionがあればそれを実行
	
			string url = GetURLFrom(request);		// 元URLを保存（URLスキームの変換を防ぐ）

			float StartTime               = Time.time;		// 開始時間StartTime生成
			float UploadTimeLimit     = MaxUploadWaitTime + StartTime;	// アップロード終了時間UploadTimeLimit生成
			float DownloadTimeLimit = MaxDownloadWaitTime      + StartTime;	// ダウンロード終了時間DownloadTimeLimit生成

			while( Time.time < UploadTimeLimit ){
				using( WWW www = GetWWWFrom(request) ){	// wwwを生成。WWWインスタンスは処理がusnigスコープ外になるときに、自動的に破棄される。
					WWWSlots.Add( www );		// スロットに追加

					while( WWWSlots.Contains(www) && www.progress<1.0f && Time.time < DownloadTimeLimit){	// wwwがダウンロードスロットにあり、ダウンロード終了しておらず、制限時間内であれば、待機処理
						yield return new WaitForSeconds( Mathf.Min( (DownloadTimeLimit-Time.time), MaxConfirmationPeriod) );	// 上限をMaxConfirmationPeriodとして待機
					}

					if( WWWSlots.Contains(www) ){	// wwwがダウンロードスロットにあるとき
						if( www.progress>=1.0f && string.IsNullOrEmpty(www.error) ){	// wwwがダウンロード終了していて、エラーなくダウンロードできている場合
							WWWSlots.Remove(www);		// ダウンロードスロットから除去
							content = GetContentFrom(www);		// wwwからコンテンツを取り出し
							DownloadContentCount++;				// DownloadContentCountをインクリメント
							break;
						}
						else{	// そうでない場合は、時間をおいてからもう一度ダウンロードを試みる
							if( MinUploadRetryPeriod + Time.time < UploadTimeLimit ){	// 待機してもMaxUploadWaitTimeを超えないとき
								float WaitTimeLimit = Time.time + MaxUploadWaitTime;	// 待機終了時間WaitTimeLimit生成
								while( WWWSlots.Contains(www) && Time.time < WaitTimeLimit )	// wwwがダウンロードスロットにあり、制限時間内であれば、待機処理
									yield return new WaitForSeconds( MaxConfirmationPeriod  );	// 上限をMaxConfirmationPeriodとして待機
								if( !WWWSlots.Remove(www) )	// ダウンロードスロットから除去 。できない場合は、キャンセルされたものとして終了
									break;
							}else{ // 超えてしまうようならあきらめる
								WWWSlots.Remove(www);		// ダウンロードスロットから除去
								break;
							}
							//	 continue;
						}
					}
					else // ダウンロードスロットにないときは、キャンセルされたものとして終了
						break;
				}
			}

			URL2ContentDictionary[url] = content;	// ダウンロードしたコンテンツをURL2ContentDictionaryに登録
			if(OnCompletedAction!=null)
				OnCompletedAction(url, content);	// OnDownloadCompletedActionがあればそれを実行

			if( WaitingSlots.Count>0 && WWWSlotCount<WWWSlotMax ){	// 待ちスロットにアイテムがあるとき
				StartCoroutine( Download(WaitingSlots[0]) );	// 新しくダウンロード開始
				WaitingSlots.RemoveAt(0);		// 待ちスロットから除去
			}
		}

		public bool Cancel(REQUEST request){	// urlのダウンロード動作をキャンセルする（ダウンロード済みなら干渉しない）
			foreach(WWW www in WWWSlots)
				if(www.url==GetURLFrom(request)){				// ダウンロードスロットの中に URLが一致するwwwがあれば
					WWWSlots.Remove(www);	// ダウンロードスロットから除去（Downloadメソッドのループから抜ける）
					www.Dispose();				// wwwを破棄
					return true;
				}
			return WaitingSlots.Remove(request);	// ダウンロードスロットの中になければ、待ちスロットを調べ、除去を試みる
		}
		public void CancelAll(){	// 全てのダウンロード動作をキャンセルする
			foreach(WWW www in WWWSlots)
				www.Dispose();					// ダウンロードスロットのwwwを全て破棄＋ダウンロード中止
			WWWSlots.Clear();				// ダウンロードスロットを空に

			WaitingSlots.Clear();				// 待ちスロットを空に
		}


		public void Dispose(string url, float delay, bool useUnload){	// ダウンロード済みコンテンツを破棄
			StartCoroutine( dispose(url,delay) );	// disposeを起動
			if(useUnload)											// useUnloadの場合
				StartCoroutine( release(delay+Time.deltaTime/2f) );	// releaseを起動
		}
		public void Dispose(string url, bool useUnload){	// 多相型
			Dispose(url,0f,useUnload);
		}
		private IEnumerator dispose(string url, float delay){	// urlのダウンロード済みコンテンツを、このクラスから破棄
			if(delay>0f)
				yield return new WaitForSeconds(delay);	// delayがある場合、その時間だけ待機

			if(URL2ContentDictionary.ContainsKey(url)){		// URL2ContentDictionaryにurlの登録がある場合
				DestroyContent(URL2ContentDictionary[url]);	// コンテンツを破棄
				URL2ContentDictionary.Remove(url);			// urlの登録を除去
				DisposedContentCount++;						// DisposedContentCountをインクリメント
			}

			yield return true;
		}
		private IEnumerator release(float delay){	// コンテンツ破棄の後処理（www.dispose()しただけでは完全に消滅しない）
			if(delay>0f)
				yield return new WaitForSeconds(delay);	// delayがある場合、その時間だけ待機
			Resources.UnloadUnusedAssets();			// Resources.UnloadUnusedAssets()で、メモリ領域を開放
		}
	
		public void DisposeAll(float delay){
			StartCoroutine( disposeAll(delay) );				// disposeを起動
			StartCoroutine( release(delay+Time.deltaTime/2f) );	// releaseを起動
		}
		public void DisposeAll(){	// 多相型
			DisposeAll(0f);
		}
		private IEnumerator disposeAll(float delay){	// 全てのダウンロード済みコンテンツを、このクラスから破棄
			if(delay>0f)
				yield return new WaitForSeconds(delay);	// delayがある場合、その時間だけ待機
		
			foreach(string url in URL2ContentDictionary.Keys){	// URL2ContentDictionaryの全てのコンテンツのurlについて
				DestroyContent(URL2ContentDictionary[url]);		// コンテンツを破棄
				DisposedContentCount++;							// DisposedContentCountをインクリメント
			}
			URL2ContentDictionary.Clear();				// URL2ContentDictionaryをクリア
	
			yield return true;
		}
	}

}