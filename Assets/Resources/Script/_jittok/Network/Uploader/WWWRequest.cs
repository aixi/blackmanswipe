using UnityEngine;

namespace DiceCreative.TsukubaLab.JTKNetwork{

	public class WWWRequest{	// WWWFormを使ったWWWクラスへのダウンロード要求に必要な情報
		public string  URL	{ get; protected set; }	// URL
		public WWWForm Form	{ get; protected set; }	// WWWForm

		public WWWRequest(string url, WWWForm form){
			URL  = url;
			Form = form;
		}
	}

}