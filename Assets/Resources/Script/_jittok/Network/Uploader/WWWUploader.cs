using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using DiceCreative.TsukubaLab.JTKNetwork;

// namespace DiceCreative.TsukubaLab.JTKNetwork{

	public class WWWUploader : WWWManager<WWWRequest,string>{	// WWWクラス経由でアップロードを実行する
		public readonly static bool canUpload = true;	// アップロードできるか
		public string DefaultURL;	// デフォルトで送信するURL（単一のサーバー用）
			
		private void Awake(){
			// if( SystemManager.useDebugMessage ){
				// this.OnStartedAction   += (request)=> Debug.Log("Upload Start at:\t" + request.URL );
				// this.OnCompletedAction += (url,content)=> Debug.Log("Upload End at:\t" + url  +"||"+ content );
			// }
		}
		
		public override	string  GetURLFrom(WWWRequest request){ return request.URL; }	//  型WWWRequestを使ったURLの取り出し方
		public override	WWW     GetWWWFrom(WWWRequest request){ return new WWW(request.URL, request.Form); }	// 型WWWRequestを使ったWWWFormの呼び出し方
		public override	string	GetContentFrom(WWW www){	return www.text;	}		// WWWクラスインスタンスからの目的のデータの取り出し方
		public override	void	DestroyContent(string content){}						// ダウンロードしたデータの破棄の仕方
		
		public     void Request(WWWForm form){			Request( new WWWRequest(DefaultURL,form) );	}		// urlに対するダウンロードリクエストを処理する
		public new void Request(WWWRequest req){		if(canUpload) base.Request( req );	}
		public     void FastRequest(WWWForm form){		FastRequest( new WWWRequest(DefaultURL,form) );	}	// urlに対する急ぎのダウンロードリクエストを処理する
		public new void FastRequest(WWWRequest req){	if(canUpload) base.FastRequest( req );	}
	}

// }