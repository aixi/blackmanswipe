using UnityEngine;
using System;
using System.Runtime.InteropServices;


public class ResourceManager : MonoBehaviour {	// プログラマ側で設定するアプリケーション情報のまとめ（今回はピヨすわの情報をベースにしている）。説明はかなり省略
	public class General{
		public class Vender{
			public readonly static string Identifier = "dice";
		}
		public class Application{
			public readonly static string Identifier = "piyosuwa";
			#if !UNITY_EDITOR && UNITY_IPHONE
			public readonly static string Version    = _GetAppVersion();	// iOSならばVersionは自動取得
			#else
			public readonly static string Version    = "1.0";
			#endif
			
			public readonly static string[] StageNames = new string[]{ "none", "shokyu", "chukyu", "joukyu", "tatsujin", "jigoku", "cho-jigoku" };
			
			public static GProNetworkPacket.AppMessages GetAppMessage(GProNetworkPacket.AppActionDetailIDs actionID){	// GProNetworkPacketの構成に必要なAppMessageを取得（GProNetworkPacketConnectorがstaticな設定である反面、こちらは動的な設定である点で差別化）
				return new GProNetworkPacket.AppMessages(
					actionID,	// AppActionDetail
					0,	// level
					0,	// charge
					0,	// point
					"None",	// Item Identifier
					StageNames[ PlayerPrefs.GetInt("LastSelectedStage",0) ],	// Stage Identifier
					PlayerPrefs.GetInt("Score1").ToString(),	// Option Mesaage1
					"shokyu:"	+ Math.Max( PlayerPrefs.GetInt("Stage1")-1, 0) +",tyukyu:" + Math.Max( PlayerPrefs.GetInt("Stage2")-1, 0) + ",joukyu:"		+ Math.Max( PlayerPrefs.GetInt("Stage3")-1, 0),	// Option Mesaage2
					"tatsujin:"	+ Math.Max( PlayerPrefs.GetInt("Stage4")-1, 0) +",jigoku:" + Math.Max( PlayerPrefs.GetInt("Stage5")-1, 0) + ",cho-jigoku:"	+ Math.Max( PlayerPrefs.GetInt("Stage6")-1, 0),	// Option Mesaage3
					"None",	// Event Identifier
					"None"	// Error Mesage
				);
			}
			// ResourceManagerで上記情報の取得が完結できない場合は、次のメソッドを使って、このクラス以外でAppMessageを作成する
			public static GProNetworkPacket.AppMessages BasicAppMessage{	get{ return GetAppMessage(); }	}
			public static GProNetworkPacket.AppMessages GetAppMessage(){	return GetAppMessage( GProNetworkPacket.AppActionDetailIDs.None );	}
		}
		public class Device{
			public static string Identifier{	get{ return SystemInfo.deviceModel; }	}
			public static string TrackingIdentifier{
				get{
				#if !UNITY_EDITOR && UNITY_IPHONE
					// iOSのみ定義
					if( int.Parse(OS.Version.Split( new char[]{ '.' })[0])>=6 )
						return _GetIdentifierForVendor(); 
					else
						return "unknown(iOS" + OS.Version +")";
				#else
					return "IdentifierForVendor will be Displayed on iOS";
				#endif
				}
			}
			private static string guid = null;
			public  static string GUID{
				get{
					if(string.IsNullOrEmpty(guid)){
						guid = PlayerPrefs.GetString("GUID", null);	// PlayerPrefsが嫌な場合は、GrobalSaveDataなどに変更する
						if(string.IsNullOrEmpty(guid)){
							#if !UNITY_EDITOR && UNITY_IPHONE
							guid =  _GetGUID();		// iOSのみGuid.NewGuid()が使えないらしいので、独自に定義
							#else
							guid = Guid.NewGuid().ToString();
							#endif
							PlayerPrefs.SetString("GUID", guid);
						}
					}
					return guid;
				}
			}
		}
		public class OS{
			public static string Identifier{
				get{
					switch(UnityEngine.Application.platform){
						case RuntimePlatform.Android:
							return "android";
						case RuntimePlatform.IPhonePlayer:
							return "ios";
					}
					return "unknown";
				}
			}
			public static string Version{
				get{
					int length = 0;
					switch(UnityEngine.Application.platform){
						case RuntimePlatform.Android:
							length = "Android OS ".Length;
						break;
						case RuntimePlatform.IPhonePlayer:
							length = "iPhone OS ".Length;
						break;
						case RuntimePlatform.OSXEditor:
							length = "Mac OS X ".Length;
						break;
					}
					return SystemInfo.operatingSystem.Substring( length, SystemInfo.operatingSystem.Length-length );
				}
			}
			public static string CountryIdentifier{
				get{
					#if !UNITY_EDITOR && UNITY_IPHONE
						// iOSのみ定義
						return _GetLocaleIdentifier();
					#else
						return UnityEngine.Application.systemLanguage.ToString();
					#endif
				}
			}
		}
		
		#if UNITY_IPHONE
		// iOSでのネイティブ連携のためのインポート処理
		[DllImport("__Internal")]
		private static extern string _GetAppName();
		[DllImport("__Internal")]
		private static extern string _GetAppVersion();
		[DllImport("__Internal")]
		private static extern string _GetIdentifierForVendor();
		[DllImport("__Internal")]
		private static extern string _GetLocaleIdentifier();
		[DllImport("__Internal")]
		private static extern string _GetGUID();
		#endif
	}
	
	/*
	public class Strage{
		// private static string SaveFilenameHeader = Application.persistentDataPath + "/Documents/";

		// public static string GrobalSaveFileResourcesPath{	get{ return SaveFilenameHeader + "GeneralSave.bin"; }	}	// GrobalSaveDataの保存先となるパス
		// public static string SaveFileResourcesPath(int nth){ return SaveFilenameHeader + "Save" + nth.ToString("D1") + ".bin"; }
		// public static string AutoSaveFileResourcesPath{	get{ return SaveFilenameHeader + "AutoSave.bin"; }	}
		
		// public static string DefaultTelephoneNumber	= "01234567890";
		// public static int    DefaultCurrentPoint	= 200;
		// public static int DefaultBGMVolumePercentage	= 50;
		// public static int DefaultSEVolumePercentage		= 50;
		// public static int DefaultTextSpeedPercentage	= 50;
	}
	*/
}
