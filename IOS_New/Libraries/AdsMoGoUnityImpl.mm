//
//  AdsMoGoUnityImpl.m
//  AdsMoGoUnity
//
//  Created by MOGO on 12-11-6.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "AdsMoGoUnityImpl.h"
#import "AdMoGoView.h"
#import "AdViewType.h"

#import "AdsMoGoUnityTool.h"

void UnitySendMessage( const char * className, const char * methodName, const char * param );

@implementation AdsMoGoUnityImpl

#pragma mark -
#pragma mark AdMoGoDelegate delegate 
/*
 返回广告rootViewController
 */
- (UIViewController *)viewControllerForPresentingModalView{
    return UnityGetGLViewController();
}



/**
 * 广告开始请求回调
 */
- (void)adMoGoDidStartAd:(AdMoGoView *)adMoGoView{
   UnitySendMessage("AdMoGoManager", "adMoGoDidStartAd",""); 
}
/**
 * 广告接收成功回调
 */
- (void)adMoGoDidReceiveAd:(AdMoGoView *)adMoGoView{
    UnitySendMessage("AdMoGoManager","adMoGoDidReceiveAd","");
}
/**
 * 广告接收失败回调
 */
- (void)adMoGoDidFailToReceiveAd:(AdMoGoView *)adMoGoView didFailWithError:(NSError *)error{
    NSString *errorStr = [error description];
    const char *err = [errorStr cStringUsingEncoding:NSUTF8StringEncoding];
    UnitySendMessage("AdMoGoManager","adMoGoDidFailToReceiveAd",err);
}
/**
 * 点击广告回调
 */
- (void)adMoGoClickAd:(AdMoGoView *)adMoGoView{
    UnitySendMessage("AdMoGoManager","adMoGoClickAd","");
}
/**
 *You can get notified when the user delete the ad 
 广告关闭回调
 */
- (void)adMoGoDeleteAd:(AdMoGoView *)adMoGoView{
    UnitySendMessage("AdMoGoManager","adMoGoDeleteAd","");
}

- (void)adMoGoInitFinish:(AdMoGoView *)adMoGoView{
    UnitySendMessage("AdMoGoManager","adMoGoInitFinish","");
}

#pragma mark -
#pragma mark AdMoGoWebBrowserControllerUserDelegate delegate 

/*
 浏览器将要展示
 */
- (void)webBrowserWillAppear{
    UnitySendMessage("AdMoGoManager","webBrowserWillAppear","");
}

/*
 浏览器已经展示
 */
- (void)webBrowserDidAppear{
    UnitySendMessage("AdMoGoManager","webBrowserDidAppear","");
}

/*
 浏览器将要关闭
 */
- (void)webBrowserWillClosed{
    UnitySendMessage("AdMoGoManager","webBrowserWillClosed","");
}

/*
 浏览器已经关闭
 */
- (void)webBrowserDidClosed{
    UnitySendMessage("AdMoGoManager","webBrowserDidClosed","");
}

- (void)webBrowserShare:(NSString *)url{
    NSString *urlStr = [url description];
    const char *url_char = [urlStr cStringUsingEncoding:NSUTF8StringEncoding];
    UnitySendMessage("AdMoGoManager","webBrowserShare",url_char);
}

@end

AdMoGoView *adsMoGoView = nil;
AdsMoGoUnityImpl *adsMoGoDelegate = nil; 
UIView *mainView;






extern "C" {
    /*
        初始化AdsMoGoView,请求广告配置
     */
    void _initAdsMoGoView(const char* mogokey,int adtype){
        
        if (adsMoGoView != NULL) {
            return;
        }
        
        NSString *key = AdsMoGoCreateNSString(mogokey);
        adsMoGoDelegate = [[AdsMoGoUnityImpl alloc] init];
        
        AdViewType  _adType =AdViewTypeNormalBanner;
        switch (adtype) {
            case 0:
            case 5:
            case 7: 
                break;
            case 1:
                _adType =AdViewTypeNormalBanner;
                break; 
            case 2:
                _adType =AdViewTypeLargeBanner;
                break;
            case 3:
                _adType =AdViewTypeMediumBanner;
                break;
            case 4:
                 _adType =AdViewTypeRectangle;
                break;
            case 8:
                _adType =AdViewTypeiPadNormalBanner;
                break;
            case 14:
                 _adType =AdViewTypeiPhoneRectangle;
                break;
            default:
                NSLog(@"The request %d AdType don't support.",adtype);
                return;
                break;
        }
        adsMoGoView = [[AdMoGoView alloc] initWithAppKey:key  adType:_adType  adMoGoViewDelegate:adsMoGoDelegate];
        adsMoGoView.adWebBrowswerDelegate = adsMoGoDelegate;
        UIView *view = UnityGetGLView();
        [view addSubview:adsMoGoView];
    } 
    
    void _initAdsMoGoViewManualFresh(const char* mogokey,int adtype){
        if (adsMoGoView != NULL) {
            return;
        }
        
        NSString *key = AdsMoGoCreateNSString(mogokey);
        adsMoGoDelegate = [[AdsMoGoUnityImpl alloc] init];
        
        AdViewType  _adType =AdViewTypeNormalBanner;
        switch (adtype) {
            case 0:
            case 5:
            case 7:
                break;
            case 1:
                _adType =AdViewTypeNormalBanner;
                break;
            case 2:
                _adType =AdViewTypeLargeBanner;
                break;
            case 3:
                _adType =AdViewTypeMediumBanner;
                break;
            case 4:
                _adType =AdViewTypeRectangle;
                break;
            case 8:
                _adType =AdViewTypeiPadNormalBanner;
                break;
            case 14:
                _adType =AdViewTypeiPhoneRectangle;
                break;
            default:
                NSLog(@"The request %d AdType don't support.",adtype);
                return;
                break;
        }
        
        //手动刷新初始化方式
        adsMoGoView = [[AdMoGoView alloc] initWithAppKey:key  adType:_adType  adMoGoViewDelegate:adsMoGoDelegate  isManualRefresh:YES];
        adsMoGoView.adWebBrowswerDelegate = adsMoGoDelegate;
        UIView *view = UnityGetGLView();
        [view addSubview:adsMoGoView];

    }
    
    
    //手动刷新接口
    void _refreshAd(){
        [adsMoGoView refreshAd];
    }
    
    
    /*
        设置AdsMoGoView 的位置
     */
    void _Setposition(const float x,const float y){
        CGRect frame = adsMoGoView.frame;
        frame.origin.x = x;
        frame.origin.y = y;
        adsMoGoView.frame = frame;
    }
    
    /*
        获取AdsMoGoView的X坐标
     */
    float _Getposition_X(){
        if (adsMoGoView) {
            CGRect frame = adsMoGoView.frame;
            return frame.origin.x;
        }
        else {
            return 0.0;
        }
    }
    
    /*
        获取AdsMoGoView的Y坐标
     */
    float _Getposition_Y(){
        if (adsMoGoView) {
            CGRect frame = adsMoGoView.frame;
            return frame.origin.x;
        }
        else {
            return 0.0;
        }
    }
    
    /*
        隐藏AdsMoGoView
     */
    void _SetAdsMoGoViewHidden(const bool ishidden){
        if (adsMoGoView) {
            adsMoGoView.hidden = ishidden;
        }
    }
    
    /*
        获取AdsMoGoView hidden 属性值
     */
    bool _GetAdsMoGoViewhidden(){
        if (adsMoGoView) {
            return adsMoGoView.isHidden;
        }
    } 
    
    /*
        释放AdsMoGoView
     */
    void _releaseAdsMoGoView(){
        if(adsMoGoView){
            adsMoGoView.delegate = nil;
            adsMoGoView.adWebBrowswerDelegate = nil;
            [adsMoGoView removeFromSuperview];
            adsMoGoView = nil;
        }
    }
    
    void _setViewPointType(int viewPointType){
        if (adsMoGoView) {
            switch (viewPointType) {
                case 0:
                    [adsMoGoView setViewPointType:AdMoGoViewPointTypeTop_left];
                    break;
                case 1:
                    [adsMoGoView setViewPointType:AdMoGoViewPointTypeTop_middle];
                    break;
                case 2:
                    [adsMoGoView setViewPointType:AdMoGoViewPointTypeTop_right];
                    break;
                case 3:
                    [adsMoGoView setViewPointType:AdMoGoViewPointTypeMiddle_left];
                    break;
                case 4:
                    [adsMoGoView setViewPointType:AdMoGoViewPointTypeMiddle_middle];
                    break;
                case 5:
                    [adsMoGoView setViewPointType:AdMoGoViewPointTypeMiddle_right];
                    break;
                case 6:
                    [adsMoGoView setViewPointType:AdMoGoViewPointTypeDown_left];
                    break;
                case 7:
                    [adsMoGoView setViewPointType:AdMoGoViewPointTypeDown_middle];
                    break;
                case 8:
                    [adsMoGoView setViewPointType:AdMoGoViewPointTypeDown_right];
                    break;
                case 9:
                    [adsMoGoView setViewPointType:AdMoGoViewPointTypeCustom];
                    break;
                default:
                    break;
            }
        }
        
    }
    
    int _getViewPointType(){
        AdMoGoViewPointType pointType = [adsMoGoView getViewPointType];
        switch (pointType) {
            case AdMoGoViewPointTypeTop_left:
                return 0;
                break;
            case AdMoGoViewPointTypeTop_middle:
                return 1;
                break;
            case AdMoGoViewPointTypeTop_right:
                return 2;
                break;
            case AdMoGoViewPointTypeMiddle_left:
                return 3;
                break;
            case AdMoGoViewPointTypeMiddle_middle:
                return 4;
                break;
            case AdMoGoViewPointTypeMiddle_right:
                return 5;
                break;
            case AdMoGoViewPointTypeDown_left:
                return 6;
                break;
            case AdMoGoViewPointTypeDown_middle:
                return 7;
                break;
            case AdMoGoViewPointTypeDown_right:
                return 8;
                break;
            case AdMoGoViewPointTypeCustom:
                return 9;
                break;
            default:
                break;
        }
    }
}

