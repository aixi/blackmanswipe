//
//  AdsMoGoUnityInterstitialImpl.h
//  Unity-iPhone
//
//  Created by MOGO on 13-9-25.
//
//

#import <Foundation/Foundation.h>
#import "AdMoGoInterstitialDelegate.h"
#import "AdMoGoWebBrowserControllerUserDelegate.h"
#import "AdMoGoInterstitialManager.h"
@interface AdsMoGoUnityInterstitialImpl : NSObject<AdMoGoInterstitialDelegate,AdMoGoWebBrowserControllerUserDelegate>

@end
