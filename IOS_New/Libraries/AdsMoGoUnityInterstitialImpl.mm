//
//  AdsMoGoUnityInterstitialImpl.m
//  Unity-iPhone
//
//  Created by MOGO on 13-9-25.
//
//

#import "AdsMoGoUnityInterstitialImpl.h"
#import "AdMoGoInterstitial.h"
#import "AdsMoGoUnityTool.h"
#import "AdMoGoLogCenter.h"
void UnitySendMessage( const char * className, const char * methodName, const char * param );


@interface AdsMoGoUnityInterstitialImpl()
{
    
}
@end

@implementation AdsMoGoUnityInterstitialImpl

/*
 返回广告rootViewController
 */
- (UIViewController *)viewControllerForPresentingInterstitialModalView{
    return UnityGetGLViewController();
}


/*
 全屏广告开始请求
 */
- (void)adsMoGoInterstitialAdDidStart{
    UnitySendMessage("AdMoGoManager","adsMoGoInterstitialAdDidStart","");
}

/*
 全屏广告准备完毕
 */
- (void)adsMoGoInterstitialAdIsReady{
    UnitySendMessage("AdMoGoManager","adsMoGoInterstitialAdIsReady","");
}

/*
 全屏广告接收成功
 */
- (void)adsMoGoInterstitialAdReceivedRequest{
    UnitySendMessage("AdMoGoManager","adsMoGoInterstitialAdReceivedRequest","");
}

/*
 全屏广告将要展示
 */
- (void)adsMoGoInterstitialAdWillPresent{
    UnitySendMessage("AdMoGoManager","adsMoGoInterstitialAdWillPresent","");
}

/*
 全屏广告接收失败
 */
- (void)adsMoGoInterstitialAdFailedWithError:(NSError *) error{
    NSString *errorStr = [error description];
    const char *err = [errorStr cStringUsingEncoding:NSUTF8StringEncoding];
    UnitySendMessage("AdMoGoManager","adsMoGoInterstitialAdFailedWithError",err);
}

/*
 全屏广告消失
 */
- (void)adsMoGoInterstitialAdDidDismiss{
    UnitySendMessage("AdMoGoManager","adsMoGoInterstitialAdDidDismiss","");
}

/*
 全屏广告浏览器展示
 */
- (void)adsMoGoWillPresentInterstitialAdModal{
    UnitySendMessage("AdMoGoManager","adsMoGoWillPresentInterstitialAdModal","");
}

/*
 全屏广告浏览器消失
 */
- (void)adsMoGoDidDismissInterstitialAdModal{
    UnitySendMessage("AdMoGoManager","adsMoGoDidDismissInterstitialAdModal","");
}

/*
 芒果广告关闭
 */
- (void)adsMogoInterstitialAdClosed{
    UnitySendMessage("AdMoGoManager","adsMogoInterstitialAdClosed","");
}

/**
 *视频广告加载成功
 */
-(void)adsMogoInterstitial:(AdMoGoInterstitial *)adMoGoInterstitial didLoadVideoAd:(NSURL *)url{
    NSString *url_str = [url absoluteString];
    const char *url_char = [url_str cStringUsingEncoding:NSUTF8StringEncoding];
    UnitySendMessage("AdMoGoManager","didLoadVideoAd",url_char);
}
/**
 *视频广告加载失败
 */
-(void)adsMogoInterstitial:(AdMoGoInterstitial *)adMoGoInterstitial failLoadVideoAd:(NSURL *)url{
    NSString *url_str = [url absoluteString];
    const char *url_char = [url_str cStringUsingEncoding:NSUTF8StringEncoding];
    UnitySendMessage("AdMoGoManager","failLoadVideoAd",url_char);
}
/**
 *视频广告开始播放
 */
-(void)adsMogoInterstitial:(AdMoGoInterstitial *)adMoGoInterstitial didPlayVideoAd:(NSURL *)url{
    NSString *url_str = [url absoluteString];
    const char *url_char = [url_str cStringUsingEncoding:NSUTF8StringEncoding];
    UnitySendMessage("AdMoGoManager","didPlayVideoAd",url_char);
}
/**
 *视频广告播放完成
 */
-(void)adsMogoInterstitial:(AdMoGoInterstitial *)adMoGoInterstitial finishVideoAd:(NSURL *)url{
    NSString *url_str = [url absoluteString];
    const char *url_char = [url_str cStringUsingEncoding:NSUTF8StringEncoding];
    UnitySendMessage("AdMoGoManager","finishVideoAd",url_char);
}

/*
 芒果广告轮空回调
 */
- (void)adsMogoInterstitialAdAllAdsFail:(NSError *) error{
    NSString *errorStr = [error description];
    const char *err = [errorStr cStringUsingEncoding:NSUTF8StringEncoding];
    UnitySendMessage("AdMoGoManager","adsMogoInterstitialAdAllAdsFail",err);
}



-(BOOL)interstitialShouldAlertQAView:(UIAlertView *)alertView{
    return NO;
}

/*插屏初始化回调*/
- (void)adMoGoInterstitialInitFinish{
    UnitySendMessage("AdMoGoManager","adMoGoInterstitialInitFinish","");
}

- (void)adMoGoInterstitialInMaualfreshAllAdsFail{
    UnitySendMessage("AdMoGoManager","adMoGoInterstitialInMaualfreshAllAdsFail","");
}
#pragma mark -
#pragma mark AdMoGoWebBrowserControllerUserDelegate delegate

/*
 浏览器将要展示
 */
- (void)webBrowserWillAppear{
    UnitySendMessage("AdMoGoManager","webBrowserWillAppear","");
}

/*
 浏览器已经展示
 */
- (void)webBrowserDidAppear{
    UnitySendMessage("AdMoGoManager","webBrowserDidAppear","");
}

/*
 浏览器将要关闭
 */
- (void)webBrowserWillClosed{
    UnitySendMessage("AdMoGoManager","webBrowserWillClosed","");
}

/*
 浏览器已经关闭
 */
- (void)webBrowserDidClosed{
    UnitySendMessage("AdMoGoManager","webBrowserDidClosed","");
}

@end

//AdMoGoInterstitial *adsMoGoInterstitial = nil;
AdsMoGoUnityInterstitialImpl *adsMoGoInterstitialDelegate = nil;




extern "C" {
    void _initAdsMoGoInterstitial(int adtype,const char* mogokey){
        AdMoGoInterstitial *adsMoGoInterstitial = nil;
        NSString *key = AdsMoGoCreateNSString(mogokey);
        adsMoGoInterstitialDelegate = [[AdsMoGoUnityInterstitialImpl alloc] init];
        switch (adtype) {
            case 6:
            case 8:
                //常规初始化方式
                adsMoGoInterstitial = [[AdMoGoInterstitialManager shareInstance] adMogoInterstitialByAppKey:key];
                adsMoGoInterstitial.delegate = adsMoGoInterstitialDelegate;
                break;
            case 11:
                //常规初始化方式
                [[AdMoGoInterstitialManager shareInstance] adMogoVideoInterstitialByAppKey:key].delegate = adsMoGoInterstitialDelegate;
                adsMoGoInterstitial = [[AdMoGoInterstitialManager shareInstance] adMogoVideoInterstitialByAppKey:key];
                break;
            default:
                return;
                break;
        }
        
        if (adsMoGoInterstitial) {
            adsMoGoInterstitial.adWebBrowswerDelegate = adsMoGoInterstitialDelegate;
        }
    }
    
    void _initAdsMoGoInterstitialManual(int adtype,const char* mogokey){
        AdMoGoInterstitial *adsMoGoInterstitial = nil;
        NSString *key = AdsMoGoCreateNSString(mogokey);
        adsMoGoInterstitialDelegate = [[AdsMoGoUnityInterstitialImpl alloc] init];
        
        switch (adtype) {
            case 6:
            case 8:
                //手动刷新的方式初始化
                adsMoGoInterstitial = [[AdMoGoInterstitialManager shareInstance] adMogoInterstitialByAppKey:key isManualRefresh:YES];
                adsMoGoInterstitial.delegate = adsMoGoInterstitialDelegate;
                break;
            case 11:
                //手动刷新的方式初始化
                adsMoGoInterstitial = [[AdMoGoInterstitialManager shareInstance] adMogoVideoInterstitialByAppKey:key isManualRefresh:YES];
                adsMoGoInterstitial.delegate = adsMoGoInterstitialDelegate;
                break;
            default:
                return;
                break;
        }
        
        if (adsMoGoInterstitial) {
            adsMoGoInterstitial.adWebBrowswerDelegate = adsMoGoInterstitialDelegate;
        }

    }
    
    void _presentAdsMoGoInterstitial(int adtype,const char* mogokey,bool isWait){
        NSString *key = AdsMoGoCreateNSString(mogokey);
        AdMoGoInterstitial *adsMoGoInterstitial = nil;
        
        switch (adtype) {
            case 6:
            case 8:
                adsMoGoInterstitial = [[AdMoGoInterstitialManager shareInstance] adMogoInterstitialByAppKey:key];
                break;
            case 11:
                adsMoGoInterstitial = [[AdMoGoInterstitialManager shareInstance] adMogoVideoInterstitialByAppKey:key];
                 break;
            default:
                return;
                break;
        }
        if (adsMoGoInterstitial) {
            [adsMoGoInterstitial interstitialShow:isWait];
        }
    }
    
    void _cancelAdsMoGoInterstitial(int adtype,const char* mogokey){
        NSString *key = AdsMoGoCreateNSString(mogokey);
        AdMoGoInterstitial *adsMoGoInterstitial = nil;
        
        switch (adtype) {
            case 6:
            case 8:
                adsMoGoInterstitial = [[AdMoGoInterstitialManager shareInstance] adMogoInterstitialByAppKey:key];
                break;
            case 11:
                adsMoGoInterstitial = [[AdMoGoInterstitialManager shareInstance] adMogoVideoInterstitialByAppKey:key];
                break;
            
            default:
                return;
                break;
        }
        if (adsMoGoInterstitial) {
            [adsMoGoInterstitial interstitialCancel];
        }
        
    }

    void _refreshAdsMoGoInterstitialAd(int adtype,const char* mogokey){
        NSString *key = AdsMoGoCreateNSString(mogokey);
        AdMoGoInterstitial *adsMoGoInterstitial = nil;
        
        switch (adtype) {
            case 6:
            case 8:
                adsMoGoInterstitial = [[AdMoGoInterstitialManager shareInstance] adMogoInterstitialByAppKey:key];
                break;
            case 11:
                adsMoGoInterstitial = [[AdMoGoInterstitialManager shareInstance] adMogoVideoInterstitialByAppKey:key];
                break;
                
            default:
                return;
                break;
        }
        [adsMoGoInterstitial  refreshAd];
    }
    
    void _releaseAdsMoGoInterstitial(int adtype,const char* mogokey){
        
        NSString *key = AdsMoGoCreateNSString(mogokey);
        switch (adtype) {
            case 6:
            case 8:
                [[AdMoGoInterstitialManager shareInstance] removeVideoInterstitialInstanceByAppKey:key];
                break;
            case 11:
                [[AdMoGoInterstitialManager shareInstance] removeInterstitialInstanceByAppKey:key];
                break;
            default:
                return;
                break;
        }
        
    }
}