//
//  IOSBridge.m
//
//
//  Created by Augeo on 5/28/14.
//
//

#import "IOSBridge.h"

@implementation BindingDelegate

-(id) init
{
    return self;
}
-(void) alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *inStr = [NSString stringWithFormat:@"%d",(int)buttonIndex];
    const char *cString = [inStr cStringUsingEncoding:NSASCIIStringEncoding];
    UnitySendMessage("BridgeManager", "UserFeedBack", cString);
}

@end

static BindingDelegate* delegateObject;

extern "C"
{
    void _Init()
    {
        [WXApi registerApp:@"wx9050cce1a043f3ab" withDescription:@"demo 2.0"];
    }
    void _SharedTextMoment(const char* txtMessage)
    {
        SendMessageToWXReq* req = [[[SendMessageToWXReq alloc] init]autorelease];
        req.text = [NSString stringWithUTF8String:txtMessage];
        req.bText = YES;
        req.scene = WXSceneTimeline;
        [WXApi sendReq:req];    }
    
    void _AddNotification(const char* title,
                          const char* body,
                          const char* cancelLabel,
                          const char* fisrtLabel,
                          const char* secondLabel)
    {
        
        if(delegateObject == nil)
        {
            delegateObject = [[BindingDelegate alloc] init];
        }
        
        UIAlertView *alrt = [[UIAlertView alloc]
                             initWithTitle:[NSString stringWithUTF8String:title]
                             message:[NSString stringWithUTF8String:body]
                             delegate:delegateObject
                             cancelButtonTitle:[NSString stringWithUTF8String:cancelLabel]
                             otherButtonTitles:[NSString stringWithUTF8String:fisrtLabel],
                             [NSString stringWithUTF8String:secondLabel],nil];
        
        [alrt show];
    }
}
